package dev.vcgroup.thonowforworkerandroidapp.util;

import android.util.Log;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

import java.util.regex.Pattern;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

@UtilityClass
public class AuthenticationUtil {
    private static final String EMAIL_THONOW_SUFFIX = "@thonowapp.vn";
    //Password matching expression. Password must be at least 6 characters, no more than 32 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit. 
    private static final String PASSWORD_REGEX = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,32}$";

    private static PhoneNumberUtil phoneNumberUtil;

    static {
        phoneNumberUtil = PhoneNumberUtil.getInstance();
    }

    public static String generateEmailFromPhone(String phone, String countryCode) {
        return parsePhoneNumber(phone.trim(), countryCode).getNationalNumber() + EMAIL_THONOW_SUFFIX;
    }

    public static String generateEmailFromNationalNumberPhone(String phone) {
        return phone.trim() + EMAIL_THONOW_SUFFIX;
    }

    public static String formatNumberPhone(Phonenumber.PhoneNumber phoneNumber, PhoneNumberUtil.PhoneNumberFormat phoneNumberFormat) {
        return phoneNumberUtil.format(phoneNumber, phoneNumberFormat);
    }

    @SneakyThrows
    public static boolean isValidNumber(String phoneNumber, String countryCode) {
        if (phoneNumber == null || phoneNumber.isEmpty()) {
            return false;
        }
        Phonenumber.PhoneNumber phoneNumberProto = phoneNumberUtil.parse(phoneNumber, countryCode);
        return phoneNumberUtil.isValidNumber(phoneNumberProto);
    }

    public static boolean isValidPasswordForChangePassword(String password) {
        return Pattern.matches(PASSWORD_REGEX, password);
    }

    public static boolean isConfirmPasswordMatch(String password, String confirmPassword) {
        return password.equals(confirmPassword);
    }

    public static PhoneNumber parsePhoneNumber(String phoneNumber, String countryCode) {
        try {
            return phoneNumberUtil.parse(phoneNumber, countryCode);
        } catch (NumberParseException e) {
            Log.d("parsePhoneNumber", e.getMessage());
        }
        return null;
    }
}
