package dev.vcgroup.thonowforworkerandroidapp.util.databinding.adapters;

import android.view.View;

import androidx.databinding.BindingAdapter;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;

@BindingMethods({
        @BindingMethod(type = View.class, attribute = "android:onClick", method = "setOnClickListener")
})
public class ViewBindingAdapter {
    @BindingAdapter({"android:onClickListener", "android:clickable"})
    public static void setClickListener(View view, View.OnClickListener clickListener,
                                        boolean clickable) {
        view.setOnClickListener(clickListener);
        view.setClickable(clickable);
    }

    @BindingAdapter({"android:onClick", "android:clickable"})
    public static void setOnClick(View view, View.OnClickListener clickListener,
                                  boolean clickable) {
        view.setOnClickListener(clickListener);
        view.setClickable(clickable);
    }

}
