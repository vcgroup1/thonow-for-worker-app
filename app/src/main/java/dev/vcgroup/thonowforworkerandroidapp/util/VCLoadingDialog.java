package dev.vcgroup.thonowforworkerandroidapp.util;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AlertDialog;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import dev.vcgroup.thonowforworkerandroidapp.R;


public class VCLoadingDialog {
    private Context mContext;
    private AlertDialog mLoadingDialog;

    public VCLoadingDialog(Context mContext) {
        this.mContext = mContext;
    }

    public static VCLoadingDialog create(Context context) {
        return new VCLoadingDialog(context);
    }

    private void initLoadingDialog() {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(mContext);
        View dialogView = LayoutInflater.from(mContext).inflate(R.layout.vcgroup_progress_loading, null);
        builder.setView(dialogView);
        builder.setCancelable(false);

        LottieAnimationView lavLoading = dialogView.findViewById(R.id.progress_loading);
        lavLoading.setAnimation(R.raw.loading);
        lavLoading.playAnimation();

        mLoadingDialog = builder.create();

        Window window = mLoadingDialog.getWindow();
        Point size = new Point();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams layoutParams = window.getAttributes();
        layoutParams.dimAmount = 0.5f;
        window.setAttributes(layoutParams);

        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);
        window.setLayout((int) (size.x * 0.3), WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
    }

    public VCLoadingDialog show() {
        if (!isShowing()) {
            initLoadingDialog();
            mLoadingDialog.show();
        }
        return this;
    }

    public boolean isShowing() {
        return mLoadingDialog != null && mLoadingDialog.isShowing();
    }

    public void dismiss() {
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mLoadingDialog.dismiss();
                }
            }, 100);
        }
    }
}
