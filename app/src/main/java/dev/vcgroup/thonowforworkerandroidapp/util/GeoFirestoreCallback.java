package dev.vcgroup.thonowforworkerandroidapp.util;

import com.koalap.geofirestore.GeoLocation;

import java.util.Map;

public interface GeoFirestoreCallback {
    void onKeyEnteredResponse(String key, GeoLocation location);
    void onKeyExitedResponse(String key);
}
