package dev.vcgroup.thonowforworkerandroidapp.util.keyboard;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.databinding.DataBindingUtil;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ViewThonowKeyboardBinding;
import sj.keyboard.XhsEmoticonsKeyBoard;
import sj.keyboard.utils.EmoticonsKeyboardUtils;

public class VCEmoticonsKeyBoard extends XhsEmoticonsKeyBoard {
    private ViewThonowKeyboardBinding binding;
    private OnChatKeyboardListener onChatKeyboardListener;
    private boolean isToggleMultimedia = false;
    private boolean isToggleSampleMessage = false;
    private boolean isToggleEmoji = false;
    private final int APP_HEIGHT = 533;
    private ImageView btnMultimedia, btnSend, btnSampleMessage;

    public void setOnChatKeyboardListener(OnChatKeyboardListener onChatKeyboardListener) {
        this.onChatKeyboardListener = onChatKeyboardListener;
    }

    public VCEmoticonsKeyBoard(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mInflater = LayoutInflater.from(context);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void inflateKeyboardBar() {
        //super.inflateKeyboardBar();
        View v = mInflater.inflate(R.layout.view_thonow_keyboard, this);
        mEtChat = v.findViewById(R.id.et_chat);
        btnSend = v.findViewById(R.id.btnSend);
        btnSampleMessage = v.findViewById(R.id.btn_sample_message);
        btnMultimedia = v.findViewById(R.id.btn_multimedia);
        mBtnVoiceOrText = v.findViewById(R.id.btn_voice_or_text);
        mBtnVoice = v.findViewById(R.id.btn_voice);

        btnSend.setOnClickListener(this);
        btnSampleMessage.setOnClickListener(this);
        btnMultimedia.setOnClickListener(this);
        mBtnVoice.setOnTouchListener((v1, event) -> {
            if (onChatKeyboardListener != null) {
                onChatKeyboardListener.onAudioChatClickListener(v1);
            }
            return true;
        });
        mBtnVoiceOrText.setOnClickListener(this);
    }

    @Override
    protected View inflateFunc() {
        return mInflater.inflate(R.layout.view_func_emoticon, null);
    }

    @Override
    public void reset() {
        super.reset();
        EmoticonsKeyboardUtils.closeSoftKeyboard(getContext());
        mLyKvml.hideAllFuncView();
        mBtnFace.setImageResource(R.drawable.ic_fb_emoji);
        btnSampleMessage.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.chat_icon_tint, getContext().getTheme())));
        btnMultimedia.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.chat_icon_tint, getContext().getTheme())));
        isToggleEmoji = false;
        isToggleSampleMessage = false;
        isToggleMultimedia = false;
    }

    @Override
    public void initEditView() {
        mEtChat.setOnTouchListener((v, event) -> {
            if (!mEtChat.isFocused()) {
                mEtChat.setFocusable(true);
                mEtChat.setFocusableInTouchMode(true);
            }
            return false;
        });

        mEtChat.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s)) {
                    btnSend.setVisibility(GONE);
                    mBtnVoiceOrText.setVisibility(VISIBLE);
                    mBtnMultimedia.setVisibility(VISIBLE);
                    btnSampleMessage.setVisibility(VISIBLE);
                } else {
                    mBtnMultimedia.setVisibility(GONE);
                    mBtnVoiceOrText.setVisibility(VISIBLE);
                    btnSampleMessage.setVisibility(GONE);
                    btnSend.setVisibility(VISIBLE);
                }

            }
        });
    }

    @Override
    public void onFuncChange(int key) {
        if (FUNC_TYPE_EMOTION == key) {
            mBtnFace.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.strong_primary_red, getContext().getTheme())));
        } else {
            mBtnFace.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.chat_icon_tint, getContext().getTheme())));
        }
        this.checkVoice();
    }

    @Override
    public void OnSoftClose() {
        super.OnSoftClose();
        if (mLyKvml.getCurrentFuncKey() == FUNC_TYPE_APPPS) {
            setFuncViewHeight(EmoticonsKeyboardUtils.dip2px(getContext(), APP_HEIGHT));
        }
    }

    @Override
    protected void showText() {
        mEtChat.setVisibility(VISIBLE);
        //mBtnFace.setVisibility(VISIBLE);
        //mBtnVoiceOrText.setVisibility(GONE);
        mBtnVoice.setVisibility(GONE);
    }

    @Override
    protected void showVoice() {
        //super.showVoice();
        mEtChat.setVisibility(GONE);
        //mBtnFace.setVisibility(GONE);
        mBtnVoice.setVisibility(VISIBLE);
        reset();
    }

    @Override
    protected void checkVoice() {
        if (mBtnVoice.isShown()) {
            mBtnVoiceOrText.setImageResource(R.drawable.ic_keyboard);
        } else {
            mBtnVoiceOrText.setImageResource(R.drawable.ic_microphone);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSend:
                if (onChatKeyboardListener != null) {
                    onChatKeyboardListener.onSendButtonClickListener(v);
                }
                break;
            case R.id.btn_multimedia:
                if (!isToggleMultimedia) {
                    mBtnMultimedia.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.strong_primary_red, getContext().getTheme())));
                    toggleFuncView(FUNC_TYPE_APPPS);
                    setFuncViewHeight(EmoticonsKeyboardUtils.getDefKeyboardHeight(getContext()));
                } else {
                    mBtnMultimedia.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.chat_icon_tint, getContext().getTheme())));
                    reset();
                }
                if (onChatKeyboardListener != null) {
                    onChatKeyboardListener.onChatImageClickListener(v);
                }
                isToggleMultimedia = !isToggleMultimedia;
                break;
            case R.id.btn_face:
                if (!isToggleEmoji) {
                    mBtnFace.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.strong_primary_red, getContext().getTheme())));
                    toggleFuncView(FUNC_TYPE_EMOTION);
                } else {
                    mBtnFace.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.chat_icon_tint, getContext().getTheme())));
                    reset();
                }
                isToggleEmoji = !isToggleEmoji;
                break;
            case R.id.btn_sample_message:
                if (!isToggleSampleMessage) {
                    btnSampleMessage.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.strong_primary_red, getContext().getTheme())));
                } else {
                    btnSampleMessage.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.chat_icon_tint, getContext().getTheme())));
                    reset();
                }
                isToggleSampleMessage = !isToggleSampleMessage;

                if (onChatKeyboardListener != null) {
                    onChatKeyboardListener.onSampleMessageClickListener(v);
                }
                break;
            case R.id.btn_voice_or_text:
                if (mEtChat.isShown()) {
                    mBtnVoiceOrText.setImageResource(R.drawable.ic_keyboard);
                    showVoice();
                } else {
                    showText();
                    mBtnVoiceOrText.setImageResource(R.drawable.ic_microphone);
                    EmoticonsKeyboardUtils.openSoftKeyboard(mEtChat);
                }
                break;
            default:
                break;

        }
    }

    public interface OnChatKeyboardListener {
        void onChatImageClickListener(View view);

        void onSendButtonClickListener(View view);

        void onSampleMessageClickListener(View view);

        void onAudioChatClickListener(View view);
    }
}