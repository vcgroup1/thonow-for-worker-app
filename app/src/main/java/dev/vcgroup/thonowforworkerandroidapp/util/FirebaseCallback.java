package dev.vcgroup.thonowforworkerandroidapp.util;

public interface FirebaseCallback {
    <T> void onResponse(T object);
}
