package dev.vcgroup.thonowforworkerandroidapp.util;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import dev.vcgroup.thonowforworkerandroidapp.R;

public class MyCustomAlertDialog {
    private Context mContext;
    private MaterialAlertDialogBuilder builder;
    private AlertDialog mAlertDialog;

    private View layoutView;
    private TextView tvTitle, tvMessage;
    private Button btnPositive, btnNegative;

    private MyCustomAlertDialog(Context mContext) {
        this.mContext = mContext;
        this.layoutView = LayoutInflater.from(mContext).inflate(R.layout.dialog_confirmation, null);
        this.tvTitle = this.layoutView.findViewById(R.id.custom_dialog_title);
        this.tvMessage = this.layoutView.findViewById(R.id.custom_dialog_message);
        this.btnPositive = this.layoutView.findViewById(R.id.btnPositive);
        this.btnNegative = this.layoutView.findViewById(R.id.btnNegative);
    }

    public static MyCustomAlertDialog create(Context mContext) {
        return new MyCustomAlertDialog(mContext);
    }

    private void initMyCustomAlertDialog() {
        builder = new MaterialAlertDialogBuilder(mContext);
        if (this.onKeyListener != null) {
            builder.setOnKeyListener(onKeyListener);
        }
        builder.setView(layoutView);
        mAlertDialog = builder.create();
    }

    public MyCustomAlertDialog setTitle(CharSequence title) {
        if (title != null && title.length() > 0) {
            tvTitle.setText(title);
        } else {
            tvTitle.setVisibility(View.GONE);
        }
        return this;
    }

    public MyCustomAlertDialog setMessage(CharSequence message) {
        if (message != null && message.length() > 0) {
            tvMessage.setText(message);
        } else {
            tvMessage.setVisibility(View.GONE);
        }
        return this;
    }

    public MyCustomAlertDialog setPositiveButton(@Nullable CharSequence text, View.OnClickListener onClickListener){
        btnPositive.setVisibility(View.VISIBLE);
        if (text != null && text.length() > 0) {
            btnPositive.setText(text);
        }

        if (onClickListener != null) {
            btnPositive.setOnClickListener(onClickListener);
        }
        return this;
    }

    public MyCustomAlertDialog setNegativeButton(@Nullable CharSequence text, View.OnClickListener onClickListener){
        btnNegative.setVisibility(View.VISIBLE);
        if (text != null && text.length() > 0) {
            btnNegative.setText(text);
        }

        if (onClickListener != null) {
            btnNegative.setOnClickListener(onClickListener);
        }
        return this;
    }

    /**
     * Sets the callback that will be called if a key is dispatched to the dialog.
     *
     * @return This Builder object to allow for chaining of calls to set methods
     */
    private DialogInterface.OnKeyListener onKeyListener;
    public MyCustomAlertDialog setOnKeyListener(DialogInterface.OnKeyListener onKeyListener) {
        this.onKeyListener = onKeyListener;
        return this;
    }

    public boolean isShowing() {
        return mAlertDialog != null && mAlertDialog.isShowing();
    }

    public MyCustomAlertDialog show() {
        if (!isShowing()) {
            initMyCustomAlertDialog();
            mAlertDialog.show();
        }
        return this;
    }

    public MyCustomAlertDialog dismiss() {
        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mAlertDialog.dismiss();
                }
            }, 100);
        }
        return this;
    }

}
