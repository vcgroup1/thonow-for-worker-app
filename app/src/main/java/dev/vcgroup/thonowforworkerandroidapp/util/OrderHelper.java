package dev.vcgroup.thonowforworkerandroidapp.util;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import dev.vcgroup.thonowforworkerandroidapp.data.model.OrderDetail;
import lombok.experimental.UtilityClass;

@UtilityClass
public class OrderHelper {

    /**
     * Trong phạm vi bán kính 0km – 6km, phí di chuyển là 13.500 đ
     * Trong phạm vi bán kính >6km – 10km, phí di chuyển là 22.500 đ
     * @param distance
     * @return movingExpense
     */
    public static double calculateMovingExpense(double distance) {
        if (distance >= 0 && distance <= 6) {
            return  13500;
        } else if (distance <= 10) {
            return 22500;
        }
        return 0;
    }

    /** Phí ngoài giờ = Giá dịch vụ x 20%
     * Giờ hành chính từ 8AM - 18PM
     * @param serviceFee
     * @param startDate
     * @return
     */
    public static double calculateOvertimeFee(double serviceFee, Date startDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        if (hour < 8 || hour > 18) {
            return serviceFee * 0.2;
        }
        return 0;
    }

    public static double distanceInKilometer(LatLng startLatlng, LatLng endLatlng) {
        float[] result = new float[1];
        Location.distanceBetween(startLatlng.latitude, startLatlng.longitude, endLatlng.latitude, endLatlng.longitude, result);
        return result[0]/1000;
    }

    public static double calculateOrderTotal(List<OrderDetail> items) {
        double total = 0;
        if (items != null && !items.isEmpty()) {
            total = items.stream().mapToDouble(OrderDetail::getSubtotal).sum();
        }
        return total;
    }


}
