package dev.vcgroup.thonowforworkerandroidapp.util.ewallet;

import android.content.Intent;
import android.nfc.Tag;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import dev.vcgroup.thonowforworkerandroidapp.BuildConfig;
import dev.vcgroup.thonowforworkerandroidapp.constant.epayment.VnPayParameters;
import dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.request.SendIPNRequest;
import dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.request.TransactionConfirmationRequest;
import dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.vnpay.CreatePaymentRequest;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.SequenceGenerator;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

import static dev.vcgroup.thonowforworkerandroidapp.constant.epayment.MoMoParameters.*;
import static dev.vcgroup.thonowforworkerandroidapp.constant.epayment.VnPayParameters.DOMAIN_VNPAY_SANDBOX;
import static dev.vcgroup.thonowforworkerandroidapp.constant.epayment.VnPayParameters.VNPAY_PAYMENT;

@UtilityClass
public class EPaymentUtil {
	private static final String M_MERCHANT_NAME = "TNHH ThoNOW";
	private static final String M_MERCHANT_CODE = "MOMOODQU20210526";
	//private static final String M_ORDER_ID = "NAPTIEN_WORKER";

	/*  E-Wallet : MOMO
		https://developers.momo.vn/#/docs/app_in_app?id=x%e1%bb%ad-l%c3%bd-thanh-to%c3%a1n
	 */
	public static Map<String, Object> getMomoData(long amount) {
		Map<String, Object> eventValue = new HashMap<>();
		//client Required
		eventValue.put(MERCHANT_NAME, M_MERCHANT_NAME);
		eventValue.put(MERCHANT_CODE, M_MERCHANT_CODE);
		eventValue.put(AMOUNT, amount);
		String orderId = "CM" + new SequenceGenerator().nextId();
		eventValue.put(ORDER_ID, orderId);
		eventValue.put(ORDER_LABEL, "Mã giao dịch");
		eventValue.put(USERNAME_V1, CommonUtil.currentUserUid);

		//client Optional - bill info
		eventValue.put(MERCHANT_NAME_LABEL, "Dịch vụ");
		eventValue.put(FEE, 0);
		eventValue.put(DESCRIPTION, "Thợ nạp tiền vào tài khoản");

		//client extra data
		eventValue.put(REQUEST_ID,
				M_MERCHANT_CODE + "merchant_billId_" + Calendar.getInstance().getTimeInMillis());
		eventValue.put(PARTNER_CODE, M_MERCHANT_CODE);

		eventValue.put(EXTRA, "");
		return eventValue;
	}

	public static MomoResponse getResponse(Intent data) {
		MomoResponse response = new MomoResponse();
		response.setStatus(data.getIntExtra(STATUS, -1));
		response.setData(data.getStringExtra(DATA));
		response.setPhoneNumber(data.getStringExtra(PHONE_NUMBER_V1));
		response.setMessage(data.getStringExtra(MESSAGE));
		return response;
	}

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class MomoResponse {
		int status;
		String message;
		String data;
		String phoneNumber;
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	public static String generateRSA(Map<String, Object> eventValue) throws Exception {
		Map<String, Object> rawData = new HashMap<>();
		rawData.put(PARTNER_REF_ID, eventValue.get(ORDER_ID));
		rawData.put(PARTNER_CODE, eventValue.get(PARTNER_CODE));
		rawData.put(AMOUNT, eventValue.get(AMOUNT));
		rawData.put(PARTNER_NAME, eventValue.get(PHONE_NUMBER_V1));
		rawData.put(PARTNER_TRANS_ID, "OD001");
		Gson gson = new Gson();
		String jsonStr = gson.toJson(rawData);
		Log.d("jsonStr", jsonStr);
		byte[] testByte = jsonStr.getBytes(StandardCharsets.UTF_8);
		return Encoder.encryptRSA(testByte, BuildConfig.MOMO_PUBLIC_KEY);
	}

	public static String generateVnPaySecureHash(CreatePaymentRequest request) throws Exception {
		String requestRawData = getQueryStringHash(request);
		Log.d("rawData", requestRawData);
		return Encoder.Sha256(BuildConfig.VNP_HASHSECRET + requestRawData);
	}

	public static String getPaymentUrl(CreatePaymentRequest request) {
		return DOMAIN_VNPAY_SANDBOX + VNPAY_PAYMENT + "?" + getQueryStringHash(request);
	}

	public static String generateSignRequest(TransactionConfirmationRequest request) throws NoSuchAlgorithmException,
			InvalidKeyException, UnsupportedEncodingException {
		String requestRawData = new StringBuilder()
				.append(PARTNER_CODE).append("=").append(request.getPartnerCode()).append("&")
				.append(PARTNER_REF_ID).append("=").append(request.getPartnerRefId()).append("&")
				.append(REQUEST_TYPE).append("=").append(request.getRequestType()).append("&")
				.append(REQUEST_ID).append("=").append(request.getRequestId()).append("&")
				.append(MOMO_TRANS_ID).append("=").append(request.getMomoTransId())
				.toString();
		return Encoder.signHmacSHA256(requestRawData, BuildConfig.MOMO_SECRET_KEY);
	}

	@SneakyThrows
	public String getQueryStringHash(Object o) {
		StringBuilder queryStringBuilder = new StringBuilder();
		Map<String, Object> map = new HashMap<>();

		for (Field field : o.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			map.put(field.getName(), field.get(o));
		}

		// remove key-value pair if it's null
		map.entrySet().removeIf(e -> e.getValue() == null);

		LinkedHashMap<String, Object> sortedMap = map.entrySet().stream()
				.sorted((o1, o2) -> o1.getKey().substring(4).compareTo(o2.getKey().substring(4)))
				.collect(Collectors.toMap(
						Map.Entry::getKey,
						Map.Entry::getValue,
						(oldValue, newValue) -> oldValue, LinkedHashMap::new));


		for (Map.Entry<String, Object> entry : sortedMap.entrySet()) {
			queryStringBuilder.append(entry.getKey());
			queryStringBuilder.append("=");
			queryStringBuilder.append(entry.getValue());
			queryStringBuilder.append("&");
		}
		final String queryString = queryStringBuilder.toString();
		Log.d("QueryString", queryString.substring(0, queryString.length() - 1));

		return queryString.substring(0, queryString.length() - 1);
	}


}
