package dev.vcgroup.thonowforworkerandroidapp.data.model;

import com.google.common.collect.Ordering;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.OrderHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    String id;
    OrderType type;
    Date workingDate;
    Date finishDate;
    String note;
    double movingExpense;
    double estimatedFee;
    double overtimeFee;
    double otherExpense;
    double tip;
    double total;
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference customer;
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference worker;
    List<OrderDetail> orderDetails;
    OrderStatus status;
    Address workingAddress;
    List<MaterialCost> materialCosts;
    Feedback feedback;
    List<String> images;

    public Order(OrderType orderType, Date workingDate, List<OrderDetail> orderDetails, Address workingAddress, DocumentReference customerDocRef, OrderStatus orderStatus) {
        this.type = orderType;
        this.workingDate = workingDate;
        this.orderDetails = orderDetails;
        this.workingAddress = workingAddress;
        this.customer = customerDocRef;
        this.status = orderStatus;
        this.estimatedFee = OrderHelper.calculateOrderTotal(orderDetails);
        this.overtimeFee = OrderHelper.calculateOvertimeFee(this.estimatedFee, workingDate);
        this.worker = FirebaseFirestore.getInstance().document("");
    }

    public static Order createNewOrder(OrderType orderType, Date workingDate, List<OrderDetail> orderDetails,
                                       Address workingAddress) {
        DocumentReference customerDocRef = FirebaseFirestore.getInstance().collection(CollectionConst.COLLECTION_USER).document(
                Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        return new Order(orderType, workingDate, orderDetails, workingAddress, customerDocRef, OrderStatus.ALLOCATING);
    }

    public Order progressing() {
        if(status == null)
            this.status = OrderStatus.ALLOCATING;
        this.status = status.nextStatus();
        return this;
    }

    public Order cancel() {
        this.status = OrderStatus.CANCELED;
        return this;
    }

    public static Order convertFrom(OrderPayload orderPayload) {
        Order order = new Order();
        order.setId(orderPayload.getId());
        order.setType(orderPayload.getType());
        order.setWorkingDate(CommonUtil.convertDateFrom(orderPayload.getWorkingDate()));
        order.setNote(orderPayload.getNote());
        order.setEstimatedFee(orderPayload.getEstimatedFee());

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference custDocRef = db.collection(CollectionConst.COLLECTION_USER).document(orderPayload.getCustomer());
        order.setCustomer(custDocRef);

        order.setOrderDetails(OrderDetail.convertFrom(orderPayload.getOrderDetails()));
        order.setWorkingAddress(orderPayload.getWorkingAddress());
        order.setStatus(orderPayload.getStatus());
        return order;
    }
}
