package dev.vcgroup.thonowforworkerandroidapp.data.model.fcm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FCMResult {
    private String message_id;
    private String registration_id;
    private String error;
}
