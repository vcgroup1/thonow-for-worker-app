package dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionConfirmationResponse {
	int status;
	String message;
	TransactionConfirmationData data;
	String signature;

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class TransactionConfirmationData {
		String partnerCode;
		String partnerRefId;
		String momoTransId;
		long amount;
	}
}
