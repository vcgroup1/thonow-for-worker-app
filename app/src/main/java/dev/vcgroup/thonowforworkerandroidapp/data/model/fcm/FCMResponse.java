package dev.vcgroup.thonowforworkerandroidapp.data.model.fcm;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FCMResponse {
    private long multicast_id;
    private int success;
    private int failure;
    private int canonical_ids;
    private List<FCMResult> results;
}
