package dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.request;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.Map;

import dev.vcgroup.thonowforworkerandroidapp.constant.epayment.MoMoParameters;
import dev.vcgroup.thonowforworkerandroidapp.data.model.TransactionType;
import dev.vcgroup.thonowforworkerandroidapp.util.ewallet.EPaymentUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static dev.vcgroup.thonowforworkerandroidapp.constant.epayment.MoMoParameters.*;

/*
	Doc: https://developers.momo.vn/#/docs/app_in_app?id=http-request
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentProcessingRequest {
	String partnerCode;
	String customerNumber;
	String partnerRefId;
	String appData;
	String hash;
	double version;
	int payType;
	String description;
	String extra_data;

	@RequiresApi(api = Build.VERSION_CODES.O)
	public static PaymentProcessingRequest createFrom(Map<String, Object> eventValue) throws Exception {
		if (!eventValue.isEmpty()) {
			PaymentProcessingRequest request = new PaymentProcessingRequest();
			request.setPartnerCode((String) eventValue.get(MERCHANT_CODE));
			request.setPartnerRefId((String) eventValue.get(ORDER_ID));
			request.setCustomerNumber((String) eventValue.get(PHONE_NUMBER_V1));
			request.setAppData((String) eventValue.get(APP_DATA));
			String hash = EPaymentUtil.generateRSA(eventValue);
			request.setHash(hash);
			request.setVersion(VERSION);
			request.setPayType(APP_PAY_TYPE);
			request.setDescription(TransactionType.CHARGE_MONEY.getTitle());
			request.setExtra_data("");
			return request;
		}
		return null;
	}
}
