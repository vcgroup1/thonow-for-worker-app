package dev.vcgroup.thonowforworkerandroidapp.data.model;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Calendar;
import java.util.Date;

import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transaction {
    DocumentReference from;
    double amount;
    Date timestamp;
    TransactionType type;

    public static Transaction newObject(FirebaseFirestore db, double amount, TransactionType type) {
        return new Transaction(CommonUtil.getCurrentUserRef(db),
                                amount,
                Calendar.getInstance().getTime(), type);
    }
}
