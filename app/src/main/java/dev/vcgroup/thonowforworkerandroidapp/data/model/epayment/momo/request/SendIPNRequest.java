package dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.request;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

import dev.vcgroup.thonowforworkerandroidapp.BuildConfig;
import dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.response.PaymentProcessingResponse;
import dev.vcgroup.thonowforworkerandroidapp.util.ewallet.EPaymentUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendIPNRequest {
	String partnerCode;
	String accessKey;
	long amount;
	String partnerRefId;
	String partnerTransId;
	String transType;
	String momoTransId;
	int status;
	String message;
	long responseTime;
	String storeId;
	String signature;

}
