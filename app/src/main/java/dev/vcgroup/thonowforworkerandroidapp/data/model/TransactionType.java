package dev.vcgroup.thonowforworkerandroidapp.data.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TransactionType {
    CHARGE_MONEY ("Nạp tiền"),
    EXEMPT_MONEY ("Trừ tiền chiết khấu dịch vụ");

    private String title;
}
