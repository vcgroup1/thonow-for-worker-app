package dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.vnpay;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import dev.vcgroup.thonowforworkerandroidapp.BuildConfig;
import dev.vcgroup.thonowforworkerandroidapp.data.remote.epaymentservice.EPaymentService;
import dev.vcgroup.thonowforworkerandroidapp.data.remote.epaymentservice.PaymentRetrofitClientInstance;
import dev.vcgroup.thonowforworkerandroidapp.util.SequenceGenerator;
import dev.vcgroup.thonowforworkerandroidapp.util.ewallet.EPaymentUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.ewallet.Encoder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreatePaymentRequest {
	@NonNull
	String vnp_Version;
	@NonNull
	String vnp_Command;
	@NonNull
	String vnp_TmnCode;
	@NonNull
	long vnp_Amount;
	String vnp_BankCode;
	@NonNull
	String vnp_CreateDate;
	@NonNull
	String vnp_CurrCode;
	@NonNull
	String vnp_IpAddr;
	@NonNull
	String vnp_Locale;
	@NonNull
	String vnp_OrderInfo;
	long vnp_OrderType;
	@NonNull
	String vnp_ReturnUrl;
	@NonNull
	String vnp_TxnRef;
	@NonNull
	String vnp_SecureHashType;
	@NonNull
	String vnp_SecureHash;

	@SneakyThrows
	public static CreatePaymentRequest createFrom(Context context, long amount, String bankCode) throws UnknownHostException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
		CreatePaymentRequest request = new CreatePaymentRequest();

		request.setVnp_IpAddr("0.0.0.0");
		request.setVnp_Version("2");
		request.setVnp_Command("pay");
		request.setVnp_TmnCode(BuildConfig.VNP_TMNCODE);
		request.setVnp_Amount(amount * 100);
		request.setVnp_BankCode(bankCode);

		@SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		request.setVnp_CreateDate(formatter.format(new Date()));

		request.setVnp_CurrCode("VND");
		request.setVnp_Locale("vn");
		request.setVnp_OrderInfo("Nap-tien-vao-tai-khoan");
		request.setVnp_OrderType(260000);
		String txnRef = "Ref" + new SequenceGenerator().nextId();
		request.setVnp_TxnRef(txnRef);
		String returnUrl ="https://thonowforworker.page.link/successful-payment";
		request.setVnp_ReturnUrl(returnUrl);
		String hash = EPaymentUtil.generateVnPaySecureHash(request);
		request.setVnp_SecureHash(hash);
		request.setVnp_SecureHashType("SHA256");
		return request;
	}
}
