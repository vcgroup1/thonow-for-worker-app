package dev.vcgroup.thonowforworkerandroidapp.data.local.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import dev.vcgroup.thonowforworkerandroidapp.constant.LocalDatabaseConst;
import dev.vcgroup.thonowforworkerandroidapp.data.local.database.dao.NotificationDao;
import dev.vcgroup.thonowforworkerandroidapp.data.model.fcm.FCMRemoteNotification;
import dev.vcgroup.thonowforworkerandroidapp.util.DateConverter;

@Database(entities = {FCMRemoteNotification.class}, version = 1)
@TypeConverters({DateConverter.class})
public abstract class MyDatabase extends RoomDatabase {
    public abstract NotificationDao notificationDao();

    private static volatile MyDatabase INSTANCE;

    public static MyDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MyDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context, MyDatabase.class, LocalDatabaseConst.DATABASE_NAME)
                                    .fallbackToDestructiveMigration()
                                    .build();
                }
            }
        }
        return INSTANCE;
    }
}
