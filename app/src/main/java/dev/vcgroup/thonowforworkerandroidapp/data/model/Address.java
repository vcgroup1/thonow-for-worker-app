package dev.vcgroup.thonowforworkerandroidapp.data.model;

import org.parceler.Parcel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Parcel
@AllArgsConstructor
@NoArgsConstructor
public class Address {
    double latitude;
    double longtitude;
    String address;
}
