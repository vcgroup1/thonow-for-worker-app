package dev.vcgroup.thonowforworkerandroidapp.data.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
public enum OrderType {
    QUICK_CALL ("Gọi nhanh"),
    MAKE_AN_APPOINTMENT ("Đặt lịch");

    private String title;
}
