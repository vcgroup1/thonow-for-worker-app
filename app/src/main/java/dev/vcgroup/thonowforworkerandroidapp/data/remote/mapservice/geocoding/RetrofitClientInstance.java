package dev.vcgroup.thonowforworkerandroidapp.data.remote.mapservice.geocoding;

import android.content.Context;

import dev.vcgroup.thonowforworkerandroidapp.BuildConfig;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {
    private static final String BASE_MAP_URL = "https://maps.googleapis.com/maps/api/";
    private Retrofit retrofit;
    private static RetrofitClientInstance mInstance;

    public RetrofitClientInstance(Context context) {
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(chain -> {
            Request original = chain.request();
            HttpUrl originalHttpUrl = original.url();
            HttpUrl url = originalHttpUrl.newBuilder()
                    .addQueryParameter("key", BuildConfig.MAPS_IP_API_KEY)
                    .build();
            return chain.proceed(original.newBuilder().url(url).build());
        }).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_MAP_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    public static synchronized RetrofitClientInstance getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new RetrofitClientInstance(context);
        }
        return mInstance;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
