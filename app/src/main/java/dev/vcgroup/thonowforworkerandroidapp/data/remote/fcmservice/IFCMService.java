package dev.vcgroup.thonowforworkerandroidapp.data.remote.fcmservice;

import dev.vcgroup.thonowforworkerandroidapp.data.model.OrderPayload;
import dev.vcgroup.thonowforworkerandroidapp.data.model.fcm.FCMResponse;
import dev.vcgroup.thonowforworkerandroidapp.data.model.fcm.FCMSingleSendData;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface IFCMService {
    @POST("fcm/send")
    Call<FCMResponse> sendReceivedCallWorkerRequest(@Body FCMSingleSendData<String> body);

    @POST("fcm/send")
    <T> Call<FCMResponse> sendFcmNotification(@Body FCMSingleSendData<T> body);
}
