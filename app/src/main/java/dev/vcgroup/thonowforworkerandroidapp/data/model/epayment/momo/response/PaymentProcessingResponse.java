package dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.response;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

import dev.vcgroup.thonowforworkerandroidapp.BuildConfig;
import dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.request.PaymentProcessingRequest;
import dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.request.SendIPNRequest;
import dev.vcgroup.thonowforworkerandroidapp.util.ewallet.EPaymentUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
	Read more: https://developers.momo.vn/#/docs/app_in_app?id=http-response
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentProcessingResponse {
	int status;
	String message;
	String transid;
	long amount;
	String signature;
}
