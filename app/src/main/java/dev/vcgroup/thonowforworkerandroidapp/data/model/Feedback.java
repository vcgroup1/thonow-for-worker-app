package dev.vcgroup.thonowforworkerandroidapp.data.model;

import org.parceler.Parcel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Feedback {
    float rating;
    String review;
}
