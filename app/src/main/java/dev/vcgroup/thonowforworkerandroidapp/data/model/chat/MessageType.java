package dev.vcgroup.thonowforworkerandroidapp.data.model.chat;

public enum MessageType {
    TEXT,
    IMAGE
}
