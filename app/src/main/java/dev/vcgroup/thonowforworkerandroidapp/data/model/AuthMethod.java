package dev.vcgroup.thonowforworkerandroidapp.data.model;

public enum AuthMethod {
    PHONE,
    GOOGLE,
    FACEBOOK
}
