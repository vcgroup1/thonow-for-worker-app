package dev.vcgroup.thonowforworkerandroidapp.data.model.fcm;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class FCMSingleSendData<T> {
    @NonNull
    @SerializedName("data")
    private GenericFcmData<T> data;

    @NonNull
    @SerializedName("to")
    private String to;

    @SerializedName("notification")
    private FCMNotification notification;
}
