package dev.vcgroup.thonowforworkerandroidapp.data.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TransactionMethod {
    ZALOPAY ("Ví ZaloPay"),
    VNPAY ("Ví VNPay"),
    MOMO ("Ví MoMo"),
    AIRPAY ("Ví AirPay");

    private String title;
}
