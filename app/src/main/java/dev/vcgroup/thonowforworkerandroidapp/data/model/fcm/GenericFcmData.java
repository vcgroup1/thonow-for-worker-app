package dev.vcgroup.thonowforworkerandroidapp.data.model.fcm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenericFcmData<T> {
    private String type;
    private T object;
}
