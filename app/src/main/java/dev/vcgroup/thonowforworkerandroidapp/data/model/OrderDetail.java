package dev.vcgroup.thonowforworkerandroidapp.data.model;

import android.util.Log;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@NoArgsConstructor
public class OrderDetail {
    double unitPrice;
    int quantity;
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference service;
    double subtotal;

    public OrderDetail(DocumentReference service, int quantity) {
        this.quantity = quantity;
        this.service = service;
        this.service.get()
                .addOnSuccessListener(documentSnapshot -> {
                    this.unitPrice = Objects.requireNonNull(documentSnapshot.toObject(Service.class)).getPrice();
                    this.setSubtotal(this.quantity * this.unitPrice);
                })
                .addOnFailureListener(e -> Log.d("order_detail", e.getMessage()));
    }

    public static List<OrderDetail> convertFrom(List<OrderDetailPayload> orderDetailPayloads) {
        if (orderDetailPayloads != null && !orderDetailPayloads.isEmpty()) {
            List<OrderDetail> orderDetails = new ArrayList<>();
            orderDetailPayloads.forEach(orderDetailPayload -> {
                orderDetails.add(convertFrom(orderDetailPayload));
            });
            return orderDetails;
        }
        return null;
    }

    public static OrderDetail convertFrom(OrderDetailPayload orderDetailPayload) {
        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setQuantity(orderDetailPayload.getQuantity());
        orderDetail.setUnitPrice(orderDetailPayload.getUnitPrice());
        orderDetail.setSubtotal(orderDetailPayload.getSubtotal());

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        orderDetail.setService(db.collection(CollectionConst.COLLECTION_SERVICE).document(orderDetailPayload.getService()));
        return orderDetail;
    }

    public void setSubtotal() {
        this.subtotal = this.unitPrice * this.quantity;
    }

    public void setService(DocumentReference service) {
        this.service = service;
        service.get()
                .addOnSuccessListener(documentSnapshot -> {
                    this.unitPrice = Objects.requireNonNull(documentSnapshot.toObject(Service.class)).getPrice();
                    this.setSubtotal(this.quantity * this.unitPrice);
                })
                .addOnFailureListener(e -> Log.d("order_detail", e.getMessage()));
    }
}
