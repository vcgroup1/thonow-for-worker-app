package dev.vcgroup.thonowforworkerandroidapp.data.model;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.List;
import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Parcel
@AllArgsConstructor
@NoArgsConstructor
public class User {
    String uid;
    String displayName;
    String phoneNumber;
    String email;
    String photoUrl;
    @ParcelPropertyConverter(DocumentReferenceConverterList.class)
    List<DocumentReference> favoriteWorkerList;

    public User(String uid, String displayName, String phoneNumber, String email, String photoUrl) {
        this.uid = uid;
        this.displayName = displayName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.photoUrl = photoUrl;
    }

    public static User convertFrom(FirebaseUser firebaseUser) {
        User user = new User();
        user.setUid(firebaseUser.getUid());
        user.setDisplayName(firebaseUser.getDisplayName());
        user.setEmail(firebaseUser.getEmail());
        user.setPhoneNumber(firebaseUser.getPhoneNumber());
        user.setPhotoUrl(Objects.requireNonNull(firebaseUser.getPhotoUrl()).toString());
        return user;
    }
}
