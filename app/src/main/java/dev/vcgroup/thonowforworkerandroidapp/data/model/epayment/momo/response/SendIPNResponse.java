package dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendIPNResponse {
	int status;
	String message;
	String partnerRefId;
	long amount;
	String signature;
}
