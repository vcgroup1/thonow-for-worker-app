package dev.vcgroup.thonowforworkerandroidapp.data.remote.epaymentservice;

import dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.request.PaymentProcessingRequest;
import dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.request.TransactionConfirmationRequest;
import dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.response.PaymentProcessingResponse;
import dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.response.TransactionConfirmationResponse;
import dev.vcgroup.thonowforworkerandroidapp.data.model.fcm.FCMResponse;
import dev.vcgroup.thonowforworkerandroidapp.data.model.fcm.FCMSingleSendData;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface EPaymentService {

    @POST("pay/app")
    Call<PaymentProcessingResponse> processPayment(@Body PaymentProcessingRequest request);

    @POST("pay/confirm")
    Call<TransactionConfirmationResponse> processPaymentConfirmation(@Body TransactionConfirmationRequest request);

    @GET("/")
    Call<String> getPublicIpAddress();


}
