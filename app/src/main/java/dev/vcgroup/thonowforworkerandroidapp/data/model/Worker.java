package dev.vcgroup.thonowforworkerandroidapp.data.model;

import android.net.Uri;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Parcel
@AllArgsConstructor
@NoArgsConstructor
public class Worker {
    String id;
    String displayName;
    String email;
    String phoneNumber;
    String photoUrl;
    String residentAddress;
    Address address;
    double balanceMoney;
    String idNumber;
    String promoCode;
    float rating;
    boolean isMale;
    boolean isOnline;
    boolean isActive;
    boolean isVerified;
    @ParcelPropertyConverter(DocumentReferenceConverterList.class)
    List<DocumentReference> serviceTypeList;
    List<CreditCardInfo> creditCardInfos;
    IdentityCard idCard;

    public static Worker convertFrom(FirebaseUser firebaseUser) {
        Worker worker = new Worker();
        worker.setId(firebaseUser.getUid());
        worker.setDisplayName(firebaseUser.getDisplayName());
        worker.setEmail(firebaseUser.getEmail());
        worker.setPhoneNumber(firebaseUser.getPhoneNumber());
        Uri photoUri = firebaseUser.getPhotoUrl();
        if (photoUri != null) {
            worker.setPhotoUrl(photoUri.toString());
        }
        worker.setRating(0f);
        worker.setActive(true);
        worker.setVerified(false);
        worker.setMale(true);
        worker.setBalanceMoney(0f);
        return worker;
    }

    @Parcel
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class IdentityCard {
        String frontIdCardUrl;
        String backIdCardUrl;
    }
}
