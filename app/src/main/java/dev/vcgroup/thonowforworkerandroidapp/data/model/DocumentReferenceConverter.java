package dev.vcgroup.thonowforworkerandroidapp.data.model;

import android.os.Parcel;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.parceler.ParcelConverter;

public class DocumentReferenceConverter implements ParcelConverter<DocumentReference> {
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    public void toParcel(DocumentReference input, Parcel parcel) {
        if (input != null) {
            parcel.writeString(input.getPath());
        }
    }

    @Override
    public DocumentReference fromParcel(Parcel parcel) {
        if (parcel != null) {
            return db.document(parcel.readString());
        }
        return null;
    }
}
