package dev.vcgroup.thonowforworkerandroidapp.data.model;

import android.util.Log;

import com.google.firebase.firestore.DocumentReference;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.List;
import java.util.Objects;

import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@NoArgsConstructor
public class OrderDetailPayload {
    double unitPrice;
    int quantity;
    String service;
    double subtotal;
}
