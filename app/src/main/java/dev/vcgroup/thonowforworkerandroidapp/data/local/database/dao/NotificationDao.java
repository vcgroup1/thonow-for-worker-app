package dev.vcgroup.thonowforworkerandroidapp.data.local.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import dev.vcgroup.thonowforworkerandroidapp.constant.LocalDatabaseConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.fcm.FCMRemoteNotification;

@Dao
public interface NotificationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(FCMRemoteNotification notification);

    @Query("SELECT * FROM " + LocalDatabaseConst.TBL_NOTIFICATION)
    List<FCMRemoteNotification> getAll();

    @Delete
    void delete(FCMRemoteNotification notification);
}
