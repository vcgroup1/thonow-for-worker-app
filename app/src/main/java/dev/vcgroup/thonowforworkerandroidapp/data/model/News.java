package dev.vcgroup.thonowforworkerandroidapp.data.model;

import org.parceler.Parcel;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Parcel
@AllArgsConstructor
@NoArgsConstructor
public class News {
    String id;
    String title;
    Date createdAt;
    String thumbnail;
    String content;
}
