package dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.request;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

import dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.response.PaymentProcessingResponse;
import dev.vcgroup.thonowforworkerandroidapp.util.SequenceGenerator;
import dev.vcgroup.thonowforworkerandroidapp.util.ewallet.EPaymentUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionConfirmationRequest {
	@NonNull
	String partnerCode;
	@NonNull
	String partnerRefId;
	@NonNull
	String requestType;
	@NonNull
	String requestId;
	@NonNull
	String momoTransId;
	@NonNull
	String signature;
	String customerNumber;
	String description;

	public static TransactionConfirmationRequest createFrom(PaymentProcessingRequest request,
	                                                   PaymentProcessingResponse response) throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
		if(request != null && response != null) {
			TransactionConfirmationRequest confirmationRequest = new TransactionConfirmationRequest();
			confirmationRequest.setPartnerCode(request.getPartnerCode());
			confirmationRequest.setPartnerRefId(request.partnerRefId);
			//	Loại request, có 2 giá trị:
			//		- Xác nhận giao dịch: capture
			//		- Hủy bỏ giao dịch: revertAuthorize
			confirmationRequest.setRequestType("capture");
			confirmationRequest.setRequestId(response.getTransid());
			confirmationRequest.setMomoTransId(response.getTransid());
			confirmationRequest.setCustomerNumber(request.getCustomerNumber());
			confirmationRequest.setDescription(request.getDescription());
			confirmationRequest.setSignature(EPaymentUtil.generateSignRequest(confirmationRequest));
			return confirmationRequest;
		}
		return null;
	}
}
