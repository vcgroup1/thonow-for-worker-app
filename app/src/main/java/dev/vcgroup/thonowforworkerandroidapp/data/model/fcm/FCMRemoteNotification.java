package dev.vcgroup.thonowforworkerandroidapp.data.model.fcm;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Date;

import dev.vcgroup.thonowforworkerandroidapp.util.DateConverter;

@Entity(tableName = "TBL_NOTIFICATION")
public class FCMRemoteNotification {
    @ColumnInfo
    @PrimaryKey
    @NonNull
    @TypeConverters(DateConverter.class)
    private Date timestamp;

    @ColumnInfo
    private String title;

    @ColumnInfo
    private String text;

    @NonNull
    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(@NonNull Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public FCMRemoteNotification(@NonNull Date timestamp, String title, String text) {
        this.timestamp = timestamp;
        this.title = title;
        this.text = text;
    }
}
