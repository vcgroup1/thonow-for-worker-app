package dev.vcgroup.thonowforworkerandroidapp.data.local.preference;

import android.content.Context;
import android.content.SharedPreferences;

import static dev.vcgroup.thonowforworkerandroidapp.constant.SessionManagerConst.KEY_IS_FINDING_WORKER;

public class SessionManager {
    private static final String SESSION_USER_SESSION = "user_session";
    private SharedPreferences userSession;
    private SharedPreferences.Editor editor;
    private Context context;

    public SessionManager(Context ctx) {
        this.context = ctx;
        this.userSession = context.getSharedPreferences(SESSION_USER_SESSION, Context.MODE_PRIVATE);
        this.editor = userSession.edit();
    }

    public void saveIsFindingWorker(boolean isFinding) {
        editor.putBoolean(KEY_IS_FINDING_WORKER, isFinding);
        editor.commit();
    }

    public boolean getIsFindingWorker() {
        return userSession.getBoolean(KEY_IS_FINDING_WORKER, false);
    }

    public void logout() {
        editor.remove(KEY_IS_FINDING_WORKER);
        editor.commit();
    }
}
