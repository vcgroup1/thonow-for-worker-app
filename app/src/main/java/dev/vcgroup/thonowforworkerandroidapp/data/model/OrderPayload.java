package dev.vcgroup.thonowforworkerandroidapp.data.model;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.OrderHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderPayload {
    String id;
    OrderType type;
    long workingDate;
    String note;
    double estimatedFee;
    String customer;
    List<OrderDetailPayload> orderDetails;
    Address workingAddress;
    OrderStatus status;

    public static OrderPayload convertFrom(Map<String, String> data) {
        OrderPayload orderPayload = new OrderPayload();
        orderPayload.setId(data.get("id"));
        orderPayload.setType(OrderType.valueOf(data.get("type")));
        orderPayload.setWorkingDate(Long.parseLong(Objects.requireNonNull(data.get("workingDate"))));
        orderPayload.setNote(data.get("note"));
        orderPayload.setEstimatedFee(Double.parseDouble(data.getOrDefault("estimatedFee", "0")));
        orderPayload.setCustomer(data.get("customer"));
        orderPayload.setStatus(OrderStatus.valueOf(data.get("status")));
//        Gson gson = new Gson();
//        orderPayload.setWorkingAddress(data.get("workingAddress"));
        return orderPayload;
    }
}
