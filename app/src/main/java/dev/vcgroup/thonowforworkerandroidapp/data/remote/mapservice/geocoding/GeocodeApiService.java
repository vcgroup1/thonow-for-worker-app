package dev.vcgroup.thonowforworkerandroidapp.data.remote.mapservice.geocoding;

import com.google.gson.JsonObject;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GeocodeApiService {
    public static final String GEOCODE_API_URL = "geocode/json";

    @GET(GEOCODE_API_URL)
    Call<JsonObject> getFormattedAddress(@Query("latlng") String latlng);

}
