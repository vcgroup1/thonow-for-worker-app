package dev.vcgroup.thonowforworkerandroidapp.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CallWorkerConst {
    public static final String RECEIVED_CALL_WORKER_REQUEST_CODE = "received_call_worker_request";
    public static final String IS_FROM_RECEIVED_CALL_WORKER_REQUEST_CODE = "isFromReceiveCallWorkerRequest";
    public static final double CALL_WORKER_FEE = 10000;
}
