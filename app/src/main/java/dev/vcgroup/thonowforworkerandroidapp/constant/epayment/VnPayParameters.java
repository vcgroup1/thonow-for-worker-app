package dev.vcgroup.thonowforworkerandroidapp.constant.epayment;

import lombok.experimental.UtilityClass;

@UtilityClass
public class VnPayParameters {
    public static final String DOMAIN_VNPAY_SANDBOX = "http://sandbox.vnpayment.vn/";
    public static final String VNPAY_PAYMENT = "paymentv2/vpcpay.html";
}
