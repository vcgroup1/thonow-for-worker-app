package dev.vcgroup.thonowforworkerandroidapp.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CollectionConst {
    public static final String COLLECTION_USER = "users";
    public static final String COLLECTION_USER_GEOLOCATION = "user_geolocations";
    public static final String LIB_COUNTRY_CODE = "lib_country_code";
    public static final String COLLECTION_OFFER = "offers";
    public static final String COLLECTION_NEWS = "news";
    public static final String COLLECTION_SERVICE = "services";
    public static final String COLLECTION_SERVICE_TYPE = "service_type";
    public static final String COLLECTION_WORKER = "workers";
    public static final String COLLECTION_WORKER_GEOLOCATION = "worker_geolocations";
    public static final String COLLECTION_TOKEN = "tokens";
    public static final String COLLECTION_ORDER = "orders";
    public static final String COLLECTION_MESSAGE = "messages";
    public static final String COLLECTION_LAST_MESSAGE = "last_message";
    public static final String COLLECTION_TRANSACTION_HISTORY = "transaction_history";
}
