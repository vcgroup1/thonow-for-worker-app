package dev.vcgroup.thonowforworkerandroidapp.constant;

import android.Manifest;

import lombok.experimental.UtilityClass;

@UtilityClass
public class RequestCodeConst {
    public static final int TAKE_PHOTO_CODE = 111;
    public static final int ASK_PERMISSION_CODE = 300;
    public static final int SELECT_SERVICE_TYPE_REQ_CODE = 400;
    public static final int SELECT_SERVICE_REQ_CODE = 401;
    public static final int SELECT_FAVORITE_WORKER_REQ_CODE = 401;
    public static final int CANCEL_ORDER_REQ_CODE = 402;
    public static final int SEE_DETAIL_ORDER_REQ_CODE = 403;
    public static final int CONFIRMATION_OF_PAYMENT_REQ_CODE = 404;
    public static final int MATERIAL_COST_MANAGEMENT_REQ_CODE = 405;
    public static final int TAKE_PHOTO_FRONT_ID_CODE = 406;
    public static final int TAKE_PHOTO_BACK_ID_CODE = 407;
    public static final int ORDER_DETAIL_MANAGEMENT_REQ_CODE = 408;
    public static final int CHARGE_MONEY_REQ_CODE = 409;
}
