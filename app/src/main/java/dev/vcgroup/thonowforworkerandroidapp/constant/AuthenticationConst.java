package dev.vcgroup.thonowforworkerandroidapp.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class AuthenticationConst {
    public static int RESULT_CODE_GOOGLE_SIGN_IN = 999;
}
