package dev.vcgroup.thonowforworkerandroidapp.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class SessionManagerConst {
    public static final String KEY_IS_FINDING_WORKER = "is_finding_worker";
    public static final String KEY_TOKEN = "token";
}
