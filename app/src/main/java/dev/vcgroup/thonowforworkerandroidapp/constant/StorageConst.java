package dev.vcgroup.thonowforworkerandroidapp.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class StorageConst {
    public static final String AVATAR_FOLDER = "images_avatar";
    public static final String WORKER_AVATAR_FOLDER = "images_worker_avatar";
    public static final String CHAT_FOLDER = "images_chat";
    public static final String FOLDER_CONSERVATION_PHOTO = "images_convervation";
    public static final String FOLDER_ORDERS_PHOTO = "images_order";
}
