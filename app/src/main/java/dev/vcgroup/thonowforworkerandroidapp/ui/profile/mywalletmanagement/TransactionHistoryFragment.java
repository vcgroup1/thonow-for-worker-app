package dev.vcgroup.thonowforworkerandroidapp.ui.profile.mywalletmanagement;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Transaction;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentTransactionHistoryBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.mywalletmanagement.adapter.TransactionHistoryAdapter;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.SpacesItemDecoration;

public class TransactionHistoryFragment extends Fragment {
    private static final String TAG = TransactionHistoryFragment.class.getSimpleName();
    private FragmentTransactionHistoryBinding binding;
    private TransactionHistoryAdapter adapter;
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_transaction_history, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupRvTransactionHistory();
    }

    private void setupRvTransactionHistory() {
        adapter = new TransactionHistoryAdapter();
        binding.rvTransactionHistory.setAdapter(adapter);
        binding.rvTransactionHistory.addItemDecoration(new SpacesItemDecoration(20));

        populateData();
    }

    private void populateData() {
        db.collection(CollectionConst.COLLECTION_TRANSACTION_HISTORY)
                .whereEqualTo("from", CommonUtil.getCurrentUserRef(db))
                .orderBy("timestamp", Query.Direction.DESCENDING)
                .addSnapshotListener((value, error) -> {
                    if (error != null) {
                        Log.w(TAG, "Listen failed!", error);
                        return;
                    }

                    if (value != null) {
                        adapter.update(value.toObjects(Transaction.class));
                        checkEmpty();
                    }
                });
    }

    private void checkEmpty() {
        if (adapter.getItemCount() == 0) {
            binding.rvTransactionHistory.setVisibility(View.INVISIBLE);
            binding.emptySection.setVisibility(View.VISIBLE);
        } else {
            binding.rvTransactionHistory.setVisibility(View.VISIBLE);
            binding.emptySection.setVisibility(View.INVISIBLE);
        }
    }
}