package dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.basicpersonalinformation;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentBasicProfileStepTwoBinding;

public class BasicProfileStepTwoFragment extends Fragment implements Step {
    private static final String TAG = BasicProfileStepTwoFragment.class.getSimpleName();
    private FragmentBasicProfileStepTwoBinding binding;

    public static BasicProfileStepTwoFragment newInstance() {
        return new BasicProfileStepTwoFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_basic_profile_step_two, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        Log.d(TAG, "Qua step 2");
        return null;
    }

    @Override
    public void onSelected() {
        Log.d(TAG, "Vô step này");
    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
}