package dev.vcgroup.thonowforworkerandroidapp.ui.profile.editprofile;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcels;

import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.constant.StorageConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Worker;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivityEditProfileBinding;
import dev.vcgroup.thonowforworkerandroidapp.databinding.DialogChangeProfilePictureBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.MainScreenViewModel;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.FileUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.ImageLoadingUtil;
import gun0912.tedimagepicker.builder.TedImagePicker;
import gun0912.tedimagepicker.builder.listener.OnSelectedListener;

import static dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst.TAKE_PHOTO_CODE;
import static dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil.getInputText;

public class EditProfileActivity extends AppCompatActivity {
    private static final String TAG = EditProfileActivity.class.getSimpleName();
    private ActivityEditProfileBinding binding;
    private MainScreenViewModel profileViewModel;
    private Worker worker;
    private Uri avatarUri;
    private FirebaseFirestore db;
    private FirebaseUser currentUser;
    private StorageReference mStorageRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile);
        profileViewModel = ViewModelProviders.of(this).get(MainScreenViewModel.class);
        if(getIntent().getExtras() != null) {
            worker = Parcels.unwrap(getIntent().getParcelableExtra("currentUser"));
            profileViewModel.setCurrentUser(worker);
        }
        binding.setItem(profileViewModel);
        binding.setLifecycleOwner(this);

        setupToolbar();

        View.OnClickListener changeAvatar = view -> showChangeProfilePictureDialog();
        binding.btnChangeProfilePicture.setOnClickListener(changeAvatar);
        binding.profileAvatar.setOnClickListener(changeAvatar);

       binding.btnSaveProfile.setOnClickListener(v -> {
            worker = validateForm();
            if (worker != null) {
                updateUserProfile();
            } else {
                Snackbar.make(binding.getRoot(), "Cập nhật thông tin cá nhân thất bại", BaseTransientBottomBar.LENGTH_SHORT);
            }
        });

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        db = FirebaseFirestore.getInstance();
    }

    private Worker validateForm() {
        String displayName = getInputText(binding.tipName);
        String residentAddress = getInputText(binding.tipResidentsAddress);

        if (displayName.isEmpty()) {
            binding.tipName.setError("Vui lòng nhập họ tên");
            return null;
        }

        worker.setDisplayName(getInputText(binding.tipName));
        worker.setResidentAddress(residentAddress);

        return worker;
    }

    private void setupToolbar() {
        Toolbar mToolbar = binding.mToolbar;
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            onBackPressed();
            return true;
        }
        return false;
    }

    private void showChangeProfilePictureDialog() {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        DialogChangeProfilePictureBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_change_profile_picture, (ViewGroup) getWindow().getDecorView().getRootView(), false);

        builder.setView(binding.getRoot());
        AlertDialog dialog = builder.create();
        dialog.show();

        binding.changeAvatarTakeAPictureSelection.setOnClickListener(view -> TedPermission.with(EditProfileActivity.this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        takeAPictureFromCamera();
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        Snackbar.make(binding.getRoot(), "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                    }
                })
                .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                .setPermissions(Manifest.permission.CAMERA)
                .check());

        binding.changeAvatarChooseExistingPhotoSelection.setOnClickListener(view -> TedPermission.with(EditProfileActivity.this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        pickImageFromGallery();
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        Snackbar.make(binding.getRoot(), "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                    }
                })
                .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                .setPermissions(Manifest.permission.CAMERA)
                .check());
    }

    private void takeAPictureFromCamera() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
        }
    }

    private void pickImageFromGallery() {
        TedImagePicker.with(this)
                .title("Chọn ảnh")
                .buttonText("Xong")
                .buttonBackground(R.color.strong_red_darker)
                .buttonTextColor(android.R.color.white)
                .start(uri -> {
                    avatarUri = uri;
                    ImageLoadingUtil.displayImage(binding.profileAvatar, uri.toString());
                    binding.profileAvatar.setScaleType(ImageView.ScaleType.CENTER_CROP);
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK && data != null) {
            avatarUri = FileUtil.getImageUri(EditProfileActivity.this, (Bitmap) data.getExtras().get("data"));
            Bitmap avatarBitmap = (Bitmap) data.getExtras().get("data");
            binding.profileAvatar.setImageBitmap(Bitmap.createScaledBitmap(avatarBitmap,
                    (int) getResources().getDimension(R.dimen.edit_profile_avatar),
                    (int) getResources().getDimension(R.dimen.edit_profile_avatar), true));
        }
    }

    private void uploadPicture(Uri imageUri) {
        if(mStorageRef == null) {
            mStorageRef = FirebaseStorage.getInstance().getReference();
        }
        StorageReference avatarRef = mStorageRef.child(StorageConst.WORKER_AVATAR_FOLDER + "/" + worker.getId());
        avatarRef.putFile(imageUri)
                .addOnSuccessListener(taskSnapshot -> {
                    Task<Uri> task = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                    task.addOnSuccessListener(uri -> {
                        updateUserProfileDb(uri);
                        updateUserProfilePictureAuth(uri);
                    });
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar.make(binding.getRoot(), "Đổi hình đại xảy ra lỗi. Thử lại!", BaseTransientBottomBar.LENGTH_SHORT);
                    }
                });
    }

    private void updateUserProfilePictureAuth(Uri avatarUri) {
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setPhotoUri(avatarUri)
                .build();

        Objects.requireNonNull(currentUser).updateProfile(profileUpdates)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "User profile updated.");
                    }
                });
    }

    private void updateUserProfileDb(Uri avatarUri) {
        db.collection(CollectionConst.COLLECTION_USER)
                .document(worker.getId())
                .update("photoUrl", avatarUri.toString())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        finish();
                    }
                })
                .addOnFailureListener(e -> Log.d("updateUserAvatar", e.getMessage()));
    }

    private void updateUserProfile() {
        db.collection(CollectionConst.COLLECTION_WORKER)
                .document(worker.getId())
                .update(
                        "displayName", worker.getDisplayName(),
                        "residentAddress", worker.getResidentAddress()
                ).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        if (avatarUri != null) {
                            uploadPicture(avatarUri);
                        } else {
                            finish();
                        }

                    } else {
                        Log.d(TAG, "updateUserProfile::" + task.getException());
                        Snackbar.make(binding.getRoot() ,"Cập nhật thông tin cá nhân thất bại", BaseTransientBottomBar.LENGTH_SHORT);
                    }
                });
    }

    private void updateUserProfileAuth() {
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(getInputText(binding.tipName))
                .build();

        Objects.requireNonNull(currentUser).updateProfile(profileUpdates)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "User profile updated.");
                    }
                });
    }
}