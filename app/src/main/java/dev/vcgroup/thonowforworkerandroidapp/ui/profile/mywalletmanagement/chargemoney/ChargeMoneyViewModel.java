package dev.vcgroup.thonowforworkerandroidapp.ui.profile.mywalletmanagement.chargemoney;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import dev.vcgroup.thonowforworkerandroidapp.data.model.TransactionMethod;

public class ChargeMoneyViewModel extends ViewModel {
    private MutableLiveData<Long> amount = new MutableLiveData<>( (long) 0);
    private MutableLiveData<TransactionMethod> transactionMethod =
            new MutableLiveData<>(TransactionMethod.MOMO);

    public MutableLiveData<TransactionMethod> getTransactionMethod() {
        return transactionMethod;
    }

    public void setTransactionMethod(TransactionMethod transactionMethod) {
        this.transactionMethod.setValue(transactionMethod);
    }

    public MutableLiveData<Long> getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount.setValue(amount);
    }
}
