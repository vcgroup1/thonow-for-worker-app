package dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import org.parceler.Parcels;

import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Worker;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivityProfileManagementBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.basicpersonalinformation.BasicPersonalInformationActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.selectservicetype.SelectServiceTypeActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.submitonbardingdocument.SubmitOnboardingDocumentActivity;

public class ProfileManagementActivity extends AppCompatActivity {
    private ActivityProfileManagementBinding binding;
    private Worker currentWorker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile_management);
        setupToolbar();

        if (getIntent() != null) {
            currentWorker = Parcels.unwrap(getIntent().getParcelableExtra("currentUser"));
        }

        binding.sectionSubmitDocument.setOnClickListener(v -> {
            startActivity(new Intent(this, SubmitOnboardingDocumentActivity.class));
        });

        binding.sectionRegisterServiceType.setOnClickListener(v -> {
            Intent intent = new Intent(this, SelectServiceTypeActivity.class);
            intent.putExtra("currentUser", Parcels.wrap(currentWorker));
            startActivity(intent);
        });

        binding.sectionBasicProfile.setOnClickListener(v -> {
            startActivity(new Intent(this, BasicPersonalInformationActivity.class));
        });
    }

    private void setupToolbar() {
        Toolbar toolbar = binding.topAppBar.mToolbar;
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setTitle(null);
        binding.topAppBar.tvTitleToolbar.setText("Xác minh hồ sơ");
        getSupportActionBar().show();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }
}