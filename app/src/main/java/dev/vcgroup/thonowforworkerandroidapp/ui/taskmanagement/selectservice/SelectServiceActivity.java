package dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.selectservice;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import org.parceler.Parcels;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Service;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivitySelectServiceBinding;
import dev.vcgroup.thonowforworkerandroidapp.util.SpacesItemDecoration;

public class SelectServiceActivity extends AppCompatActivity implements ServiceAdapter.OnServiceListener {
    private static final String TAG = SelectServiceActivity.class.getSimpleName();
    private ActivitySelectServiceBinding binding;
    private ServiceAdapter adapter;
    private String selectedServiceType, selectedService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_service);
        binding.setLifecycleOwner(this);

        if (getIntent() != null) {
            selectedServiceType = getIntent().getStringExtra("serviceTypeId");
            selectedService = getIntent().getStringExtra("serviceId");
        }

        setupToolbar();
        setupServiceRecyclerView();
        setupServiceSearchView();
    }

    private void setupToolbar() {
        Toolbar toolbar = binding.topAppBar.mToolbar;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        binding.topAppBar.tvTitleToolbar.setText("Dịch vụ");
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupServiceSearchView() {
        binding.svService.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    private void setupServiceRecyclerView() {
        Log.d(TAG, "selectedServiceType: " + selectedServiceType + "; selectedService: " + selectedService);
        adapter = new ServiceAdapter(this, selectedServiceType, selectedService);
        binding.rvService.setAdapter(adapter);
        binding.rvService.addItemDecoration(new SpacesItemDecoration(15));
    }

    @Override
    public void onServiceClickedListener(int position) {
        Service service = adapter.getItem(position);
        if (service != null) {
            Intent intent =  new Intent();
            intent.putExtra("selectedService", Parcels.wrap(service));
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}