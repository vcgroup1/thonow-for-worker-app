package dev.vcgroup.thonowforworkerandroidapp.ui.homepage;

import androidx.databinding.DataBindingUtil;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcels;

import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst;
import dev.vcgroup.thonowforworkerandroidapp.databinding.BsdInviteFriendDialogBinding;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentHomePageBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.MainScreenActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.MainScreenViewModel;
import dev.vcgroup.thonowforworkerandroidapp.ui.homepage.adapter.NewsAdapter;
import dev.vcgroup.thonowforworkerandroidapp.ui.homepage.adapter.OfferAdapter;
import dev.vcgroup.thonowforworkerandroidapp.ui.homepage.newsdetail.NewsDetailActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.homepage.offerdetail.OfferDetailActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.editprofile.EditProfileActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.jobhistory.JobHistoryActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.mywalletmanagement.MyWalletManagementActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.ProfileManagementActivity;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.ImageLoadingUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.MyCustomAlertDialog;

import static dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst.CHARGE_MONEY_REQ_CODE;

public class HomePageFragment extends Fragment implements OfferAdapter.OnOfferListener, NewsAdapter.OnNewsListener {
    private FragmentHomePageBinding binding;
    private MainScreenViewModel mViewModel;

    // Offer Section
    private OfferAdapter offerAdapter;

    // News Adapter
    private NewsAdapter newsAdapter;
    private BottomSheetDialog bsdInviteFriend;

    public static HomePageFragment newInstance() {
        return new HomePageFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home_page, container, false);
        mViewModel = ViewModelProviders.of(requireActivity()).get(MainScreenViewModel.class);
        binding.setItem(mViewModel);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mViewModel != null) {
            mViewModel.getCurrentUser().observe(requireActivity(), worker -> {
                if (worker == null) {
                    mViewModel.getCurrentWorker();
                    return;
                }

                if (binding.profileDisplayName.getText().toString().isEmpty()) {
                    binding.profileDisplayName.setText(worker.getDisplayName());
                }

                ImageLoadingUtil.displayImage(binding.profileAvatar, worker.getPhotoUrl());

                if (binding.tvBalanceMoney.getText().toString().isEmpty()) {
                    binding.tvBalanceMoney.setText(CommonUtil.convertCurrencyText(worker.getBalanceMoney()) + " đ");
                }

                // setup isActive section
                if (worker.isVerified()) {
                    binding.tvVerified.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#E9F6E2")));
                    binding.tvVerified.setTextColor(Color.parseColor("#5cb85c"));
                } else {
                    binding.tvVerified.setBackgroundTintList(ColorStateList.valueOf(getActivity().getResources().getColor(R.color.pale_orange, requireActivity().getTheme())));
                    binding.tvVerified.setTextColor(getActivity().getResources().getColor(R.color.strong_primary_red, requireActivity().getTheme()));
                }

                registerForClickListener();
            });

        }

        setupOfferSection();
        setupNewsSection();

        mViewModel.getCurrentUser().observe(requireActivity(), worker -> {
            if (worker != null) {
                binding.setItem(mViewModel);
            }
        });
    }

    private void registerForClickListener() {
        binding.functionSection.btnChargeMoney.setOnClickListener(v -> {
            startActivityForResult(new Intent(requireActivity(),
                            MyWalletManagementActivity.class), CHARGE_MONEY_REQ_CODE,
                    ActivityOptions.makeCustomAnimation(getActivity(), android.R.anim.fade_in,
                            android.R.anim.fade_out).toBundle());
        });

        binding.functionSection.btnJobHistory.setOnClickListener(v -> {
            startActivityForResult(new Intent(requireActivity(), JobHistoryActivity.class), CHARGE_MONEY_REQ_CODE, ActivityOptions.makeCustomAnimation(getActivity(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
        });

        binding.functionSection.btnRevenueStatistics.setOnClickListener(v -> {
            startActivityForResult(new Intent(requireActivity(), MyWalletManagementActivity.class), CHARGE_MONEY_REQ_CODE, ActivityOptions.makeCustomAnimation(getActivity(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
        });

        binding.functionSection.btnInviteFriends.setOnClickListener(v -> {
            // ((MainScreenActivity) requireActivity()).navView.setSelectedItemId(R.id.navigation_profile);
            BsdInviteFriendDialogBinding inviteBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.bsd_invite_friend_dialog, (ViewGroup) v.getRootView(), false);
            bsdInviteFriend = new BottomSheetDialog(requireActivity());
            bsdInviteFriend.setContentView(inviteBinding.getRoot());

            String id = Objects.requireNonNull(mViewModel.getCurrentUser().getValue()).getId();

            inviteBinding.txtGuiLink.setOnClickListener(v1 -> {
                generateContentLink(id);
            });

            inviteBinding.txtGuiMa.setOnClickListener(v1 -> {
                String contentShare = "Mã giới thiệu khi cài đặt ứng dụng ThoNOW Dành cho thợ: " + id;
                shareLink(contentShare);
            });

            inviteBinding.txtBack.setOnClickListener(v1 -> {
                bsdInviteFriend.dismiss();
            });

            bsdInviteFriend.show();
        });

        binding.tvVerified.setOnClickListener(v -> {
            Intent intent = new Intent(requireActivity(), ProfileManagementActivity.class);
            intent.putExtra("currentUser", Parcels.wrap(mViewModel.getCurrentUser().getValue()));
            requireActivity().startActivity(intent, ActivityOptions.makeCustomAnimation(getActivity(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
        });

        binding.profileAvatar.setOnClickListener(v -> {
            ((MainScreenActivity) requireActivity()).navView.setSelectedItemId(R.id.navigation_profile);
        });

        binding.tvBalanceMoney.setOnClickListener(v -> {
            startActivityForResult(new Intent(requireActivity(), MyWalletManagementActivity.class), CHARGE_MONEY_REQ_CODE,
                    ActivityOptions.makeCustomAnimation(getActivity(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
        });

        binding.functionSection.btnSettings.setOnClickListener(v -> {
            ((MainScreenActivity) requireActivity()).navView.setSelectedItemId(R.id.navigation_profile);
        });

        binding.profileSection.setOnClickListener(v -> {
            startActivity(new Intent(requireActivity(), EditProfileActivity.class), ActivityOptions.makeCustomAnimation(getActivity(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
        });
    }

    private void generateContentLink(String id) {
        String sharelinktext  = "https://thonowforworker.page.link/?"+
                "link=https://www.thonowforworker.com/invite-friend/" + id + "/" +
                "&apn=" + requireActivity().getPackageName()+
                "&st=" + "Link-gioi-thieu-cua-toi";

        FirebaseDynamicLinks.getInstance()
                .createDynamicLink()
                .setLongLink(Uri.parse(sharelinktext))
                .buildShortDynamicLink()
                .addOnSuccessListener(shortDynamicLink -> {
                    // Short link created
                    Uri shortLink = shortDynamicLink.getShortLink();
                    Uri flowchartLink = shortDynamicLink.getPreviewLink();

                    if (shortLink != null) {
                        String contentShare = "Mời bạn sử dụng ứng dụng nhận gọi thợ ThoNOW App Dành cho thợ\n"
                                + "Link download ứng dụng:\n"
                                + shortLink.toString();

                        shareLink(contentShare);
                    }

                })
                .addOnFailureListener(e -> {
                    Log.d("dynamicLink", e.getMessage());
                    bsdInviteFriend.dismiss();
                    MyCustomAlertDialog dialog = MyCustomAlertDialog.create(requireActivity());
                    dialog.setTitle("Thông báo")
                            .setMessage("Tạo link giới thiệu xảy ra lỗi. Vui lòng kiểm tra internet và thử lại!")
                            .setPositiveButton("Đã hiểu", v -> dialog.dismiss())
                            .show();
                });
    }

    private void shareLink(String contentShare) {
        // Send app dialog
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, contentShare);
        startActivity(Intent.createChooser(intent, "Mời bạn bè sử dụng"));
        bsdInviteFriend.dismiss();
    }

    private void setupNewsSection() {
        newsAdapter = new NewsAdapter(this, NewsAdapter.NewsAdapterType.NEWS_ITEM);
        ViewPager2 vpNews = binding.newsSection.vpNews;
        vpNews.setAdapter(newsAdapter);
        vpNews.setClipToPadding(false);
        vpNews.setClipChildren(false);
        vpNews.setOffscreenPageLimit(3);
        vpNews.getChildAt(0).setOverScrollMode(View.OVER_SCROLL_NEVER);

        binding.newsSection.newsIndicator.setViewPager2(vpNews);

        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        compositePageTransformer.addTransformer(new MarginPageTransformer(10));
        vpNews.setPageTransformer(compositePageTransformer);
    }

    private void setupOfferSection() {
        offerAdapter = new OfferAdapter(this);
        ViewPager2 vpOffer = binding.offerSection.vpOffer;
        vpOffer.setAdapter(offerAdapter);
        vpOffer.setClipToPadding(false);
        vpOffer.setClipChildren(false);
        vpOffer.setOffscreenPageLimit(2);
        vpOffer.getChildAt(0).setOverScrollMode(View.OVER_SCROLL_NEVER);

        binding.offerSection.offerIndicator.setViewPager2(vpOffer);

        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        compositePageTransformer.addTransformer(new MarginPageTransformer(20));
        vpOffer.setPageTransformer(compositePageTransformer);
    }

    @Override
    public void onOfferClickedListener(int position) {
        Intent intent = new Intent(getActivity(), OfferDetailActivity.class);
        intent.putExtra("offer", Parcels.wrap(offerAdapter.getItem(position)));
        startActivity(intent, ActivityOptions.makeCustomAnimation(getActivity(), android.R.anim.slide_in_left, android.R.anim.slide_out_right).toBundle());
    }

    @Override
    public void onNewsClickedListener(int position) {
        Intent intent = new Intent(getActivity(), NewsDetailActivity.class);
        intent.putExtra("news", Parcels.wrap(newsAdapter.getItem(position)));
        startActivity(intent, ActivityOptions.makeCustomAnimation(getActivity(), android.R.anim.slide_in_left, android.R.anim.slide_out_right).toBundle());
    }
}