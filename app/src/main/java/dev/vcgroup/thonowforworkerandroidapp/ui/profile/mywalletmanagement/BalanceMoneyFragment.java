package dev.vcgroup.thonowforworkerandroidapp.ui.profile.mywalletmanagement;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Worker;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentBalanceMoneyBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.mywalletmanagement.chargemoney.ChargeMoneyActivity;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;

public class BalanceMoneyFragment extends Fragment {
    private static final String TAG = BalanceMoneyFragment.class.getSimpleName();
    private FragmentBalanceMoneyBinding binding;
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }
    public static BalanceMoneyFragment newInstance() {
        return new BalanceMoneyFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_balance_money, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CommonUtil.getCurrentUserRef(db).get()
                .addOnSuccessListener(documentSnapshot -> {
                    Worker worker = documentSnapshot.toObject(Worker.class);
                    double balanceMoney = worker.getBalanceMoney();
                    if (balanceMoney > 0) {
                        binding.sectionChargeMoney.setVisibility(View.VISIBLE);
                        binding.tvBalanceMoney.setText(CommonUtil.convertCurrencyText(worker.getBalanceMoney()) + " đ");
                    } else {
                        binding.sectionChargeMoneyEmpty.setVisibility(View.VISIBLE);
                    }
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));

        binding.btnChargeMoneyNow.setOnClickListener(v -> {
            Intent intent = new Intent(requireActivity(), ChargeMoneyActivity.class);
            requireActivity().startActivityForResult(intent, RequestCodeConst.CHARGE_MONEY_REQ_CODE);
        });
    }
}