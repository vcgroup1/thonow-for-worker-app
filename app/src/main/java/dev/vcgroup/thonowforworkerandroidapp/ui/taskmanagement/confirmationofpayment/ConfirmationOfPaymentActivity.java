package dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.confirmationofpayment;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.parceler.Parcels;

import java.util.Date;
import java.util.List;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Order;
import dev.vcgroup.thonowforworkerandroidapp.data.model.OrderStatus;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Service;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Transaction;
import dev.vcgroup.thonowforworkerandroidapp.data.model.TransactionType;
import dev.vcgroup.thonowforworkerandroidapp.data.model.User;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Worker;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivityConfirmationOfPaymentBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.audiocall.callscreen.CallScreenActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.message.chatwindow.ChatActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.thankyou.ThankYouActivity;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.MyCustomAlertDialog;

import static dev.vcgroup.thonowforworkerandroidapp.constant.AudioCallConst.OUT_GOING_CALL;
import static dev.vcgroup.thonowforworkerandroidapp.constant.CommonConst.CALL_PERMISSIONS;

public class ConfirmationOfPaymentActivity extends AppCompatActivity {
    private static final String TAG = ConfirmationOfPaymentActivity.class.getSimpleName();
    private ActivityConfirmationOfPaymentBinding binding;
    private Order order;
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_confirmation_of_payment);

        if (getIntent().getExtras() != null) {
            order = Parcels.unwrap(getIntent().getParcelableExtra("order"));
            if (order != null) {
                binding.setOrder(order);

                order.getCustomer().get()
                        .addOnSuccessListener(documentSnapshot -> {
                            binding.setCustomer(documentSnapshot.toObject(User.class));
                        })
                        .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
            }

        }

        binding.btnConfirm.setOnClickListener(v -> {
            OrderStatus status = order.getStatus().nextStatus();
            Date finishDate = null;
            if (status == OrderStatus.COMPLETED) {
                finishDate = new Date();
            }
            db.collection(CollectionConst.COLLECTION_ORDER)
                    .document(order.getId())
                    .update(
                            "status", status,
                            "total", order.getTotal() * 0.83,
                            "finishDate", finishDate
                    )
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            double exemptMoney = order.getTotal() * 0.17;
                            insertTransactionHistory(exemptMoney);
                            subtractBalanceMoney(exemptMoney);
                            updateOrderCount();
                        } else {
                            MyCustomAlertDialog dialog = MyCustomAlertDialog.create(this);
                            dialog.setTitle("Thông báo")
                                    .setMessage("Có gì đó không ổn. Vui lòng thử lại!")
                                    .setPositiveButton("Thử lại", v1 -> {
                                        dialog.dismiss();
                                        binding.btnConfirm.performClick();
                                    })
                                    .setNegativeButton("Huỷ", v1 -> dialog.dismiss());
                        }
                    });
        });

        binding.btnCallCustomer.setOnClickListener(v -> doCallToCustomer());
        binding.ibnCall.setOnClickListener(v -> doCallToCustomer());

        binding.ibnChat.setOnClickListener(v -> {
            Intent intent = new Intent(this, ChatActivity.class);
            intent.putExtra("orderId", order.getId());
            intent.putExtra("customerId", order.getCustomer().getId());
            startActivity(intent, ActivityOptions.makeCustomAnimation(this, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
        });
    }

    private void updateOrderCount() {
        order
                .getOrderDetails()
                .get(0)
                .getService()
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    Service service = documentSnapshot.toObject(Service.class);
                    if (service != null) {
                        DocumentReference serviceTypeDocRef = db.collection(CollectionConst.COLLECTION_SERVICE_TYPE)
                                .document(service.getId());
                                serviceTypeDocRef.get()
                                .addOnSuccessListener(documentSnapshot1 -> {
                                    if (documentSnapshot1 != null) {
                                        Long totalOrders = documentSnapshot1.getLong("totalOrders");
                                        serviceTypeDocRef.update(
                                                "totalOrders", totalOrders
                                        );
                                    }

                                })
                                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
                    }
                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, e.getMessage());
                });
    }

    private void subtractBalanceMoney(double subtractMoney) {
        DocumentReference currentUserDocRef = CommonUtil.getCurrentUserRef(db);
        currentUserDocRef
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    double balanceMoney = documentSnapshot.toObject(Worker.class).getBalanceMoney();
                    currentUserDocRef.
                            update("balanceMoney", balanceMoney - subtractMoney)
                            .addOnSuccessListener(command -> {
                                Intent intent = new Intent(this, ThankYouActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent, ActivityOptions.makeCustomAnimation(this, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
                                setResult(RESULT_OK);
                                finish();
                            })
                            .addOnFailureListener(e -> {
                                Log.d(TAG, e.getMessage());
                            });
                });
    }

    private void doCallToCustomer() {
        if (!TedPermission.isGranted(this, CALL_PERMISSIONS)) {
            TedPermission.with(this)
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            callToCustomer(order);
                        }

                        @Override
                        public void onPermissionDenied(List<String> deniedPermissions) {
                            Snackbar.make(binding.getRoot(), "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                        }
                    })
                    .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                    .setPermissions(CALL_PERMISSIONS)
                    .check();
        } else {
            callToCustomer(order);
        }
    }

    private void callToCustomer(Order order) {
        order.getCustomer()
                .get()
                .addOnSuccessListener(documentSnapshot1 -> {
                    Intent intent = new Intent(this, CallScreenActivity.class);
                    intent.putExtra("customer", Parcels.wrap(documentSnapshot1.toObject(User.class)));
                    intent.putExtra(OUT_GOING_CALL, true);
                    startActivity(intent, ActivityOptions.makeCustomAnimation(this, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, e.getMessage());
                    notifyFailedAudioCall();
                });

    }

    private void notifyFailedAudioCall() {
        MyCustomAlertDialog dialog = MyCustomAlertDialog.create(this);
        dialog.setTitle("Thông báo")
                .setMessage("Gọi thoại cho khách hàng xảy ra lỗi. Hãy thử lại xem sao.")
                .setPositiveButton("Thử lại", v -> {
                    dialog.dismiss();
                })
                .show();
    }

    private void insertTransactionHistory(double exemptMoney) {
        Transaction transaction = Transaction.newObject(db, exemptMoney,
                TransactionType.EXEMPT_MONEY);
        db.collection(CollectionConst.COLLECTION_TRANSACTION_HISTORY)
                .add(transaction);
    }
}