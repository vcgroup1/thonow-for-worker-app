package dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.thankyou;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import org.parceler.Parcels;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Worker;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivityThankYouWorkerBinding;

public class ThankYouActivity extends AppCompatActivity {
    private ActivityThankYouWorkerBinding binding;

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_thank_you_worker);

        binding.btnFinish.setOnClickListener(v -> {
           finish();
        });
    }
}
