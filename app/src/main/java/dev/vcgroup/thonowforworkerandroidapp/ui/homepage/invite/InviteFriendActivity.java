package dev.vcgroup.thonowforworkerandroidapp.ui.homepage.invite;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import dev.vcgroup.thonowforworkerandroidapp.R;

public class InviteFriendActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friend);
    }
}