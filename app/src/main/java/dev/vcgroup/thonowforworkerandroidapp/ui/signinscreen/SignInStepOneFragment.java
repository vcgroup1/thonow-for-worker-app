package dev.vcgroup.thonowforworkerandroidapp.ui.signinscreen;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.kusu.loadingbutton.LoadingButton;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.CountryInfo;
import dev.vcgroup.thonowforworkerandroidapp.databinding.BsdChooseCountryCodeBinding;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentSignInStepOneBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.loginscreen.adapter.CountryCodeAdapter;
import dev.vcgroup.thonowforworkerandroidapp.util.AuthenticationUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.FragmentUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.MyCustomAlertDialog;
import lombok.SneakyThrows;

public class SignInStepOneFragment extends Fragment implements CountryCodeAdapter.OnCountryCodeListener {
    private static final String REGISTER_STEP_TWO_TAG = SignInStepTwoFragment.class.getSimpleName();
    private static final String TAG = SignInStepOneFragment.class.getSimpleName();
    private SignInViewModel signInViewModel;
    private FragmentSignInStepOneBinding binding;
    private BottomSheetDialog changeCountryBsd;
    private CountryCodeAdapter countryCodeAdapter;
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in_step_one, container, false);
        signInViewModel = ViewModelProviders.of(requireActivity()).get(SignInViewModel.class);
        binding.setSignInViewModel(signInViewModel);
        binding.setLifecycleOwner(getActivity());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        signInViewModel.setSelectedCountryCode(new CountryInfo("Vietnam", "VN", "84"));

        LoadingButton btnResendCode = binding.btnResendCode;
        btnResendCode.setOnClickListener(v -> {
            btnResendCode.showLoading();
            String phoneNumber = signInViewModel.getPhoneNumber().getValue();
            db.collection(CollectionConst.COLLECTION_WORKER)
                    .whereEqualTo("phoneNumber", phoneNumber)
                    .get()
                    .addOnSuccessListener(queryDocumentSnapshots -> {
                        if (queryDocumentSnapshots.isEmpty()) {
                            if (validateForm(phoneNumber)) {
                                signInViewModel.setPhoneNumber(phoneNumber);
                                FragmentUtil.replaceFragment(requireActivity(), R.id.register_fragment_container,
                                        new SignInStepTwoFragment(), getArguments(), REGISTER_STEP_TWO_TAG, true);
                            }
                        } else {
                            MyCustomAlertDialog dialog = MyCustomAlertDialog.create(requireActivity());
                            dialog.setTitle("Thông báo")
                                    .setMessage("Số điện thoại này đã đăng ký. Vui lòng thử lại số điện thoại khác.")
                                    .setPositiveButton("Đóng", v1 -> {
                                        dialog.dismiss();
                                    })
                                    .show();
                        }
                        btnResendCode.hideLoading();
                    })
                    .addOnFailureListener(e -> {
                        Log.d(TAG, e.getMessage());
                        btnResendCode.hideLoading();
                    });
        });

        binding.dropDownCountry.setOnClickListener(v -> {
            BsdChooseCountryCodeBinding chooseCountryCodeBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.bsd_choose_country_code, (ViewGroup) v.getRootView(), false);
            changeCountryBsd = new BottomSheetDialog(requireActivity());
            changeCountryBsd.setContentView(chooseCountryCodeBinding.getRoot());

            countryCodeAdapter = new CountryCodeAdapter(db, this);
            chooseCountryCodeBinding.rvCountries.addItemDecoration(new DividerItemDecoration(requireActivity(), DividerItemDecoration.VERTICAL));
            chooseCountryCodeBinding.rvCountries.setItemAnimator(new DefaultItemAnimator());
            chooseCountryCodeBinding.rvCountries.setAdapter(countryCodeAdapter);
            changeCountryBsd.show();

            chooseCountryCodeBinding.svCountryCode.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    countryCodeAdapter.getFilter().filter(query);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    countryCodeAdapter.getFilter().filter(newText);
                    return true;
                }
            });
        });
    }

    @SneakyThrows
    private boolean validateForm(String phoneNumber) {
        if (phoneNumber == null || phoneNumber.isEmpty()) {
            signInViewModel.errPhoneNumber.set("Vui lòng nhập số điện thoại");
        } else {
            if (AuthenticationUtil.isValidNumber(phoneNumber, signInViewModel.selectedCountryCode.getValue().getAlp2Code())) {
                return true;
            } else {
                signInViewModel.errPhoneNumber.set("Vui lòng nhập số điện thoại hợp lệ");
            }
        }
        return false;
    }

    @Override
    public void onCountryCodeClickedListener(int position) {
        signInViewModel.setSelectedCountryCode(countryCodeAdapter.getItem(position));
        new Handler().postDelayed(() -> {
            changeCountryBsd.cancel();
        }, 1000);
    }
}
