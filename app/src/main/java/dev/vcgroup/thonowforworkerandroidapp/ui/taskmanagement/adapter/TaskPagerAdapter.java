package dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import org.jetbrains.annotations.NotNull;

import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.taskbycalendar.TaskByCalendarFragment;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.taskbylist.TaskByListFragment;

public class TaskPagerAdapter extends FragmentStateAdapter {

    public TaskPagerAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @NotNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 1: return TaskByListFragment.newInstance();
            case 0:
            default: return TaskByCalendarFragment.newInstance();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
