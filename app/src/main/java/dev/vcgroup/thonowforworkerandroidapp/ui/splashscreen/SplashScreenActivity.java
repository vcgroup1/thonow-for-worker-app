package dev.vcgroup.thonowforworkerandroidapp.ui.splashscreen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.User;
import dev.vcgroup.thonowforworkerandroidapp.ui.MainScreenActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.walkthroughscreen.WalkthroughActivity;

public class SplashScreenActivity extends AppCompatActivity {
    private static final String TAG = SplashScreenActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser == null) {
            startActivity(new Intent(this, WalkthroughActivity.class));
            finish();
        } else {
            Log.d(TAG, currentUser.toString());
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection(CollectionConst.COLLECTION_WORKER)
                    .whereEqualTo("email", currentUser.getEmail())
                    .get()
                    .addOnSuccessListener(queryDocumentSnapshots -> {
                        List<User> result = queryDocumentSnapshots.toObjects(User.class);
                        if (!result.isEmpty()) {
                            startActivity(new Intent(this, MainScreenActivity.class));
                        } else {
                            startActivity(new Intent(this, WalkthroughActivity.class));
                        }
                        finish();
                    })
                    .addOnFailureListener(e -> {
                        Log.d(TAG, e.getMessage());
                    });
        }
    }
}