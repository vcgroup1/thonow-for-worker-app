package dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;

import dev.vcgroup.thonowforworkerandroidapp.data.model.MaterialCost;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ItemMaterialCostBinding;

public class MaterialCostAdapter extends RecyclerView.Adapter<MaterialCostAdapter.MaterialCostVH> {
    private static final String TAG = MaterialCostAdapter.class.getSimpleName();
    private List<MaterialCost> list;
    private AsyncListDiffer<MaterialCost> mDiffer;
    private DiffUtil.ItemCallback<MaterialCost> mCallback = new DiffUtil.ItemCallback<MaterialCost>() {
        @Override
        public boolean areItemsTheSame(@NonNull MaterialCost oldItem, @NonNull MaterialCost newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull MaterialCost oldItem, @NonNull MaterialCost newItem) {
            return oldItem.equals(newItem);
        }
    };
    private OnMaterialCostListener onMaterialCostListener;

    public MaterialCostAdapter(List<MaterialCost> list, OnMaterialCostListener onMaterialCostListener) {
        this.list = list;
        this.onMaterialCostListener = onMaterialCostListener;
        this.mDiffer = new AsyncListDiffer<>(this, mCallback);
        update(list);
    }

    @NonNull
    @Override
    public MaterialCostVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MaterialCostVH(ItemMaterialCostBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false), onMaterialCostListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MaterialCostVH holder, int position) {
        MaterialCost materialCost = getItemAt(position);
        holder.bindItem(materialCost);
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public MaterialCost getItemAt(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    public void update(List<MaterialCost> newList) {
        mDiffer.submitList(newList);
    }

    public void addOrUpdateItem(MaterialCost materialCost) {
        List<MaterialCost> list = new ArrayList<>(mDiffer.getCurrentList());
        if(!updateItem(materialCost)) {
            list.add(materialCost);
            update(list);
        }
    }

    public List<MaterialCost> getCurrentList()   {
        return mDiffer.getCurrentList();
    }

    public void deleteItem(int position) {
        List<MaterialCost> list = new ArrayList<>(mDiffer.getCurrentList());
        if (!list.isEmpty()) {
            list.remove(position);
            update(list);
        }
    }

    private boolean updateItem(MaterialCost materialCost) {
        List<MaterialCost> list = new ArrayList<>(mDiffer.getCurrentList());
        int index = list.indexOf(materialCost);
        if (index >= 0) {
            list.set(index, materialCost);
            update(list);
            return true;
        }
        return false;
    }

    public static class MaterialCostVH extends RecyclerView.ViewHolder {
        private ItemMaterialCostBinding binding;
        private OnMaterialCostListener onMaterialCostListener;

        public MaterialCostVH(@NonNull ItemMaterialCostBinding binding, OnMaterialCostListener onMaterialCostListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.onMaterialCostListener = onMaterialCostListener;

            this.binding.getRoot().setOnClickListener(v -> {
                if (onMaterialCostListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        this.onMaterialCostListener.onRemoveMaterialCost(v, position);
                    }
                }
            });
        }

        public void bindItem(MaterialCost materialCost) {
            this.binding.setItem(materialCost);
            this.binding.executePendingBindings();
        }
    }

    public interface OnMaterialCostListener {
        void onRemoveMaterialCost(View view, int position);
    }
}
