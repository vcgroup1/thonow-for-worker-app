package dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.DiffUtil.ItemCallback;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.List;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.data.model.MaterialCost;
import dev.vcgroup.thonowforworkerandroidapp.data.model.OrderDetail;
import dev.vcgroup.thonowforworkerandroidapp.util.ImageLoadingUtil;
import lombok.Getter;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {
    private List<String> listImageUrl;
    private OnImageListener onImageListener;
    private AsyncListDiffer<String> mDiffer;
    private DiffUtil.ItemCallback<String> mCallback = new ItemCallback<String>() {
        @Override
        public boolean areItemsTheSame(@NonNull String oldItem, @NonNull String newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull String oldItem, @NonNull String newItem) {
            return oldItem.equals(newItem);
        }
    };

    public ImageAdapter(List<String> listImageUrl, OnImageListener onImageListener) {
        this.listImageUrl = listImageUrl;
        this.onImageListener = onImageListener;
        this.mDiffer = new AsyncListDiffer<>(this, mCallback);
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ImageViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_detail_image, parent, false), onImageListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        String photoUrl = getItemAt(position);
        ImageLoadingUtil.displayImage(holder.getImageView(), photoUrl, 150, 100);
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public String getItemAt(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    public void update(List<String> newList) {
        mDiffer.submitList(newList);
    }

    public List<String> getItems() {
        return mDiffer.getCurrentList();
    }

    public void addtem(String photoUrl) {
        List<String> list = new ArrayList<>(mDiffer.getCurrentList());
        if (photoUrl != null && !photoUrl.isEmpty()) {
            list.add(photoUrl);
            update(list);
            notifyItemInserted(list.size() - 1);
        }
    }

    public void deleteItem(int position) {
        List<String> list = new ArrayList<>(mDiffer.getCurrentList());
        if (!list.isEmpty()) {
            list.remove(position);
            update(list);
        }
    }

    @Getter
    public class ImageViewHolder extends RecyclerView.ViewHolder {
        private RoundedImageView imageView;
        private ImageView ivClear;
        private OnImageListener onImageListener;

        public ImageViewHolder(@NonNull View itemView, OnImageListener onImageListener) {
            super(itemView);
            this.onImageListener = onImageListener;
            imageView = itemView.findViewById(R.id.iv_image);
            ivClear = itemView.findViewById(R.id.iv_clear);
            imageView.setOnClickListener(view -> {
                if (onImageListener != null) {
                    int position = getAdapterPosition();
                    if(position!= RecyclerView.NO_POSITION)
                        onImageListener.onImageClickListener(position);
                }
            });

            ivClear.setOnClickListener(view -> {
                if (onImageListener != null) {
                    int position = getAdapterPosition();
                    if(position!= RecyclerView.NO_POSITION)
                        onImageListener.onImageRemoveClickListener(position);
                }
            });

        }
    }

    public interface OnImageListener {
        void onImageClickListener(int position);
        void onImageRemoveClickListener(int position);
    }
}
