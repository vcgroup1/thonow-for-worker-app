package dev.vcgroup.thonowforworkerandroidapp.ui.forgotpasswordscreen;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthSettings;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentForgotPasswordStepTwoBinding;
import dev.vcgroup.thonowforworkerandroidapp.util.AuthenticationUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.FragmentUtil;

public class ForgotPasswordStepTwoFragment extends Fragment {
    private static final String KEY_VERIFICATION_ID = "key_verification_id";
    private FragmentForgotPasswordStepTwoBinding binding;
    private ForgotPasswordViewModel mViewModel;
    private String phoneNumber, strVerificationId;
    private static FirebaseAuth mAuth;

    static {
        mAuth = FirebaseAuth.getInstance();
    }

    private TextView btnResendCode;
    private CountDownTimer countDownTimer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forgot_password_step_two, container, false);
        mViewModel = ViewModelProviders.of(requireActivity()).get(ForgotPasswordViewModel.class);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        phoneNumber = AuthenticationUtil.formatNumberPhone(mViewModel.getPhoneNumberProto().getValue(), PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL).trim();
        sendVerificationCode(phoneNumber);
        binding.inputForgotPwdInput.setOtpCompletionListener(this::verifyVerificationCode);

        btnResendCode = binding.btnResendCode;
        countDownTimer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                String secondString = "";
                long second = millisUntilFinished / 1000;
                if (second < 10) {
                    secondString = "0" + second;
                } else {
                    secondString = second + "";
                }
                binding.tvCountDownTimer.setText("0:" + secondString);
            }

            @Override
            public void onFinish() {
                countDownTimer.cancel();
                binding.countDownTimerSection.setVisibility(View.GONE);
                btnResendCode.setVisibility(View.VISIBLE);
            }
        };
        countDownTimer.start();
            
        btnResendCode.setOnClickListener(view1 -> {
            sendVerificationCode(phoneNumber);
            binding.countDownTimerSection.setVisibility(View.VISIBLE);
            btnResendCode.setVisibility(View.GONE);
            countDownTimer.start();
        });

    }

    private void sendVerificationCode(String phoneNumber) {
        PhoneAuthOptions phoneAuthOptions = PhoneAuthOptions.newBuilder(mAuth)
                .setPhoneNumber(phoneNumber)
                .setTimeout(60L, TimeUnit.SECONDS)
                .setActivity(requireActivity())
                .setCallbacks(new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                        String code = phoneAuthCredential.getSmsCode();
                        if (code != null) {
                            binding.inputForgotPwdInput.setText(code);
                            verifyVerificationCode(code);
                        }
                    }

                    @Override
                    public void onVerificationFailed(@NonNull FirebaseException e) {
                        Snackbar.make(binding.getRoot(), "Mã xác thực không chính xác", BaseTransientBottomBar.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                        super.onCodeSent(s, forceResendingToken);
                        strVerificationId = s;
                    }
                })
                .requireSmsValidation(false)
                .build();
        PhoneAuthProvider.verifyPhoneNumber(phoneAuthOptions);
    }

    private void verifyVerificationCode(String code) {
        try {
            String phoneNumber = "+84968958053";
            String smsCode = "190599";

            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
            FirebaseAuthSettings firebaseAuthSettings = firebaseAuth.getFirebaseAuthSettings();

            firebaseAuthSettings.setAutoRetrievedSmsCodeForPhoneNumber(phoneNumber, smsCode);

            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(strVerificationId, code);
            signInWithPhoneAuthCredential(credential);
        } catch (Exception e) {
            Snackbar.make(binding.getRoot(), "Mã xác nhận không hợp lệ. Thử lại", BaseTransientBottomBar.LENGTH_SHORT).show();
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential phoneAuthCredential) {
        mAuth.signInWithCredential(phoneAuthCredential)
                .addOnCompleteListener(requireActivity(), task -> {
                    if (task.isSuccessful()) {
                        FragmentUtil.replaceFragment(getActivity(), R.id.forgot_password_fragment_container, new ForgotPasswordStepThreeFragment(),
                                null, ForgotPasswordStepTwoFragment.class.getSimpleName(), false);
                    } else {
                        String message = "Có lỗi xảy ra. Vui lòng thử lại sau!";
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            binding.inputForgotPwdInput.setError("Mã xác nhận không hợp lệ!");
                        }
                        Snackbar.make(binding.getRoot(), message, BaseTransientBottomBar.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (strVerificationId == null && savedInstanceState != null) {
            onViewStateRestored(savedInstanceState);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_VERIFICATION_ID, strVerificationId);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            strVerificationId = savedInstanceState.getString(KEY_VERIFICATION_ID);
        }
    }
}
