package dev.vcgroup.thonowforworkerandroidapp.ui.profile.bankingaccountmanagement;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.CreditCardInfo;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ItemCreditCardInfoBinding;


public class BankingAccountAdapter extends RecyclerView.Adapter<BankingAccountAdapter.CreditCardVH> {
    private static final String TAG = BankingAccountAdapter.class.getSimpleName();
    private List<CreditCardInfo> creditCardInfos = new ArrayList<>();
    private AsyncListDiffer<CreditCardInfo> mDiffer;
    private OnCreditCardInfoListener onCreditCardInfoListener;
    private DiffUtil.ItemCallback<CreditCardInfo> diffCallback = new DiffUtil.ItemCallback<CreditCardInfo>() {
        @Override
        public boolean areItemsTheSame(@NonNull @NotNull CreditCardInfo oldItem, @NonNull @NotNull CreditCardInfo newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull @NotNull CreditCardInfo oldItem, @NonNull @NotNull CreditCardInfo newItem) {
            return oldItem.getAccountNumber().equals(newItem.getAccountNumber());
        }
    };
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }

    public BankingAccountAdapter(List<CreditCardInfo> creditCardInfos, OnCreditCardInfoListener onCreditCardInfoListener) {
        this.creditCardInfos = creditCardInfos;
        this.mDiffer = new AsyncListDiffer<>(this, diffCallback);
        this.onCreditCardInfoListener = onCreditCardInfoListener;
    }

    @NonNull
    @Override
    public CreditCardVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CreditCardVH(ItemCreditCardInfoBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false), onCreditCardInfoListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CreditCardVH holder, int position) {
        CreditCardInfo creditCardInfo = getItem(position);
        holder.bind(creditCardInfo);
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public CreditCardInfo getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    public List<CreditCardInfo> getCurrentList() {
        return mDiffer.getCurrentList() == null ? new ArrayList<>() : mDiffer.getCurrentList();
    }

    public void addOrUpdateItem(CreditCardInfo creditCardInfo) {
        List<CreditCardInfo> list = new ArrayList<>(getCurrentList());
        if(!updateItem(creditCardInfo)) {
            list.add(creditCardInfo);
            update(list);
            updateCurrentBankAccountList();
        }
    }

    public void deleteItem(int position) {
        List<CreditCardInfo> list = new ArrayList<>(getCurrentList());
        if (!list.isEmpty()) {
            list.remove(position);
            update(list);
            updateCurrentBankAccountList();
        }
    }

    private void updateCurrentBankAccountList() {
        db.collection(CollectionConst.COLLECTION_WORKER)
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .update(
                        "creditCardInfos", getCurrentList()
                )
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Added Item!!");
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
    }

    private boolean updateItem(CreditCardInfo creditCardInfo) {
        List<CreditCardInfo> list = new ArrayList<>(getCurrentList());
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountNumber().equals(creditCardInfo.getAccountNumber())){
                list.set(i, creditCardInfo);
                notifyItemChanged(i);
                return true;
            }
        }
        return false;
    }


    public void update(List<CreditCardInfo> newList) {
        mDiffer.submitList(newList);
    }

    public class CreditCardVH extends RecyclerView.ViewHolder {
        private ItemCreditCardInfoBinding binding;

        public CreditCardVH(@NonNull ItemCreditCardInfoBinding binding, OnCreditCardInfoListener onCreditCardInfoListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.getRoot().setOnClickListener(v -> {
                if (onCreditCardInfoListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        onCreditCardInfoListener.onCreditCardInfoClickedListener(v, position);
                    }
                }
            });
        }

        public void bind(CreditCardInfo creditCardInfo) {
            binding.setItem(creditCardInfo);
        }
    }

    public interface OnCreditCardInfoListener {
        void onCreditCardInfoClickedListener(View view, int position);
    }
}
