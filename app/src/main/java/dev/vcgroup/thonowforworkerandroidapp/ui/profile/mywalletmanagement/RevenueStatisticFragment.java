package dev.vcgroup.thonowforworkerandroidapp.ui.profile.mywalletmanagement;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Order;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentRevenueStatisticBinding;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;

public class RevenueStatisticFragment extends Fragment {
    private static final String TAG = RevenueStatisticFragment.class.getSimpleName();
    private FragmentRevenueStatisticBinding binding;
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }
    private final int[] colors = new int[] {
            Color.rgb(137, 230, 81),
            Color.rgb(240, 240, 30),
            Color.rgb(89, 199, 250),
            Color.rgb(250, 104, 104),
            Color.parseColor("#CF5002"),
            Color.parseColor("#61D31E"),
            Color.parseColor("#FF0FA1"),
            Color.parseColor("#0FC790"),
            Color.parseColor("#185F63"),
            Color.parseColor("#ECDF3A"),
            Color.parseColor("#3DF1D9"),
            Color.parseColor("#14D3F6")
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_revenue_statistic, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        db.collection(CollectionConst.COLLECTION_ORDER)
                .whereEqualTo("worker", CommonUtil.getCurrentUserRef(db))
                .whereNotEqualTo("finishDate", null)
                .addSnapshotListener((value, error) -> {
                    if (error != null) {
                        Log.d(TAG, "Listen failed");
                        return;
                    }

                    if (value != null) {
                        List<Order> results = value.toObjects(Order.class);

                        setData(binding.revenueChart, results);
                        setupChart(binding.revenueChart);

                    }
                });
    }

    private double calculateTotalForWorker(List<Order> orders) {
        return orders.stream().mapToDouble(Order::getTotal).sum();
    }

    private List<Order> filterOrdersByMonth(List<Order> orders, int month) {
        if (orders != null && !orders.isEmpty()) {
            return orders.stream().filter(order -> {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(order.getFinishDate());
                int orderMonth = calendar.get(Calendar.MONTH);
                return month == orderMonth;
            }).collect(Collectors.toList());
        }
        return null;
    }

    private void setData(HorizontalBarChart chart, List<Order> orders) {
        float barWidth = .8f;

        ArrayList<BarEntry> values = new ArrayList<>();

        for (int i = 0; i < 12; i++) {
            List<Order> ordersByMonth = filterOrdersByMonth(orders, i);
            if (ordersByMonth != null && !ordersByMonth.isEmpty()) {
                values.add(new BarEntry(i + 1, (float) (calculateTotalForWorker(ordersByMonth))));
            } else {
                values.add(new BarEntry(i + 1, 0));
            }
        }

        BarDataSet set1;

        if (chart.getData() != null &&
                chart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) chart.getData().getDataSetByIndex(0);
            set1.setColors(colors);
            set1.setValues(values);
            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(values, "Doanh thu theo tháng");
            set1.setColors(colors);
            set1.setDrawIcons(false);
            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setBarWidth(barWidth);
            chart.setData(data);
        }
        set1.setDrawValues(true);

    }

    private void setupChart(HorizontalBarChart chart) {
        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

            }

            @Override
            public void onNothingSelected() {

            }
        });

        XAxis xl = chart.getXAxis();
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        //xl.setTypeface(tfLight);
        xl.setDrawAxisLine(true);
        xl.setDrawGridLines(false);
        xl.setGranularity(1f);
        xl.setLabelCount(12);
        xl.setAvoidFirstLastClipping(true);
        xl.setAxisMaximum(12);
        xl.setAxisMinimum(1);
        xl.setLabelCount(12,true);
        final List<String> xAxisLabel = Arrays.asList("T1", "T2", "T3", "T4", "T5", "T6", "T7",
                "T8", "T9", "T10", "T11", "T12");

        xl.setValueFormatter(new ValueFormatter() {
            @Override
            public String getAxisLabel(float value, AxisBase axis) {
                int index = (int) Math.round(value) - 1;
                if (index >= 0) {
                    return xAxisLabel.get(index);
                }
                return null;
            }
        });

        YAxis yl = chart.getAxisLeft();
        yl.setDrawAxisLine(true);
        yl.setDrawGridLines(false);
        yl.setSpaceTop(15f);
        yl.setAxisMinimum(1f);

        YAxis yr = chart.getAxisRight();
        yr.setDrawAxisLine(true);
        yr.setDrawGridLines(false);
        yr.setAxisMinimum(1f);

        chart.setFitBars(true);
        chart.animateY(2500);

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setFormSize(8f);
        l.setXEntrySpace(4f);

        chart.setDrawBarShadow(false);
        chart.setDrawValueAboveBar(true);
        chart.getDescription().setEnabled(false);
        chart.setMaxVisibleValueCount(12);
        chart.setPinchZoom(false);
        chart.setDragEnabled(true);
        chart.setDrawGridBackground(false);
        chart.setAutoScaleMinMaxEnabled(true);
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);
        chart.setTouchEnabled(true);
        chart.setPinchZoom(true);
        chart.setViewPortOffsets(0f, 0f, 0f, 0f);
        chart.setVisibleXRangeMaximum(12);
        chart.setExtraOffsets(10f, 10f, 10f, 20f);
    }

}