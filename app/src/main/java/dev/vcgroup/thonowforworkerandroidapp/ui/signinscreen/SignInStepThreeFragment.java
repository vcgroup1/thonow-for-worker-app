package dev.vcgroup.thonowforworkerandroidapp.ui.signinscreen;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Worker;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentSignInStepThreeBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.MainScreenActivity;
import dev.vcgroup.thonowforworkerandroidapp.util.AuthenticationUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.VCLoadingDialog;

import static dev.vcgroup.thonowforworkerandroidapp.util.AuthenticationUtil.isConfirmPasswordMatch;
import static dev.vcgroup.thonowforworkerandroidapp.util.AuthenticationUtil.isValidPasswordForChangePassword;
import static dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil.getInputText;

public class SignInStepThreeFragment extends Fragment {
    private static final String TAG = SignInStepThreeFragment.class.getSimpleName();
    private FragmentSignInStepThreeBinding binding;
    private SignInViewModel signInViewModel;
    private static FirebaseAuth mAuth;
    private static FirebaseFirestore db;
    static {
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in_step_three, container, false);
        signInViewModel = ViewModelProviders.of(requireActivity()).get(SignInViewModel.class);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        
        listenForValidateForm();
        
        binding.btnSignIn.setOnClickListener(v -> {
            binding.btnSignIn.showLoading();
            linkCurrentUserWithEmailAuthProvider();
            binding.btnSignIn.hideLoading();
        });
    }

    private void linkCurrentUserWithEmailAuthProvider() {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            if (checkIfItLinkedWithCurrentUser(currentUser)) {
                updateUserProfileChange(currentUser);
            } else {
                String emailFromPhone = AuthenticationUtil.generateEmailFromNationalNumberPhone(String.valueOf(signInViewModel.getPhoneNumberProto().getValue().getNationalNumber()));
                AuthCredential credential = EmailAuthProvider.getCredential(emailFromPhone, getInputText(binding.tipPassword));

                currentUser.linkWithCredential(credential)
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                updateUserProfileChange(currentUser);
                            } else {
                                Snackbar.make(binding.getRoot(), "Đăng ký tài khoản thất bại", BaseTransientBottomBar.LENGTH_SHORT).show();
                            }
                        });
            }
        }
    }

    private boolean checkIfItLinkedWithCurrentUser(FirebaseUser currentUser) {
        for (UserInfo providerData : currentUser.getProviderData()) {
            if (providerData.getProviderId().equals(EmailAuthProvider.EMAIL_PASSWORD_SIGN_IN_METHOD)) {
                return true;
            }
        }
        return false;
    }

    private void createWorkerProfile(FirebaseUser currentUser) {
        db.collection(CollectionConst.COLLECTION_WORKER)
                .document(currentUser.getUid())
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document != null) {
                            if (document.exists()) {
                                db.collection(CollectionConst.COLLECTION_WORKER)
                                        .document(currentUser.getUid())
                                        .update(
                                                "displayName", currentUser.getDisplayName()
                                        )
                                        .addOnSuccessListener(aVoid -> navigateToMainScreen())
                                        .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
                            } else {
                                db.collection(CollectionConst.COLLECTION_WORKER)
                                        .document(currentUser.getUid())
                                        .set(Worker.convertFrom(currentUser))
                                        .addOnCompleteListener(task1 -> {
                                            if (task1.isSuccessful()) {
                                                Log.d(TAG, "createUserPro5::onSuccess::" + "Thành công");
                                                navigateToMainScreen();
                                            } else {
                                                Log.d(TAG, "createUserPro5::onFailure:: " + "Thất bại");
                                                Snackbar.make(binding.getRoot(), "Đăng nhập xảy ra lỗi. Thử lại!", BaseTransientBottomBar.LENGTH_SHORT).show();
                                            }
                                        });
                            }
                        }
                    }
                });
    }

    private void updateUserProfileChange(FirebaseUser currentUser) {
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(CommonUtil.getInputText(binding.tipDisplayName))
                .build();

        currentUser.updatePassword(CommonUtil.getInputText(binding.tipPassword))
                .addOnCompleteListener(task1 -> {
                    if (task1.isSuccessful()) {
                        Log.d(TAG, "Password updated.");

                        currentUser.updateProfile(profileUpdates)
                                .addOnCompleteListener(task -> {
                                    if (task.isSuccessful()) {
                                        Log.d(TAG, "User profile updated.");
                                        createWorkerProfile(currentUser);
                                    }
                                });
                    }
                });
    }
    private void navigateToMainScreen() {
        VCLoadingDialog loading = VCLoadingDialog.create(getActivity());
        loading.show();
        Intent intent = new Intent(getActivity(), MainScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent, ActivityOptions.makeCustomAnimation(getActivity(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
        requireActivity().finish();
        loading.dismiss();
    }


    private void listenForValidateForm() {
        binding.tipDisplayName.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                binding.btnSignIn.setEnabled(!s.toString().isEmpty());
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.btnSignIn.setEnabled(!s.toString().isEmpty());
            }

            @Override
            public void afterTextChanged(Editable s) {
                binding.btnSignIn.setEnabled(!s.toString().isEmpty());
            }
        });
        
        binding.tipPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateFormNotEmpty(charSequence.toString(),
                        getInputText(binding.tipPasswordConfirm));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateFormNotEmpty(charSequence.toString(),
                        getInputText(binding.tipPasswordConfirm));
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!isValidPasswordForChangePassword(editable.toString())) {
                    binding.tipPassword.setError("Mật khẩu phải từ 6-32 ký tự, gồm chữ kèm theo số hoặc ký tự đặc biệt");
                } else {
                    binding.tipPassword.setError(null);
                }
            }
        });

        binding.tipPasswordConfirm.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateFormNotEmpty(charSequence.toString(),
                        getInputText(binding.tipPassword));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateFormNotEmpty(charSequence.toString(),
                        getInputText(binding.tipPassword));
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!isConfirmPasswordMatch(getInputText(binding.tipPassword), editable.toString())) {
                    binding.tipPasswordConfirm.setError("Mật khẩu không trùng khớp");
                } else {
                    binding.tipPasswordConfirm.setError(null);
                }
            }
        });
    }

    private void validateFormNotEmpty(String str1, String str2) {
        binding.btnSignIn.setEnabled(!str1.isEmpty() && !str2.isEmpty());
    }

    OnFocusChangeListener focusChangeListener = (v, hasFocus) -> {
        binding.ivSigninStepThree.setVisibility(hasFocus ? View.GONE : View.VISIBLE);
    };

    @Override
    public void onResume() {
        super.onResume();
        binding.tipDisplayName.getEditText().setOnFocusChangeListener(focusChangeListener);
        binding.tipPassword.getEditText().setOnFocusChangeListener(focusChangeListener);
        binding.tipPasswordConfirm.getEditText().setOnFocusChangeListener(focusChangeListener);
    }
}