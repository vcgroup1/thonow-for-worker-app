package dev.vcgroup.thonowforworkerandroidapp.ui.notification.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import dev.vcgroup.thonowforworkerandroidapp.data.model.fcm.FCMRemoteNotification;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ItemNotificationBinding;

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.NotificationViewHolder> {
    private List<FCMRemoteNotification> notificationList;
    private OnNotificationItemListener onNotificationItemListener;
    private AsyncListDiffer<FCMRemoteNotification> mDiffer;
    private DiffUtil.ItemCallback<FCMRemoteNotification> mCallback = new DiffUtil.ItemCallback<FCMRemoteNotification>() {
        @Override
        public boolean areItemsTheSame(@NonNull @NotNull FCMRemoteNotification oldItem, @NonNull @NotNull FCMRemoteNotification newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull @NotNull FCMRemoteNotification oldItem, @NonNull @NotNull FCMRemoteNotification newItem) {
            return oldItem.getTimestamp().equals(newItem.getTimestamp());
        }
    };

    public NotificationListAdapter(List<FCMRemoteNotification> notificationList, OnNotificationItemListener onNotificationItemListener) {
        this.notificationList = notificationList;
        this.onNotificationItemListener = onNotificationItemListener;
        this.mDiffer = new AsyncListDiffer<>(this, mCallback);
    }

    @NonNull
    @NotNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new NotificationViewHolder(ItemNotificationBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false), onNotificationItemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull NotificationListAdapter.NotificationViewHolder holder, int position) {
        FCMRemoteNotification message = getItem(position);
        holder.bindItem(message);
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public void update(List<FCMRemoteNotification> newConservationList) {
        mDiffer.submitList(newConservationList);
    }

    public FCMRemoteNotification getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    public void removeItem(int position) {
        notificationList.remove(position);
        update(notificationList);
        notifyItemRemoved(position);
    }

    public interface OnNotificationItemListener {
        void onNotificationItemClick(int position);

        void onNotificationItemLongClick(int position);
    }

    public static class NotificationViewHolder extends RecyclerView.ViewHolder {
        private ItemNotificationBinding binding;
        private OnNotificationItemListener onNotificationItemListener;

        public NotificationViewHolder(@NonNull @NotNull ItemNotificationBinding binding, OnNotificationItemListener onNotificationItemListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.onNotificationItemListener = onNotificationItemListener;
            if (onNotificationItemListener != null) {
                int position = getAdapterPosition();
                if(position != RecyclerView.NO_POSITION) {
                    binding.getRoot().setOnLongClickListener(v -> {
                        this.onNotificationItemListener.onNotificationItemLongClick(position);
                        return true;
                    });
                }
            }
        }
        
        public void bindItem(FCMRemoteNotification notification) {
            binding.setNotification(notification);
            binding.executePendingBindings();
        }
    }
}