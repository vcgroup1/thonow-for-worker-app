package dev.vcgroup.thonowforworkerandroidapp.ui.signinscreen;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthSettings;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.kusu.loadingbutton.LoadingButton;
import com.mukesh.OtpView;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeUnit;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentSignInStepTwoBinding;
import dev.vcgroup.thonowforworkerandroidapp.util.AuthenticationUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.FragmentUtil;
import lombok.SneakyThrows;

public class SignInStepTwoFragment extends Fragment {
    private static final String TAG = SignInStepTwoFragment.class.getSimpleName();
    private static final String KEY_VERIFICATION_ID = "key_verification_id";
    private String strVerificationId;
    private CountDownTimer countDownTimer;
    private FragmentSignInStepTwoBinding binding;
    private SignInViewModel signInViewModel;
    private static FirebaseAuth mAuth;
    private static FirebaseFirestore db;
    static {
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
    }
    private LoadingButton btnResendCode;
    private String phoneNumber;
    private OtpView inputOtp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in_step_two, container, false);
        signInViewModel = ViewModelProviders.of(requireActivity()).get(SignInViewModel.class);
        binding.setSignInViewModel(signInViewModel);
        binding.setLifecycleOwner(getActivity());
        return binding.getRoot();
    }

    @SneakyThrows
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        inputOtp = view.findViewById(R.id.input_otp);

        phoneNumber = AuthenticationUtil.formatNumberPhone(signInViewModel.getPhoneNumberProto().getValue(), PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
        binding.tvPhoneNumber.setText(phoneNumber);
        sendVerificationCode(phoneNumber.trim());

        btnResendCode = binding.btnResendCode;
        countDownTimer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                String secondString = "";
                long second = millisUntilFinished / 1000;
                if (second < 10) {
                    secondString = "0" + second;
                } else {
                    secondString = second + "";
                }
                binding.tvCountDownTimer.setText("0:" + secondString);
            }

            @Override
            public void onFinish() {
                countDownTimer.cancel();
                btnResendCode.setEnabled(true);
                btnResendCode.setBackgroundColor(getActivity().getColor(R.color.strong_primary_red));
                btnResendCode.setTextColor(getActivity().getColor(android.R.color.white));
                binding.tvRegisterHelper.setText("Bạn không nhận được mã?");
                binding.tvCountDownTimer.setVisibility(View.GONE);
                binding.tvWrongPhoneNumber.setVisibility(View.VISIBLE);
            }
        };
        countDownTimer.start();

        btnResendCode.setOnClickListener(view1 -> {
            btnResendCode.showLoading();
            sendVerificationCode(phoneNumber);
            binding.tvRegisterHelper.setText("Gửi lại mã sau: ");
            binding.tvCountDownTimer.setVisibility(View.VISIBLE);
            btnResendCode.setEnabled(false);
            btnResendCode.setBackgroundColor(getActivity().getColor(R.color.light_gray));
            btnResendCode.setTextColor(getActivity().getColor(R.color.txt_medium_gray));
            binding.tvWrongPhoneNumber.setVisibility(View.GONE);
            countDownTimer.start();
            btnResendCode.hideLoading();
        });

        binding.ibnEditPhoneNumber.setOnClickListener(view1 -> {
            requireActivity().onBackPressed();
        });

        inputOtp.setOtpCompletionListener(this::verifyVerificationCode);
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(strVerificationId == null && savedInstanceState != null) {
            onViewStateRestored(savedInstanceState);
        }
    }

    private void sendVerificationCode(String phoneNumber) {
        PhoneAuthOptions phoneAuthOptions = PhoneAuthOptions.newBuilder(mAuth)
                .setPhoneNumber(phoneNumber)
                .setTimeout(100L, TimeUnit.SECONDS)
                .setActivity(requireActivity())
                .setCallbacks(phoneCallbacks)
                .requireSmsValidation(false)
                .build();
        PhoneAuthProvider.verifyPhoneNumber(phoneAuthOptions);
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks phoneCallbacks
            = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                inputOtp.setText(code);
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            setErrorText("Mã pin không chính xác");
        }

        @Override
        public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            strVerificationId = s;
        }
    };

    private void verifyVerificationCode(String code) {
        try {
            String phoneNumber = "+84968958053";
            String smsCode = "190599";
            String phoneNumber2 = "+84986660762";
            String phoneNumber3 = "+84968958052";

            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
            FirebaseAuthSettings firebaseAuthSettings = firebaseAuth.getFirebaseAuthSettings();

            firebaseAuthSettings.setAutoRetrievedSmsCodeForPhoneNumber(phoneNumber, smsCode);
            firebaseAuthSettings.setAutoRetrievedSmsCodeForPhoneNumber(phoneNumber2, smsCode);
            firebaseAuthSettings.setAutoRetrievedSmsCodeForPhoneNumber(phoneNumber3, smsCode);

            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(strVerificationId, code);

            signInWithPhoneAuthCredential(credential);
        } catch (Exception e) {
            Log.d("VerifyVerificationCode", e.getMessage());
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential phoneAuthCredential) {
        mAuth.signInWithCredential(phoneAuthCredential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull @NotNull Task<AuthResult> task) {

                    }
                })
                .addOnCompleteListener(requireActivity(), task -> {
                    if (task.isSuccessful()) {
                        countDownTimer.cancel();
                        FragmentUtil.replaceFragment(getActivity(), R.id.register_fragment_container,
                                new SignInStepThreeFragment(), getArguments(), SignInStepThreeFragment.class.getSimpleName(), true);
                    } else {
                        String message = "Có lỗi xảy ra. Vui lòng thử lại sau!";
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            setErrorText("Mã xác nhận không hợp lệ!");
                        } else {
                            setErrorText(message);
                        }
                    }
                });
    }

    private void setErrorText(String text) {
        binding.tvRegisterHint.setText(text);
        binding.tvRegisterHint.setTextColor(getResources().getColor(R.color.design_default_color_error, getActivity().getTheme()));
    }

    @Override
    public void onSaveInstanceState(@NonNull @NotNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_VERIFICATION_ID, strVerificationId);
    }

    @Override
    public void onViewStateRestored(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            strVerificationId = savedInstanceState.getString(KEY_VERIFICATION_ID);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        inputOtp.setText(strVerificationId);
        if (inputOtp.length() == 6) {
            verifyVerificationCode(strVerificationId);
        }
    }
}