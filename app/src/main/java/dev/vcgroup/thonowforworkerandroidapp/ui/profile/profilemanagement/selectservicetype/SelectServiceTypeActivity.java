package dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.selectservicetype;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.parceler.Parcels;

import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.ServiceType;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Worker;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivitySelectServiceTypeBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.homepage.adapter.ServiceTypeAdapter;
import dev.vcgroup.thonowforworkerandroidapp.util.SpacesItemDecoration;

public class SelectServiceTypeActivity extends AppCompatActivity implements ServiceTypeAdapter.OnServiceTypeListener {
    private ActivitySelectServiceTypeBinding binding;
    private ServiceTypeAdapter adapter;
    private Worker currentUser;
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_service_type);
        binding.setLifecycleOwner(this);

        if (getIntent().getExtras() != null) {
            currentUser = Parcels.unwrap(getIntent().getParcelableExtra("currentUser"));
        }

        setupToolbar();
        setupServiceTypeRv();
        setupSearchView();

        binding.btnConfirm.setOnClickListener(v -> {
            db.collection(CollectionConst.COLLECTION_WORKER)
                    .document(currentUser.getId())
                    .update(
                            "serviceTypeList", adapter.getSelectedServiceTypeList()
                    )
                    .addOnSuccessListener(aVoid -> finish());
        });
    }

    private void setupServiceTypeRv() {
        adapter = new ServiceTypeAdapter(this, ServiceTypeAdapter.ServiceTypeEnum.CARD_VIEW, currentUser);
        binding.rvServiceType.setAdapter(adapter);
        binding.rvServiceType.addItemDecoration(new SpacesItemDecoration(20));
    }

    private void setupSearchView() {
        binding.svServiceType.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });

    }

    private void setupToolbar() {
        Toolbar toolbar = binding.topAppBar.mToolbar;
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        binding.topAppBar.tvTitleToolbar.setText("Dịch vụ");
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onServiceTypeClickedListener(int position) {

    }
}