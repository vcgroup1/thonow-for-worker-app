package dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.basicpersonalinformation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivityBasicPersonalInformationBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.MainScreenViewModel;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.basicpersonalinformation.definemylocation.DefineMyLocationFragment;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.basicpersonalinformation.definemylocation.picklocationinmap.PickLocationInMapFragment;
import dev.vcgroup.thonowforworkerandroidapp.util.FragmentUtil;

public class BasicPersonalInformationActivity extends AppCompatActivity implements StepperLayout.StepperListener {
    private ActivityBasicPersonalInformationBinding binding;
    private MainScreenViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_basic_personal_information );
        mViewModel = ViewModelProviders.of(this).get(MainScreenViewModel.class);

        binding.stepperLayout.setAdapter(new BasicProfileStepAdapter(getSupportFragmentManager(), this));
        setupToolbar();
    }

    private void setupToolbar() {
        Toolbar toolbar = binding.topAppBar.mToolbar;
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        binding.topAppBar.tvTitleToolbar.setText("Thiết lập thông tin hồ sơ");

        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            Fragment currentFragment = FragmentUtil.getCurrentFragment(this, R.id.basic_profile_container);
            if (currentFragment instanceof DefineMyLocationFragment) {
                binding.topAppBar.tvTitleToolbar.setText("Chọn địa điểm");
            } else if (currentFragment instanceof PickLocationInMapFragment) {
                binding.topAppBar.tvTitleToolbar.setText("Chọn vị trí trên bản đồ");
            } else {
                binding.topAppBar.tvTitleToolbar.setText("Thiết lập thông tin hồ sơ");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCompleted(View completeButton) {
        finish();
    }

    @Override
    public void onError(VerificationError verificationError) {

    }

    @Override
    public void onStepSelected(int newStepPosition) {

    }

    @Override
    public void onReturn() {
        finish();
    }
}