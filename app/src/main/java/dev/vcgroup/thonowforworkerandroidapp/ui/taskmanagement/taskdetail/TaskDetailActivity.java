package dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.taskdetail;

import android.Manifest;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.imageviewer.loader.ImageLoader;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.constant.StorageConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.MaterialCost;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Order;
import dev.vcgroup.thonowforworkerandroidapp.data.model.OrderDetail;
import dev.vcgroup.thonowforworkerandroidapp.data.model.OrderStatus;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Service;
import dev.vcgroup.thonowforworkerandroidapp.data.model.User;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Worker;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivityOrderDetailBinding;
import dev.vcgroup.thonowforworkerandroidapp.databinding.DialogChangeProfilePictureBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.audiocall.callscreen.CallScreenActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.message.chatwindow.ChatActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.editprofile.EditProfileActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.adapter.ImageAdapter;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.adapter.OrderDetailsAdapter;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.cancelorder.CancelOrderActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.adapter.MaterialCostAdapter;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.materialcostmanagement.MaterialCostManagementActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.orderdetailmanagement.OrderDetailManagementActivity;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.FileUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.ImageLoadingUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.MyCustomAlertDialog;
import dev.vcgroup.thonowforworkerandroidapp.util.OrderHelper;
import dev.vcgroup.thonowforworkerandroidapp.util.SpacesItemDecoration;
import gun0912.tedimagepicker.builder.TedImagePicker;

import static dev.vcgroup.thonowforworkerandroidapp.constant.AudioCallConst.OUT_GOING_CALL;
import static dev.vcgroup.thonowforworkerandroidapp.constant.CommonConst.CALL_PERMISSIONS;
import static dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst.CANCEL_ORDER_REQ_CODE;
import static dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst.MATERIAL_COST_MANAGEMENT_REQ_CODE;
import static dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst.ORDER_DETAIL_MANAGEMENT_REQ_CODE;
import static dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst.TAKE_PHOTO_CODE;

public class TaskDetailActivity extends AppCompatActivity implements ImageAdapter.OnImageListener, MaterialCostAdapter.OnMaterialCostListener, OrderDetailsAdapter.OnOrderDetailListener {
    private static final String TAG = TaskDetailActivity.class.getSimpleName();
    private ActivityOrderDetailBinding binding;
    private Order order;
    private static FirebaseFirestore db;
    private static StorageReference mStorageRef;
    static {
        db = FirebaseFirestore.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }
    private MaterialCostAdapter materialAdapter;
    private OrderDetailsAdapter orderDetailsAdapter;
    private ImageAdapter imageAdapter;

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order_detail);

        setupToolbar();

        if (getIntent() != null) {
            order = Parcels.unwrap(getIntent().getParcelableExtra("order"));
            if (order != null) {
                populateAndBindData();
            }

        }

        binding.btnSubmitOrder.setOnClickListener(v -> {
            // chuyển status tiếp theo
            order.setStatus(OrderStatus.WAIT_FOR_ORDER_CONFIRMATION);
            order.setImages(imageAdapter.getItems());

            db.collection(CollectionConst.COLLECTION_ORDER)
                    .document(order.getId())
                    .set(order)
                    .addOnSuccessListener(unused -> {
                        binding.setOrder(order);
                        Toast.makeText(this, "Đã gửi đơn khảo sát đến khách hàng!", Toast.LENGTH_SHORT).show();
                        //binding.btnSubmitOrder.setVisibility(View.GONE);
                    })
                    .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
        });

        binding.btnCancelOrder.setOnClickListener(v -> {
            Intent intent = new Intent(this, CancelOrderActivity.class);
            intent.putExtra("deleteOrderId", Parcels.wrap(order.getId()));
            startActivityForResult(intent, CANCEL_ORDER_REQ_CODE);
        });

        binding.btnEditOrderDetails.setOnClickListener(v -> {
            Intent intent = new Intent(this, OrderDetailManagementActivity.class);
            intent.putExtra("order", Parcels.wrap(order));
            startActivityForResult(intent, ORDER_DETAIL_MANAGEMENT_REQ_CODE);
        });

        binding.btnEditMaterialCosts.setOnClickListener(v -> {
            Intent intent = new Intent(this, MaterialCostManagementActivity.class);
            intent.putExtra("materialCosts", Parcels.wrap(order.getMaterialCosts()));
            intent.putExtra("orderId", order.getId());
            startActivityForResult(intent, MATERIAL_COST_MANAGEMENT_REQ_CODE);
        });

        binding.btnAddImage.setOnClickListener(v -> showChangeProfilePictureDialog());
    }

    private void populateAndBindData() {
        // bind order
        Log.d(TAG, order.toString());
        binding.setOrder(order);

        // bind worker
        order.getWorker()
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    Worker worker = documentSnapshot.toObject(Worker.class);
                    binding.setWorker(worker);
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));

        // bind service type
        order.getOrderDetails().get(0).getService().get()
                .addOnSuccessListener(documentSnapshot -> {
                    Service service = documentSnapshot.toObject(Service.class);
                    binding.setService(service);
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));

        // setup Order Details RecyclerView
        orderDetailsAdapter = new OrderDetailsAdapter(new ArrayList<>(), this);
        binding.rvOrderDetails.setAdapter(orderDetailsAdapter);
        binding.rvOrderDetails.addItemDecoration(new SpacesItemDecoration(10));
        orderDetailsAdapter.update(order.getOrderDetails());

        // setup image recyclerview
        imageAdapter = new ImageAdapter(new ArrayList<>(), this);
        binding.rvOrderImages.setAdapter(imageAdapter);
        binding.rvOrderImages.addItemDecoration(new SpacesItemDecoration(10));
        imageAdapter.update(order.getImages());
        checkIfOrderDetailEmpty();

        // setup material cost recyclerview
        materialAdapter = new MaterialCostAdapter(order.getMaterialCosts(), this);
        binding.rvMaterialCosts.setAdapter(materialAdapter);
        binding.rvMaterialCosts.addItemDecoration(new SpacesItemDecoration(10));

        switch (order.getStatus()) {
            case WAIT_FOR_ORDER_CONFIRMATION:
                binding.btnSubmitOrder.setVisibility(View.VISIBLE);
                binding.btnCancelOrder.setVisibility(View.VISIBLE);
                break;
            case CONFIRMED_ORDER:
            case WORKING:
            case WAIT_FOR_CONFIRMATION_TO_COMPLETE:
            case WAIT_FOR_CONFIRMATION_TO_PAY:
            case COMPLETED:
            case CANCELED:
                binding.btnSubmitOrder.setVisibility(View.GONE);
                binding.btnCancelOrder.setVisibility(View.GONE);
                break;
        }

        if (order.getStatus() == OrderStatus.ARRIVED) {
            double movingExpense = OrderHelper.calculateMovingExpense(10);
            double overtimeExpense = OrderHelper.calculateOvertimeFee(order.getEstimatedFee(), order.getWorkingDate());
            double total = order.getEstimatedFee() + movingExpense + overtimeExpense;
            order.setMovingExpense(movingExpense);
            order.setOvertimeFee(overtimeExpense);
            order.setTotal(total);

            binding.tvMovingExpense.setText(CommonUtil.convertCurrencyText(movingExpense) + "đ");
            binding.tvOvertimeExpense.setText(CommonUtil.convertCurrencyText(overtimeExpense) + "đ");
            binding.tvTotal.setText(CommonUtil.convertCurrencyText(total) + "đ");
        }
    }

    private void checkIfOrderDetailEmpty() {
        if (imageAdapter.getItemCount() == 0) {
            binding.rvOrderImages.setVisibility(View.INVISIBLE);
            binding.sectionNoImages.setVisibility(View.VISIBLE);
        } else {
            binding.rvOrderImages.setVisibility(View.VISIBLE);
            binding.sectionNoImages.setVisibility(View.INVISIBLE);
        }
    }

    private void setupToolbar() {
        Toolbar mToolbar = binding.topAppBar.mToolbar;
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        binding.topAppBar.tvTitleToolbar.setText("Chi tiết đơn hàng");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_order_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_chat) {
            if (order.getStatus() != OrderStatus.CANCELED) {
                String orderId = order.getId();
                Intent intent = new Intent(this, ChatActivity.class);
                intent.putExtra("orderId", orderId);
                intent.putExtra("customerId", order.getCustomer().getId());
                startActivity(intent, ActivityOptions.makeCustomAnimation(this, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
            } else {
                MyCustomAlertDialog dialog = MyCustomAlertDialog.create(this);
                dialog.setTitle("Thông báo")
                        .setMessage("Đơn trong trạng thái đã bị từ chối. Bạn không thể liên hệ với khách hàng.")
                        .setPositiveButton("Quay lại", v -> dialog.dismiss())
                        .show();
            }
            return true;
        } else if (item.getItemId() == R.id.menu_call) {
            if (!TedPermission.isGranted(this, CALL_PERMISSIONS)) {
                TedPermission.with(this)
                        .setPermissionListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted() {
                                callToCustomer(order);
                            }

                            @Override
                            public void onPermissionDenied(List<String> deniedPermissions) {
                                Snackbar.make(binding.getRoot(), "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                            }
                        })
                        .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                        .setPermissions(CALL_PERMISSIONS)
                        .check();
            } else {
                callToCustomer(order);
            }
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            setResult(RESULT_OK);
            finish();
            return true;
        }
        return false;
    }

    private void callToCustomer(Order order) {
        order.getCustomer()
                .get()
                .addOnSuccessListener(documentSnapshot1 -> {
                    Intent intent = new Intent(this, CallScreenActivity.class);
                    intent.putExtra("customer", Parcels.wrap(documentSnapshot1.toObject(User.class)));
                    intent.putExtra(OUT_GOING_CALL, true);
                    startActivity(intent, ActivityOptions.makeCustomAnimation(TaskDetailActivity.this, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, e.getMessage());
                    notifyFailedAudioCall();
                });

    }
    private void notifyFailedAudioCall() {
        MyCustomAlertDialog dialog = MyCustomAlertDialog.create(this);
        dialog.setTitle("Thông báo")
                .setMessage("Gọi thoại cho khách hàng xảy ra lỗi. Hãy thử lại xem sao.")
                .setPositiveButton("Thử lại", v -> {
                    dialog.dismiss();
                })
                .show();
    }

    @Override
    public void onImageClickListener(int position) {
        StfalconImageViewer.Builder<String> builder = new StfalconImageViewer.Builder<>(this, imageAdapter.getItems(), ImageLoadingUtil::displayImage);
        builder.show().setCurrentPosition(position);
    }

    @Override
    public void onImageRemoveClickListener(int position) {
        MyCustomAlertDialog dialog = MyCustomAlertDialog.create(this);
        dialog.setTitle("Thông báo")
                .setMessage("Bạn có chắn chắn muốn xóa ảnh này?")
                .setPositiveButton("Xóa", v -> {
                    imageAdapter.deleteItem(position);
                    order.setImages(imageAdapter.getItems());
                    dialog.dismiss();
                })
                .setNegativeButton("Hủy", v -> dialog.dismiss())
                .show();
    }

    @Override
    public void onRemoveMaterialCost(View view, int position) {

    }

    @Override
    public void onOrderDetailClickedListener(View view, int position) {

    }

    private void showChangeProfilePictureDialog() {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        DialogChangeProfilePictureBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_change_profile_picture, (ViewGroup) getWindow().getDecorView().getRootView(), false);

        builder.setView(binding.getRoot());
        AlertDialog dialog = builder.create();
        dialog.show();

        binding.changeAvatarTakeAPictureSelection.setOnClickListener(view -> TedPermission.with(TaskDetailActivity.this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        takeAPictureFromCamera();
                        dialog.dismiss();
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        Snackbar.make(binding.getRoot(), "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                    }
                })
                .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                .setPermissions(Manifest.permission.CAMERA)
                .check());

        binding.changeAvatarChooseExistingPhotoSelection.setOnClickListener(view -> TedPermission.with(TaskDetailActivity.this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        pickImageFromGallery();
                        dialog.dismiss();
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        Snackbar.make(binding.getRoot(), "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                    }
                })
                .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                .setPermissions(Manifest.permission.CAMERA)
                .check());
    }

    private void takeAPictureFromCamera() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
        }
    }

    private void pickImageFromGallery() {
        TedImagePicker.with(this)
                .title("Chọn ảnh")
                .buttonText("Xong")
                .buttonBackground(R.color.strong_red_darker)
                .buttonTextColor(android.R.color.white)
                .start(this::uploadPicture);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CANCEL_ORDER_REQ_CODE && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        } else if (requestCode == MATERIAL_COST_MANAGEMENT_REQ_CODE && resultCode == RESULT_OK && data.getExtras() != null) {
            List<MaterialCost> list = Parcels.unwrap(data.getParcelableExtra("materialCosts"));
            materialAdapter.update(list);
            order.setMaterialCosts(list);
        } else if (requestCode == ORDER_DETAIL_MANAGEMENT_REQ_CODE && resultCode == RESULT_OK && data.getExtras() != null) {
            List<OrderDetail> list = Parcels.unwrap(data.getParcelableExtra("orderDetails"));
            orderDetailsAdapter.update(list);
            order.setOrderDetails(list);
            order.setEstimatedFee(OrderHelper.calculateOrderTotal(list));
            binding.tvEstimatedFee.setText(CommonUtil.convertCurrencyText(order.getEstimatedFee()) + " đ");
            order.setTotal(order.getTotal() + order.getEstimatedFee());
            binding.tvTotal.setText(CommonUtil.convertCurrencyText(order.getTotal()) + " đ");
        } else if (requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK && data != null) {
            Uri uri = FileUtil.getImageUri(this, (Bitmap) data.getExtras().get("data"));
            uploadPicture(uri);
        }
    }

    private void uploadPicture(Uri imageUri) {
        StorageReference avatarRef = mStorageRef.child(StorageConst.FOLDER_ORDERS_PHOTO + "/" + order.getId());
        avatarRef.putFile(imageUri)
                .addOnSuccessListener(taskSnapshot -> {
                    Task<Uri> task = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                    task.addOnSuccessListener(uri -> {
                        Log.d("imageUri", uri.toString());
                        imageAdapter.addtem(uri.toString());
                        checkIfOrderDetailEmpty();
                    });
                })
                .addOnFailureListener(e -> Snackbar.make(binding.getRoot(), "Upload hình xảy ra lỗi. Thử lại!", BaseTransientBottomBar.LENGTH_SHORT));
    }
}
