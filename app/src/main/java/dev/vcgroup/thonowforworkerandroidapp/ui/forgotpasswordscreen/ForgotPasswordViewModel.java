package dev.vcgroup.thonowforworkerandroidapp.ui.forgotpasswordscreen;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.data.model.CountryInfo;
import dev.vcgroup.thonowforworkerandroidapp.util.AuthenticationUtil;
import lombok.SneakyThrows;

public class ForgotPasswordViewModel extends ViewModel {
    private MutableLiveData<String> phoneNumber = new MutableLiveData<>();
    private MutableLiveData<CountryInfo> selectedCountryInfo = new MutableLiveData<>();
    private MutableLiveData<Phonenumber.PhoneNumber> phoneNumberProto = new MutableLiveData<>();
    private static PhoneNumberUtil phoneNumberUtil;

    public MutableLiveData<Phonenumber.PhoneNumber> getPhoneNumberProto() {
        return phoneNumberProto;
    }

    public void setPhoneNumberProto(Phonenumber.PhoneNumber phoneNumberProto) {
        this.phoneNumberProto.setValue(phoneNumberProto);
    }

    public MutableLiveData<String> getPhoneNumber() {
        return phoneNumber;
    }

    @SneakyThrows
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber.setValue(phoneNumber);
        this.setPhoneNumberProto(AuthenticationUtil.parsePhoneNumber(phoneNumber, Objects.requireNonNull(getSelectedCountryInfo().getValue()).getAlp2Code()));
    }

    public MutableLiveData<CountryInfo> getSelectedCountryInfo() {
        return selectedCountryInfo;
    }

    public void setSelectedCountryInfo(CountryInfo selectedCountryInfo) {
        this.selectedCountryInfo.setValue(selectedCountryInfo);
    }
}
