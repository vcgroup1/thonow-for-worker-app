package dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.basicpersonalinformation;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.wifi.aware.IdentityChangedListener;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.constant.StorageConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Worker;
import dev.vcgroup.thonowforworkerandroidapp.databinding.DialogChangeProfilePictureBinding;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentBasicProfileStepThreeBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.MainScreenViewModel;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.editprofile.EditProfileActivity;
import dev.vcgroup.thonowforworkerandroidapp.util.FileUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.ImageLoadingUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.MyCustomAlertDialog;
import gun0912.tedimagepicker.builder.TedImagePicker;

import static dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst.TAKE_PHOTO_BACK_ID_CODE;
import static dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst.TAKE_PHOTO_CODE;
import static dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst.TAKE_PHOTO_FRONT_ID_CODE;

public class BasicProfileStepThreeFragment extends Fragment implements Step {
    private static final String TAG = BasicProfileStepThreeFragment.class.getSimpleName();
    private FragmentBasicProfileStepThreeBinding binding;
    private MainScreenViewModel mViewModel;
    private Worker.IdentityCard identityCard;
    private Uri frontIdCardUri, backIdCardUri;
    private StorageReference mStorageRef;
    private StorageReference avatarRef;

    public static BasicProfileStepThreeFragment newInstance() {
        return new BasicProfileStepThreeFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_basic_profile_step_three, container, false);
        mViewModel = ViewModelProviders.of(requireActivity()).get(MainScreenViewModel.class);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.btnAddIdCardFront.setOnClickListener(v -> showChangeProfilePictureDialog(view, TAKE_PHOTO_FRONT_ID_CODE));
        binding.btnAddIdCardBack.setOnClickListener(v -> showChangeProfilePictureDialog(view, TAKE_PHOTO_BACK_ID_CODE));

    }

    private void showChangeProfilePictureDialog(View view1, int code) {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        DialogChangeProfilePictureBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_change_profile_picture, (ViewGroup) view1.getRootView(), false);

        builder.setView(binding.getRoot());
        AlertDialog dialog = builder.create();
        binding.tvAvatar.setText("Ảnh CMND/CCCD");
        dialog.show();

        binding.changeAvatarTakeAPictureSelection.setOnClickListener(view ->
                TedPermission.with(requireActivity())
                        .setPermissionListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted() {
                                takeAPictureFromCamera(code);
                                dialog.dismiss();
                            }

                            @Override
                            public void onPermissionDenied(List<String> deniedPermissions) {
                                Snackbar.make(binding.getRoot(), "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                            }
                        })
                        .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                        .setPermissions(Manifest.permission.CAMERA)
                        .check());

        binding.changeAvatarChooseExistingPhotoSelection.setOnClickListener(view ->
                TedPermission.with(requireActivity())
                        .setPermissionListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted() {
                                pickImageFromGallery(code);
                                dialog.dismiss();
                            }

                            @Override
                            public void onPermissionDenied(List<String> deniedPermissions) {
                                Snackbar.make(binding.getRoot(), "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                            }
                        })
                        .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                        .setPermissions(Manifest.permission.CAMERA)
                        .check());
    }

    @SuppressLint("QueryPermissionsNeeded")
    private void takeAPictureFromCamera(int code) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            getActivity().startActivityForResult(cameraIntent, code);
        }
    }

    private void pickImageFromGallery(int code) {
        TedImagePicker.with(requireActivity())
                .title("Chọn ảnh")
                .buttonText("Xong")
                .showCameraTile(false)
                .buttonBackground(R.color.strong_red_darker)
                .buttonTextColor(android.R.color.white)
                .start(uri -> {
                    if (code == TAKE_PHOTO_FRONT_ID_CODE) {
                        frontIdCardUri = uri;
                        ImageLoadingUtil.displayImage(binding.idCardFront, uri.toString());
                        binding.idCardFront.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        binding.addIdCardFrontSection.setVisibility(View.GONE);
                    } else {
                        backIdCardUri = uri;
                        ImageLoadingUtil.displayImage(binding.idCardBack, uri.toString());
                        binding.idCardBack.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        binding.addIdCardBackSection.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == TAKE_PHOTO_FRONT_ID_CODE || requestCode == TAKE_PHOTO_BACK_ID_CODE) && resultCode == requireActivity().RESULT_OK && data != null) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            Bitmap idCardBitmap = Bitmap.createScaledBitmap(bitmap,
                    (int) getResources().getDimension(R.dimen.id_card_size),
                    (int) getResources().getDimension(R.dimen.id_card_size), true);
            if (requestCode == TAKE_PHOTO_FRONT_ID_CODE) {
                frontIdCardUri = FileUtil.getImageUri(requireActivity(), bitmap);
                binding.idCardFront.setImageBitmap(idCardBitmap);
                binding.addIdCardFrontSection.setVisibility(View.GONE);
            } else {
                backIdCardUri = FileUtil.getImageUri(requireActivity(), bitmap);
                binding.idCardBack.setImageBitmap(idCardBitmap);
                binding.addIdCardBackSection.setVisibility(View.GONE);
            }
        }
    }

    private class UploadTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            uploadPicture(frontIdCardUri, TAKE_PHOTO_FRONT_ID_CODE);
            uploadPicture(backIdCardUri, TAKE_PHOTO_BACK_ID_CODE);
            Log.d(TAG, "uploadPicture");
            return Boolean.TRUE;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean == Boolean.TRUE) {
                Log.d(TAG, "vô update");
                updateIdentityCard();
            }
        }
    }

    private void uploadIdentityCard() {
        if (frontIdCardUri != null && backIdCardUri != null) {
            if (identityCard == null) {
                identityCard = new Worker.IdentityCard();
            }
            new UploadTask().execute();
        } else {
            MyCustomAlertDialog dialog = MyCustomAlertDialog.create(requireActivity());
            dialog.setTitle("Thông báo")
                    .setMessage("Vui lòng nhập đầy đủ hai mặt CMND/CCCD")
                    .setPositiveButton("Đã hiểu", v -> dialog.dismiss())
                    .show();
        }
    }

    private void updateIdentityCard() {
        if (identityCard.getBackIdCardUrl() == null && identityCard.getFrontIdCardUrl() == null) {
            identityCard.setFrontIdCardUrl(frontIdCardUri.toString());
            identityCard.setBackIdCardUrl(backIdCardUri.toString());
        } else {
            FirebaseFirestore.getInstance().collection(CollectionConst.COLLECTION_WORKER)
                    .document(mViewModel.getCurrentUser().getValue().getId())
                    .update("idCard", identityCard)
                    .addOnSuccessListener(aVoid -> {
                        Toast.makeText(getActivity(), "Cập nhật CMND/CCCD thành công!", Toast.LENGTH_SHORT).show();
                        requireActivity().finish();
                    })
                    .addOnFailureListener(e -> Snackbar.make(binding.getRoot(), "Quá trình cập nhật xảy ra lỗi. Thử lại!", BaseTransientBottomBar.LENGTH_SHORT));
        }
    }

    private void uploadPicture(Uri imageUri, int code) {
        if (mStorageRef == null) {
            mStorageRef = FirebaseStorage.getInstance().getReference();
            avatarRef = mStorageRef.child(StorageConst.WORKER_AVATAR_FOLDER + "/" + Objects.requireNonNull(mViewModel.getCurrentUser().getValue()).getId());
        }
        if (!imageUri.toString().contains(getActivity().getResources().getString(R.string.google_storage_bucket))) {
            avatarRef.putFile(imageUri)
                    .addOnSuccessListener(taskSnapshot -> {
                        Task<Uri> task = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                        task.addOnSuccessListener(uri -> {
                            if (code == TAKE_PHOTO_FRONT_ID_CODE) {
                                frontIdCardUri = uri;
                                identityCard.setFrontIdCardUrl(uri.toString());
                            } else {
                                backIdCardUri = uri;
                                identityCard.setBackIdCardUrl(uri.toString());
                            }
                        });
                    })
                    .addOnFailureListener(e -> Snackbar.make(binding.getRoot(), "Quá trình upload xảy ra lỗi. Thử lại!", BaseTransientBottomBar.LENGTH_SHORT));
        }
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        uploadIdentityCard();
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
}