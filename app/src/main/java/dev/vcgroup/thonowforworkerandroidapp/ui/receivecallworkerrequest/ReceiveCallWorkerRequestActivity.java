package dev.vcgroup.thonowforworkerandroidapp.ui.receivecallworkerrequest;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.parceler.Parcels;

import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CallWorkerConst;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Order;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Service;
import dev.vcgroup.thonowforworkerandroidapp.data.model.ServiceType;
import dev.vcgroup.thonowforworkerandroidapp.data.model.User;
import dev.vcgroup.thonowforworkerandroidapp.data.model.fcm.FCMResponse;
import dev.vcgroup.thonowforworkerandroidapp.data.model.fcm.FCMSingleSendData;
import dev.vcgroup.thonowforworkerandroidapp.data.model.fcm.GenericFcmData;
import dev.vcgroup.thonowforworkerandroidapp.data.model.fcm.Token;
import dev.vcgroup.thonowforworkerandroidapp.data.remote.fcmservice.IFCMService;
import dev.vcgroup.thonowforworkerandroidapp.data.remote.fcmservice.RetrofitFCMClientInstance;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivityReceiveTaskBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.MainScreenActivity;
import dev.vcgroup.thonowforworkerandroidapp.util.MyCustomAlertDialog;
import dev.vcgroup.thonowforworkerandroidapp.util.VCLoadingDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static dev.vcgroup.thonowforworkerandroidapp.constant.CallWorkerConst.IS_FROM_RECEIVED_CALL_WORKER_REQUEST_CODE;

public class ReceiveCallWorkerRequestActivity extends AppCompatActivity {
    private static final String TAG = ReceiveCallWorkerRequestActivity.class.getSimpleName();
    private ActivityReceiveTaskBinding binding;
    private Order order;
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }
    private IFCMService ifcmService;
    private VCLoadingDialog loading;
    private int tryAgainCount = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_receive_task);

        if (getIntent().getExtras() != null) {
            order = Order.convertFrom(Parcels.unwrap(getIntent().getParcelableExtra("order")));

            if (order != null) {
                binding.setOrder(order);
                getOrderExtraInfomation();
            } else {
                String orderId = getIntent().getStringExtra("orderId");
                db.collection(CollectionConst.COLLECTION_ORDER)
                        .document(orderId)
                        .get()
                        .addOnSuccessListener(documentSnapshot -> {
                            order = documentSnapshot.toObject(Order.class);
                            binding.setOrder(order);
                            getOrderExtraInfomation();
                        })
                        .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
            }

            getOrderExtraInfomation();
        }

        setupCountDownTimer();

        registerClickListener();
    }

    private void getOrderExtraInfomation() {
        order.getCustomer().get()
                .addOnSuccessListener(documentSnapshot -> {
                    binding.setCustomer(documentSnapshot.toObject(User.class));
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));

        order.getOrderDetails().get(0).getService().get()
                .addOnSuccessListener(documentSnapshot -> {
                    Objects.requireNonNull(documentSnapshot.toObject(Service.class))
                            .getServiceType()
                            .get()
                            .addOnSuccessListener(documentSnapshot1 -> {
                                binding.setServiceType(documentSnapshot1.toObject(ServiceType.class));
                            })
                            .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
    }

    private void setupCountDownTimer() {
        new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                String secondString = "";
                long second = millisUntilFinished / 1000;
                if (second < 10) {
                    secondString = "0" + second;
                } else {
                    secondString = second + "";
                }
                binding.tvTimeRemaining.setText(secondString);
            }

            @Override
            public void onFinish() {
                finish();
            }
        }.start();
    }

    private void registerClickListener() {
        if (loading == null) {
            loading = VCLoadingDialog.create(this);
        }
        binding.btnAcceptOrder.setOnClickListener(v -> {
            doAcceptOrder();
        });

        binding.btnRejectOrder.setOnClickListener(v -> {
            MyCustomAlertDialog dialog = MyCustomAlertDialog.create(ReceiveCallWorkerRequestActivity.this);
            dialog.setTitle("Xác nhận")
                    .setMessage("Bạn có chắn chắn muốn hủy nhận đơn hàng này?")
                    .setPositiveButton("Hủy đơn hàng", v1 -> {
                        dialog.dismiss();
                        finish();
                    })
                    .setNegativeButton("Quay lại", v1 -> {
                        dialog.dismiss();
                    })
                    .show();
        });
    }

    private void doAcceptOrder() {
        // kiểm tra xem order với id đó đã dc tạo ra chưa
        // nếu tạo rồi thì tức đã có người nhận
        // chưa thì tạo order ra
        // sau đó gửi thông báo về người dùng
        loading.show();
        DocumentReference orderDocRef = db.collection(CollectionConst.COLLECTION_ORDER)
                .document(order.getId());

        orderDocRef
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        loading.dismiss();
                        MyCustomAlertDialog dialog = MyCustomAlertDialog.create(ReceiveCallWorkerRequestActivity.this);
                        dialog.setTitle("Thông báo")
                                .setMessage("Rất tiếc! Đã có thợ nhận công việc này. Hãy nhanh tay hơn nhé")
                                .setPositiveButton("Đã hiểu", v1 -> {
                                    dialog.dismiss();
                                    finish();
                                })
                                .show();

                    } else {
                        order.setWorker(db.collection(CollectionConst.COLLECTION_WORKER).document(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()));
                        order.setStatus(order.getStatus().nextStatus());

                        orderDocRef.set(order)
                                .addOnCompleteListener(task -> {
                                    if (task.isSuccessful()) {
                                        // gửi thông báo cho user
                                        sendNotificationToCustomer();
                                    } else {
                                        loading.dismiss();
                                        MyCustomAlertDialog dialog = MyCustomAlertDialog.create(ReceiveCallWorkerRequestActivity.this);
                                        dialog.setTitle("Thông báo")
                                                .setMessage("Khởi tạo đơn hàng xảy ra lỗi. Hãy thử lại lần nữa!")
                                                .setPositiveButton("Thử lại", v1 -> {
                                                    dialog.dismiss();
                                                    doAcceptOrder();
                                                })
                                                .setNegativeButton("Hủy", v1 -> {
                                                    dialog.dismiss();
                                                })
                                                .show();
                                    }
                                });
                    }
                })
                .addOnFailureListener(e -> {
                    loading.dismiss();
                    MyCustomAlertDialog dialog = MyCustomAlertDialog.create(ReceiveCallWorkerRequestActivity.this);
                    dialog.setTitle("Thông báo")
                            .setMessage("Quá trình thực hiện xảy ra lỗi. Chúng tôi sẽ kiểm tra sau!")
                            .setPositiveButton("Đã hiểu", v1 -> {
                                dialog.dismiss();
                            })
                            .show();
                    Log.d(TAG, e.getMessage());
                });
    }

    private void sendNotificationToCustomer() {
        if (ifcmService == null) {
            ifcmService = RetrofitFCMClientInstance.getInstance().getRetrofit().create(IFCMService.class);
        }
        tryAgainCount++;
        loading.show();
        db.collection(CollectionConst.COLLECTION_TOKEN)
                .document(order.getCustomer().getId())
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    String token = Objects.requireNonNull(documentSnapshot.toObject(Token.class)).getToken();

                    if (!token.isEmpty()) {
                        FCMSingleSendData<String> data = new FCMSingleSendData<>(
                                new GenericFcmData<>(CallWorkerConst.RECEIVED_CALL_WORKER_REQUEST_CODE, order.getId()), token);

                        ifcmService.sendReceivedCallWorkerRequest(data)
                                .enqueue(new Callback<FCMResponse>() {
                                    @Override
                                    public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                                        FCMResponse responseBody = response.body();
                                        if (responseBody != null) {
                                            if (response.isSuccessful() && responseBody.getSuccess() > 0) {
                                                loading.dismiss();
                                                MyCustomAlertDialog dialog = MyCustomAlertDialog.create(ReceiveCallWorkerRequestActivity.this);
                                                dialog.setTitle("Thông báo")
                                                        .setMessage("Nhận việc thành công. Hãy đến [Quản lý việc] để bắt đầu làm việc ngay bây giờ.")
                                                        .setPositiveButton("Đã hiểu", v -> {
                                                            dialog.dismiss();
                                                            Intent intent = new Intent(ReceiveCallWorkerRequestActivity.this, MainScreenActivity.class);
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                            intent.putExtra(IS_FROM_RECEIVED_CALL_WORKER_REQUEST_CODE, true);
                                                            startActivity(intent, ActivityOptions.makeCustomAnimation(ReceiveCallWorkerRequestActivity.this, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
                                                            finish();
                                                        })
                                                        .show();

                                            } else {
                                                Log.d(TAG, responseBody.getResults().get(0).getError());
                                                notifyFailedNotificationToCustomner();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<FCMResponse> call, Throwable t) {
                                        Log.d(TAG, t.getMessage());
                                        notifyFailedNotificationToCustomner();
                                    }
                                });
                    }
                });


    }

    private void notifyFailedNotificationToCustomner() {
        if (tryAgainCount < 3)   {
            sendNotificationToCustomer();
        } else {
            MyCustomAlertDialog dialog = MyCustomAlertDialog.create(ReceiveCallWorkerRequestActivity.this);
            dialog.setTitle("Thông báo")
                    .setMessage("Gửi thông báo cho khách hàng xảy ra lỗi. Hãy thử lại xem sao.")
                    .setPositiveButton("Thử lại", v -> {
                        dialog.dismiss();
                        sendNotificationToCustomer();
                    })
                    .setNegativeButton("Hủy", v -> {
                        dialog.dismiss();
                        finish();
                    })
                    .show();
        }
    }
}
