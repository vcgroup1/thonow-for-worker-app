package dev.vcgroup.thonowforworkerandroidapp.ui.forgotpasswordscreen;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.CountryInfo;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentForgotPasswordStepOneBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.signinscreen.SignInActivity;
import dev.vcgroup.thonowforworkerandroidapp.util.AuthenticationUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.FragmentUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.MyCustomAlertDialog;

public class ForgotPasswordStepOneFragment extends Fragment {
    private static final String TAG = ForgotPasswordStepOneFragment.class.getSimpleName();
    private FragmentForgotPasswordStepOneBinding binding;
    private ForgotPasswordViewModel mViewModel;
    private static FirebaseAuth mAuth;
    private static FirebaseFirestore db;
    static {
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forgot_password_step_one, container, false);
        mViewModel = ViewModelProviders.of(requireActivity()).get(ForgotPasswordViewModel.class);
        mViewModel.setSelectedCountryInfo(new CountryInfo("Vietnam", "VN", "84"));
        binding.setItem(mViewModel);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.tipPhone.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkInputText(charSequence.toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkInputText(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                checkInputText(editable.toString());
            }
        });

        binding.fabForgetStepOneNext.setOnClickListener(view1 -> {
            mViewModel.setPhoneNumber(CommonUtil.getInputText(binding.tipPhone));
            isAccountExists();
        });
    }

    private void isAccountExists() {
        if (mViewModel.getPhoneNumberProto().getValue() != null) {
            String email = AuthenticationUtil.generateEmailFromNationalNumberPhone(String.valueOf(mViewModel.getPhoneNumberProto().getValue().getNationalNumber()));
            db.collection(CollectionConst.COLLECTION_WORKER)
                    .whereEqualTo("email", email)
                    .get()
                    .addOnSuccessListener(queryDocumentSnapshots -> {
                        if (queryDocumentSnapshots.isEmpty()) {
                            showCreateNewAccountRecommend();
                        } else {
                            showForgotPasswordConfirmation();
                        }
                    })
                    .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
        } else {
            MyCustomAlertDialog dialog = MyCustomAlertDialog.create(requireActivity());
            dialog.setTitle("Thông báo")
                    .setMessage("Có lỗi xảy ra. Vui lòng kiểm tra internet hoặc dữ liệu nhập!")
                    .setPositiveButton("Đã hiểu", v -> dialog.dismiss())
                    .show();
        }

    }

    private void showCreateNewAccountRecommend() {
        MyCustomAlertDialog dialog = MyCustomAlertDialog.create(requireActivity());
        String message = "<h4 style=\"text-align: center;\"><strong>" + AuthenticationUtil.formatNumberPhone(mViewModel.getPhoneNumberProto().getValue(), PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL) + "</strong></h4>" + "Không có tài khoản nào với số điện thoại này. Bạn có muốn tạo tài khoản mới chứ?";
        dialog.setTitle("Xác nhận")
                .setMessage(Html.fromHtml(message, Html.FROM_HTML_MODE_COMPACT))
                .setPositiveButton("Đăng ký tài khoản", v -> {
                    Intent intent = new Intent(requireActivity(), SignInActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                })
                .setNegativeButton("Quay lại", v -> {
                    dialog.dismiss();
                })
                .setOnKeyListener((dialog1, keyCode, event) -> {
                    if (keyCode == KeyEvent.KEYCODE_BACK)
                        dialog.dismiss();
                    return true;
                })
                .show();
    }

    private void showForgotPasswordConfirmation() {
        MyCustomAlertDialog dialog = MyCustomAlertDialog.create(requireActivity());
        String message = "<h4 style=\"text-align: center;\"><strong>" + AuthenticationUtil.formatNumberPhone(mViewModel.getPhoneNumberProto().getValue(), PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL) + "</strong></h4>" + "Chúng tôi sẽ gửi mã xác nhận đến số điện thoại trên để kích hoạt tài khoản của bạn. Vui lòng xác nhận thông tin này là đúng.";
        dialog.setTitle("Xác nhận")
                .setMessage(Html.fromHtml(message, Html.FROM_HTML_MODE_COMPACT))
                .setPositiveButton("Xác nhận", v -> {
                    dialog.dismiss();
                    FragmentUtil.replaceFragment(requireActivity(), R.id.forgot_password_fragment_container,
                            new ForgotPasswordStepTwoFragment(), null, ForgotPasswordStepTwoFragment.class.getSimpleName(), true);
                })
                .setNegativeButton("Thay đổi", v -> {
                    dialog.dismiss();
                })
                .setOnKeyListener((dialog1, keyCode, event) -> {
                    if (keyCode == KeyEvent.KEYCODE_BACK)
                        dialog.dismiss();
                    return true;
                })
                .show();
    }

    private void checkInputText(String str) {
        binding.tipPhone.setError(null);
        if (!str.isEmpty() && !AuthenticationUtil.isValidNumber(mViewModel.getPhoneNumber().getValue(),
                Objects.requireNonNull(mViewModel.getSelectedCountryInfo().getValue()).getAlp2Code())) {
            binding.fabForgetStepOneNext.setClickable(true);
            binding.fabForgetStepOneNext.setAlpha(1f);
        } else {
            binding.tipPhone.setError("Vui lòng nhập số điện thoại hợp lệ!");
            binding.fabForgetStepOneNext.setClickable(false);
            binding.fabForgetStepOneNext.setAlpha(.2f);
        }
    }
}