package dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.materialcostmanagement;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.data.model.MaterialCost;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivityMaterialCostManagementBinding;
import dev.vcgroup.thonowforworkerandroidapp.databinding.BsdAddMaterialCostBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.adapter.MaterialCostAdapter;
import dev.vcgroup.thonowforworkerandroidapp.util.SpacesItemDecoration;

public class MaterialCostManagementActivity extends AppCompatActivity implements MaterialCostAdapter.OnMaterialCostListener {
    private static final String TAG = MaterialCostManagementActivity.class.getSimpleName();
    private ActivityMaterialCostManagementBinding binding;
    private MaterialCostAdapter adapter;
    private BottomSheetDialog materialCostBsd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_material_cost_management);

        setupToolbar();
        if (getIntent().getExtras() != null) {
            List<MaterialCost> list = Parcels.unwrap(getIntent().getParcelableExtra("materialCosts"));
            String orderId = getIntent().getStringExtra("orderId");
            setupRvMaterialCost(list, orderId);
        }

        binding.btnAddMaterialCost.setOnClickListener(v -> {
            setupBsdMaterialCost(v, -1);
        });
    }

    private void setupRvMaterialCost(List<MaterialCost> list, String orderId) {
        if (list == null) {
            list = new ArrayList<>();
        }
        adapter = new MaterialCostAdapter(list, this);
        binding.rvMaterialCosts.setAdapter(adapter);
        binding.rvMaterialCosts.addItemDecoration(new SpacesItemDecoration(10));
        adapter.update(list);
    }

    private void setupBsdMaterialCost(View view, int position) {
        BsdAddMaterialCostBinding bsdMaterialCostBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.bsd_add_material_cost, (ViewGroup) view.getRootView(), false);
        materialCostBsd = new BottomSheetDialog(this);
        materialCostBsd.setContentView(bsdMaterialCostBinding.getRoot());

        if (position >= 0) {
            MaterialCost item = adapter.getItemAt(position);
            bsdMaterialCostBinding.setItem(item);
            bsdMaterialCostBinding.btnDelete.setVisibility(View.VISIBLE);
        }

        materialCostBsd.show();

        bsdMaterialCostBinding.btnClose.setOnClickListener(v -> materialCostBsd.dismiss());

        bsdMaterialCostBinding.btnSave.setOnClickListener(v -> {
            MaterialCost item = new MaterialCost(bsdMaterialCostBinding.tipMaterialName.getText().toString(),
                    Double.parseDouble(bsdMaterialCostBinding.tipMaterialCost.getText().toString()));
            adapter.addOrUpdateItem(item);
            materialCostBsd.dismiss();
        });

        bsdMaterialCostBinding.btnDelete.setOnClickListener(v -> {
            adapter.deleteItem(position);
            materialCostBsd.dismiss();
        });
    }

    private void setupToolbar() {
        Toolbar mToolbar = binding.topAppBar.mToolbar;
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        binding.topAppBar.tvTitleToolbar.setText("Phí vật tư sử dụng");
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (adapter.getCurrentList() != null && !adapter.getCurrentList().isEmpty()) {
                Intent intent = new Intent();
                intent.putExtra("materialCosts", Parcels.wrap(new ArrayList<>(adapter.getCurrentList())));
                setResult(RESULT_OK, intent);
            }
            finish();
            return true;
        }
        return false;
    }

    @Override
    public void onRemoveMaterialCost(View view, int position) {
        setupBsdMaterialCost(view, position);
    }
}