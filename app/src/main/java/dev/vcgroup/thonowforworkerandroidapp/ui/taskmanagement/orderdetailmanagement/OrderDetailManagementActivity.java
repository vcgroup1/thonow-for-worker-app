package dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.orderdetailmanagement;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.firestore.FirebaseFirestore;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Order;
import dev.vcgroup.thonowforworkerandroidapp.data.model.OrderDetail;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Service;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivityOrderDetailManagementBinding;
import dev.vcgroup.thonowforworkerandroidapp.databinding.BsdOrderDetailBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.adapter.OrderDetailsAdapter;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.selectservice.SelectServiceActivity;
import dev.vcgroup.thonowforworkerandroidapp.util.SpacesItemDecoration;

import static dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst.SELECT_SERVICE_REQ_CODE;

public class OrderDetailManagementActivity extends AppCompatActivity implements OrderDetailsAdapter.OnOrderDetailListener {
    private ActivityOrderDetailManagementBinding binding;
    private BottomSheetDialog orderDetailBsd;
    private OrderDetailsAdapter orderDetailsAdapter;
    private Order order;
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }

    private Service currentService = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order_detail_management);
        setupToolbar();
        if (getIntent().hasExtra("order")) {
            order = Parcels.unwrap(getIntent().getParcelableExtra("order"));
            setupRv(order.getOrderDetails());
        }
        binding.btnAddService.setOnClickListener(v -> {
            setupBsdService(v, -1);
        });
    }

    private void setupRv (List<OrderDetail> list) {
        if (list == null) {
            list = new ArrayList<>();
        }
        orderDetailsAdapter = new OrderDetailsAdapter(list, this);
        binding.rvOrderDetails.setAdapter(orderDetailsAdapter);
        binding.rvOrderDetails.addItemDecoration(new SpacesItemDecoration(10));
        orderDetailsAdapter.update(list);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (orderDetailsAdapter.getCurrentList() != null && !orderDetailsAdapter.getCurrentList().isEmpty()) {
                Intent intent = new Intent();
                intent.putExtra("orderDetails", Parcels.wrap(new ArrayList<>(orderDetailsAdapter.getCurrentList())));
                setResult(RESULT_OK, intent);
            }
            finish();
            return true;
        }
        return false;
    }

    private void setupToolbar() {
        Toolbar toolbar = binding.topAppBar.mToolbar;
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        binding.topAppBar.tvTitleToolbar.setText("Chi tiết đơn hàng");
    }

    private BsdOrderDetailBinding bsdOrderDetailBinding;
    private String serviceTypeId;
    private void setupBsdService(View view, int position) {
        bsdOrderDetailBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.bsd_order_detail, (ViewGroup) view.getRootView(), false);
        orderDetailBsd = new BottomSheetDialog(this);
        orderDetailBsd.setContentView(bsdOrderDetailBinding.getRoot());

        OrderDetail orderDetail = position == -1 ? new OrderDetail() : orderDetailsAdapter.getItemAt(position);

        if (position > -1) {
            orderDetail.getService().get()
                    .addOnSuccessListener(documentSnapshot -> {
                        currentService = documentSnapshot.toObject(Service.class);
                        if (position >= 0) {
                            bsdOrderDetailBinding.setItem(orderDetail);
                            bsdOrderDetailBinding.setService(currentService);
                            if (orderDetailsAdapter.getItemCount() > 1) {
                                bsdOrderDetailBinding.btnDelete.setVisibility(View.VISIBLE);
                            }
                        }
                    });
        } else {
            order.getOrderDetails()
                    .get(0)
                    .getService()
                    .get()
                    .addOnSuccessListener(documentSnapshot -> {
                        serviceTypeId = documentSnapshot.toObject(Service.class).getServiceType().getId();
                    });
        }


        orderDetailBsd.show();

        bsdOrderDetailBinding.btnClose.setOnClickListener(v -> orderDetailBsd.dismiss());

        bsdOrderDetailBinding.btnSelectService.setOnClickListener(v -> {
            Intent intent = new Intent(this, SelectServiceActivity.class);
            if (currentService != null) {
                intent.putExtra("serviceId", currentService.getId());
                intent.putExtra("serviceTypeId", currentService.getServiceType().getId());
            } else {
                Log.d("serviceTypeId", serviceTypeId);
                intent.putExtra("serviceTypeId", serviceTypeId);
            }
            startActivityForResult(intent, SELECT_SERVICE_REQ_CODE);
        });

        bsdOrderDetailBinding.btnSave.setOnClickListener(v -> {
            orderDetail.setQuantity(Integer.parseInt(bsdOrderDetailBinding.tipQuantity.getText().toString()));
            orderDetail.setService(db.collection(CollectionConst.COLLECTION_SERVICE).document(currentService.getId()));



            new Handler().postDelayed(() -> {
                Log.d("service", orderDetail.toString());
                orderDetailsAdapter.addOrUpdateItem(orderDetail);
                orderDetailBsd.dismiss();
            }, 1000);
        });

        bsdOrderDetailBinding.btnDelete.setOnClickListener(v -> {
            orderDetailsAdapter.deleteItem(position);
            orderDetailBsd.dismiss();
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == SELECT_SERVICE_REQ_CODE && resultCode == RESULT_OK && data != null) {
            currentService = Parcels.unwrap(data.getParcelableExtra("selectedService"));
            bsdOrderDetailBinding.setService(currentService);
        }
    }

    @Override
    public void onOrderDetailClickedListener(View view, int position) {
        setupBsdService(view, position);
    }
}