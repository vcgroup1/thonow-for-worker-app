package dev.vcgroup.thonowforworkerandroidapp.ui.walkthroughscreen;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import dev.vcgroup.thonowforworkerandroidapp.R;
import lombok.AllArgsConstructor;
import lombok.Data;

public class WalkthroughSliderAdapter extends RecyclerView.Adapter<WalkthroughSliderAdapter.WalkthroughSliderVH> {
    private List<WalkthroughSliderItem> sliderItemList;

    public WalkthroughSliderAdapter() {
        sliderItemList = new ArrayList<>();
        sliderItemList.add(new WalkthroughSliderItem(R.drawable.ic_slider1,
                "Dễ dàng đăng ký, dễ dàng tăng thu nhập",
                "Hãy kết nối với chúng tôi, ứng dụng đang kết nối hơn hàng ngàn người dùng, đối tác,...để tạo nhiều cơ hội cho công việc của bạn."));
        sliderItemList.add(new WalkthroughSliderItem(R.drawable.ic_slider2,
                "Dễ dàng kết nối với khách hàng đa dạng",
                "Chỉ với hai bước, mọi nhu cầu về sửa chữa, bảo dưỡng nhà cửa và đồ đạc gia dụng của khách hàng sẽ đến tay bạn thông qua ThoNOW."));
        sliderItemList.add(new WalkthroughSliderItem(R.drawable.ic_slider3,
                "Chúng ta là một gia đình",
                "Thật vinh dự khi mà chúng tôi là một phần trong cuộc sống của các đới tác. Đó là lý do tại sao nhiều chương trình được tổ chức để mang đến trải nghiệm tốt nhất cho các bạn."));
        sliderItemList.add(new WalkthroughSliderItem(R.drawable.ic_slider4,
                "Bảo hiểm & sự hỗ trợ 24/7",
                "Chúng tôi chăm lo cho công việc cuả bạn với bảo hiểm tai nạn, tổng đài 24/07, đội ứng phó tình huống khẩn cấp."));
    }

    @NonNull
    @org.jetbrains.annotations.NotNull
    @Override
    public WalkthroughSliderAdapter.WalkthroughSliderVH onCreateViewHolder(@NonNull @org.jetbrains.annotations.NotNull ViewGroup parent, int viewType) {
        return new WalkthroughSliderVH(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_in_app_welcome_slider, parent, false
        ));
    }

    @Override
    public void onBindViewHolder(@NonNull @org.jetbrains.annotations.NotNull WalkthroughSliderAdapter.WalkthroughSliderVH holder, int position) {
        WalkthroughSliderItem item = sliderItemList.get(position);
        holder.setWelcomeSliderContent(item);
    }

    @Override
    public int getItemCount() {
        return sliderItemList == null ? 0 : sliderItemList.size();
    }

    public class WalkthroughSliderVH extends RecyclerView.ViewHolder {
        private ImageView image;
        private TextView title;
        private TextView description;

        public WalkthroughSliderVH(@NonNull @org.jetbrains.annotations.NotNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.welcome_item_thumbnail);
            title = itemView.findViewById(R.id.welcome_item_title);
            description = itemView.findViewById(R.id.welcome_item_desc);
        }

        public void setWelcomeSliderContent(WalkthroughSliderItem item) {
            image.setImageResource(item.getImage());
            title.setText(item.getTitle());
            description.setText(item.getDescription());
        }
    }

    @Data
    @AllArgsConstructor
    public class WalkthroughSliderItem {
        private int image;
        private String title;
        private String description;
    }
}
