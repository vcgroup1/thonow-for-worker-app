package dev.vcgroup.thonowforworkerandroidapp.ui.profile.mywalletmanagement.chargemoney;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentChargeMoneyStepOneBinding;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.FragmentUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.MyCustomAlertDialog;

public class ChargeMoneyStepOneFragment extends Fragment {
    private FragmentChargeMoneyStepOneBinding binding;
    private ChargeMoneyViewModel mViewModel;

    public ChargeMoneyStepOneFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_charge_money_step_one, container, false);
        mViewModel = ViewModelProviders.of(requireActivity()).get(ChargeMoneyViewModel.class);
        binding.setItem(mViewModel);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //binding.tipAmount.setText(mViewModel.getAmount().getValue().toString());
        binding.btnChargeMoneyNow.setOnClickListener(v -> {
            long amount = Long.parseLong(CommonUtil.getInputText(binding.tipAmount));
            if (amount >= 1000 && amount <= 20000000) {
                mViewModel.setAmount(amount);
                FragmentUtil.replaceFragment(requireActivity(), R.id.charge_money_fragment_container, new ChooseTransactionMethodFragment(),
                        null, ChooseTransactionMethodFragment.class.getSimpleName(), true);
            } else {
                MyCustomAlertDialog dialog = MyCustomAlertDialog.create(requireActivity());
                dialog.setTitle("Thông báo")
                        .setMessage("Số tiền nạp tiền không đúng. Chúng tôi chỉ chấp nhận " +
                                "trong khoảng 1.000đ đến 20.000.000đ")
                        .setPositiveButton("Đã hiểu", v1 -> {
                            dialog.dismiss();
                        })
                        .show();
            }

        });

    }
}