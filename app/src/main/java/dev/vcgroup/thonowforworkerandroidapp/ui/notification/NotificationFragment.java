package dev.vcgroup.thonowforworkerandroidapp.ui.notification;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayoutMediator;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentNotificationBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.notification.adapter.NotificationPagerAdapter;

public class NotificationFragment extends Fragment {
    private FragmentNotificationBinding binding;

    public static NotificationFragment newInstance() {
        return new NotificationFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification, container,
                false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupTabLayout();
    }

    private void setupTabLayout() {
        NotificationPagerAdapter adapter = new NotificationPagerAdapter(getChildFragmentManager(), this.getLifecycle());
        binding.notificationViewpager.setAdapter(adapter);
        new TabLayoutMediator(binding.notificationTablayout, binding.notificationViewpager, (tab, position) -> {
            switch (position) {
                case 0:
                    tab.setText("Hộp thư");
                    tab.setIcon(R.drawable.ic_inbox);
                    break;
                case 1:
                    tab.setText("Thông báo");
                    tab.setIcon(R.drawable.ic_bell);
                    break;
            }
        }).attach();
    }

}