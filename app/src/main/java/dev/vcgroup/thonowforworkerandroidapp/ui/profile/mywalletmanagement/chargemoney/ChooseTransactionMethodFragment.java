package dev.vcgroup.thonowforworkerandroidapp.ui.profile.mywalletmanagement.chargemoney;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.constant.epayment.MoMoParameters;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Transaction;
import dev.vcgroup.thonowforworkerandroidapp.data.model.TransactionMethod;
import dev.vcgroup.thonowforworkerandroidapp.data.model.TransactionType;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Worker;
import dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.request.PaymentProcessingRequest;
import dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.request.TransactionConfirmationRequest;
import dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.response.PaymentProcessingResponse;
import dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.momo.response.TransactionConfirmationResponse;
import dev.vcgroup.thonowforworkerandroidapp.data.model.epayment.vnpay.CreatePaymentRequest;
import dev.vcgroup.thonowforworkerandroidapp.data.remote.epaymentservice.EPaymentService;
import dev.vcgroup.thonowforworkerandroidapp.data.remote.epaymentservice.PaymentRetrofitClientInstance;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentChooseTransactionMethodBinding;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.FragmentUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.GenericWebview;
import dev.vcgroup.thonowforworkerandroidapp.util.ewallet.EPaymentUtil;
import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.momo.momo_partner.AppMoMoLib;

public class ChooseTransactionMethodFragment extends Fragment implements View.OnClickListener {
	private static final String TAG = ChooseTransactionMethodFragment.class.getSimpleName();
	private FragmentChooseTransactionMethodBinding binding;
	private ChargeMoneyViewModel mViewModel;
	private static EPaymentService ePaymentService;
	private static FirebaseFirestore db;
	static {
		ePaymentService =
				PaymentRetrofitClientInstance.getInstance(TransactionMethod.MOMO).getRetrofit().create(EPaymentService.class);
		db = FirebaseFirestore.getInstance();
	}

	public ChooseTransactionMethodFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		binding = DataBindingUtil.inflate(inflater, R.layout.fragment_choose_transaction_method, container, false);
		mViewModel = ViewModelProviders.of(requireActivity()).get(ChargeMoneyViewModel.class);
		return binding.getRoot();
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		binding.rdBtnMomo.setOnClickListener(this);
		binding.rdBtnZaloPay.setOnClickListener(this);
		binding.btnMomo.setOnClickListener(this);
		binding.btnZaloPay.setOnClickListener(this);
		binding.btnVnpay.setOnClickListener(this);
		binding.rdBtnVnpay.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_zalo_pay:
			case R.id.rd_btn_zalo_pay:
				binding.rdBtnMomo.setChecked(false);
				binding.rdBtnVnpay.setChecked(false);
				binding.rdBtnZaloPay.setChecked(true);
				new Handler().postDelayed(() -> {

				}, 500);

				break;
			case R.id.btn_momo:
			case R.id.rd_btn_momo:
				binding.rdBtnZaloPay.setChecked(false);
				binding.rdBtnVnpay.setChecked(false);
				binding.rdBtnMomo.setChecked(true);
				new Handler().postDelayed(this::chargeMoneyThroughMomo, 500);

				break;
			case R.id.btn_vnpay:
			case R.id.rd_btn_vnpay:
				binding.rdBtnZaloPay.setChecked(false);
				binding.rdBtnMomo.setChecked(false);
				binding.rdBtnVnpay.setChecked(true);
				new Handler().postDelayed(this::chargeMoneyThroughVnPay, 500);

				break;
		}
	}

	private void chargeMoneyThroughVnPay() {
		try {
			CreatePaymentRequest vnPaymentRequest =
					CreatePaymentRequest.createFrom(requireActivity(),
							mViewModel.getAmount().getValue(), null);
			String paymentUrl = EPaymentUtil.getPaymentUrl(vnPaymentRequest);
			Log.d(TAG, paymentUrl);
			GenericWebview.create(binding.wvVnpay, paymentUrl, () -> {
				binding.wvVnpay.setVisibility(View.GONE);
				updateBalanceMoney(vnPaymentRequest.getVnp_TxnRef());
			});
			binding.wvVnpay.setVisibility(View.VISIBLE);
		} catch (UnknownHostException | NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}

	private Map<String, Object> eventValue;

	private void chargeMoneyThroughMomo() {
		AppMoMoLib.getInstance().setEnvironment(AppMoMoLib.ENVIRONMENT.DEVELOPMENT);
		AppMoMoLib.getInstance().setAction(AppMoMoLib.ACTION.PAYMENT);
		AppMoMoLib.getInstance().setActionType(AppMoMoLib.ACTION_TYPE.GET_TOKEN);
		eventValue = EPaymentUtil.getMomoData(mViewModel.getAmount().getValue());
		Log.d(TAG, eventValue.toString());
		AppMoMoLib.getInstance().requestMoMoCallBack(requireActivity(), eventValue);
	}

	@SneakyThrows
	@RequiresApi(api = Build.VERSION_CODES.O)
	@Override
	public void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == AppMoMoLib.getInstance().REQUEST_CODE_MOMO && resultCode == Activity.RESULT_OK) {
			if (data != null) {
				EPaymentUtil.MomoResponse response = EPaymentUtil.getResponse(data);

				if (response.getStatus() == 0) { // User xác nhận thanh toán thành công
					String token = response.getData();
					if (token != null && !token.isEmpty()) {
						eventValue.put(MoMoParameters.PHONE_NUMBER_V1, response.getPhoneNumber());
						eventValue.put(MoMoParameters.APP_DATA, response.getData());
						processMomoPayment();
					} else {
						Log.d(TAG, "token = null");
						Toast.makeText(requireActivity(), this.getString(R.string.not_receive_info), Toast.LENGTH_SHORT).show();
					}
				} else if (response.getStatus() == 5) { //Hết thời gian thực hiện giao dịch (Timeout transaction)
					Toast.makeText(requireActivity(), "Hết thời gian thực hiện giao dịch", Toast.LENGTH_SHORT).show();
					Log.d(TAG, response.getMessage());
				} else { // User huỷ giao dịch
					Log.d(TAG, response.getMessage());
				}
			} else {
				Log.d(TAG, "data == null");
				Toast.makeText(requireActivity(), this.getString(R.string.not_receive_info), Toast.LENGTH_SHORT).show();
			}
		} else {
			Log.d(TAG, "không chuyển tiếp");
			Toast.makeText(requireActivity(), this.getString(R.string.not_receive_info), Toast.LENGTH_SHORT).show();
		}
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	private void processMomoPayment() throws Exception {
		PaymentProcessingRequest request = PaymentProcessingRequest.createFrom(eventValue);
		Log.d(TAG, "request: " + request.toString());
		ePaymentService.processPayment(request)
				.enqueue(new Callback<PaymentProcessingResponse>() {
					@SneakyThrows
					@Override
					public void onResponse(Call<PaymentProcessingResponse> call, Response<PaymentProcessingResponse> response) {
						if (response.isSuccessful()) {
							PaymentProcessingResponse rsp = response.body();
							if (rsp != null) {
								Log.d(TAG, rsp.toString());
								if (rsp.getStatus() == 0) { // Giao dịch thành công
									TransactionConfirmationRequest confirmationRequest = TransactionConfirmationRequest.createFrom(request,
											rsp);
									processTransactionConfirmation(confirmationRequest);
								} else {
									Log.d(TAG, "processPayment: " + rsp.getMessage());
									Toast.makeText(requireActivity(), rsp.getMessage(), Toast.LENGTH_SHORT).show();
								}
							}

						} else {
							Toast.makeText(requireActivity(), "Không thể thực hiện giao dịch. Vui" +
											" lòng thử lại!",
									Toast.LENGTH_SHORT).show();
						}
					}

					@Override
					public void onFailure(Call<PaymentProcessingResponse> call, Throwable t) {
						Log.d(TAG, t.getMessage());
						Toast.makeText(requireActivity(), "Đã xảy ra lỗi gì đó. Xin hãy thử lại một lần nữa!", Toast.LENGTH_SHORT).show();
					}
				});
	}

	private void processTransactionConfirmation(TransactionConfirmationRequest confirmationRequest) {
		ePaymentService.processPaymentConfirmation(confirmationRequest)
				.enqueue(new Callback<TransactionConfirmationResponse>() {
					@Override
					public void onResponse(Call<TransactionConfirmationResponse> call, Response<TransactionConfirmationResponse> response) {
						if (response.isSuccessful()) {
							TransactionConfirmationResponse confirmationResponse = response.body();
							if (confirmationResponse != null) {
								Log.d(TAG, confirmationResponse.toString());

								if (confirmationResponse.getStatus() == 0) {
									// update balance_money
									updateBalanceMoney(confirmationResponse.getData().getMomoTransId());
									// show success screen
								} else {
									Toast.makeText(requireActivity(), "Giao dịch xử lý thất bại!",
											Toast.LENGTH_SHORT).show();
								}
							} else {
								Toast.makeText(requireActivity(), "Xác nhận giao dịch xảy ra lỗi!", Toast.LENGTH_SHORT).show();
							}
						} else {
							Toast.makeText(requireActivity(), "Oops! Dường như đã có lỗi xảy ra. " +
											"Hãy thử lại!",
									Toast.LENGTH_SHORT).show();
						}
					}

					@Override
					public void onFailure(Call<TransactionConfirmationResponse> call, Throwable t) {
						Log.d(TAG, t.getMessage());
						Toast.makeText(requireActivity(), "Oops! Dường như đã có lỗi xảy ra. " +
										"Hãy thử lại!",
								Toast.LENGTH_SHORT).show();
					}
				});
	}

	private void updateBalanceMoney(String momoTransId) {
		DocumentReference currentUserDocRef = CommonUtil.getCurrentUserRef(db);
		currentUserDocRef
				.get()
				.addOnSuccessListener(documentSnapshot -> {
					insertTransactionHistory();
					Worker worker = documentSnapshot.toObject(Worker.class);
					double balanceMoney =
							worker.getBalanceMoney() + mViewModel.getAmount().getValue();
					currentUserDocRef
							.update("balanceMoney", balanceMoney)
							.addOnSuccessListener(command -> {
								Bundle bundle = new Bundle();
								bundle.putLong("amount", mViewModel.getAmount().getValue());
								bundle.putDouble("balanceMoney", balanceMoney);
								bundle.putString("transactionId", momoTransId);
								FragmentUtil.replaceFragment(requireActivity(),
										R.id.charge_money_fragment_container, new ThankYourForChargeMoneyFragment(),
										bundle, ThankYourForChargeMoneyFragment.class.getSimpleName(),
										true);
							})
							.addOnFailureListener(e -> {
								Toast.makeText(requireActivity(), "Cập nhật số dư tài khoản xảy ra lỗi. Chúng tôi sẽ cập nhật sau!", Toast.LENGTH_SHORT).show();
								Log.d(TAG, e.getMessage());
							});
				})
				.addOnFailureListener(e -> {
					Toast.makeText(requireActivity(), "Cập nhật số dư tài khoản xảy ra lỗi. Chúng tôi sẽ cập nhật sau!", Toast.LENGTH_SHORT).show();
					Log.d(TAG, e.getMessage());
				});
	}

	private void insertTransactionHistory() {
		Transaction transaction = Transaction.newObject(db, mViewModel.getAmount().getValue(),
				TransactionType.CHARGE_MONEY);
		db.collection(CollectionConst.COLLECTION_TRANSACTION_HISTORY)
				.add(transaction);
	}


}