package dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.basicpersonalinformation;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;

public class BasicProfileStepAdapter extends AbstractFragmentStepAdapter {

    public BasicProfileStepAdapter(@NonNull FragmentManager fm, @NonNull Context context) {
        super(fm, context);
    }

    @Override
    public Step createStep(int position) {
        switch (position) {
            case 0:
                return BasicProfileStepOneFragment.newInstance();
            case 1:
                return BasicProfileStepTwoFragment.newInstance();
            case 2:
                return BasicProfileStepThreeFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
