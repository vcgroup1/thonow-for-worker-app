package dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.taskbycalendar;

import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.FirebaseFirestore;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Order;
import dev.vcgroup.thonowforworkerandroidapp.data.model.OrderStatus;
import dev.vcgroup.thonowforworkerandroidapp.data.model.User;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ItemTaskBinding;
import dev.vcgroup.thonowforworkerandroidapp.databinding.TaskByCalendarFragmentBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.audiocall.callscreen.CallScreenActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.message.chatwindow.ChatActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.TaskManagementViewModel;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.adapter.TaskListAdapter;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.cancelorder.CancelOrderActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.confirmationofpayment.ConfirmationOfPaymentActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.taskdetail.TaskDetailActivity;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.MyCustomAlertDialog;
import dev.vcgroup.thonowforworkerandroidapp.util.SpacesItemDecoration;
import lombok.SneakyThrows;

import static dev.vcgroup.thonowforworkerandroidapp.constant.AudioCallConst.OUT_GOING_CALL;
import static dev.vcgroup.thonowforworkerandroidapp.constant.CommonConst.CALL_PERMISSIONS;
import static dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst.CANCEL_ORDER_REQ_CODE;
import static dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst.CONFIRMATION_OF_PAYMENT_REQ_CODE;
import static dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst.SEE_DETAIL_ORDER_REQ_CODE;

public class TaskByCalendarFragment extends Fragment implements TaskListAdapter.OnTaskListener {
    private static final String TAG = TaskByCalendarFragment.class.getSimpleName();
    private TaskByCalendarFragmentBinding binding;
    private CalendarView calendarView;
    private TaskListAdapter adapter;
    private TaskManagementViewModel mViewModel;
    private Calendar selectedCalendar;
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }

    public static TaskByCalendarFragment newInstance() {
        return new TaskByCalendarFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.task_by_calendar_fragment, container, false);
        mViewModel = ViewModelProviders.of(requireActivity()).get(TaskManagementViewModel.class);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupRvTaskList();
        setupCalendarView();
    }

    private void setupRvTaskList() {
        adapter = new TaskListAdapter(new ArrayList<>(), TaskListAdapter.TaskAdapterType.TASK_BY_CALENDAR_ITEM, this);
        binding.rvTaskListByCalendar.setAdapter(adapter);
        binding.rvTaskListByCalendar.addItemDecoration(new SpacesItemDecoration(20));
    }

    @SneakyThrows
    private void setupCalendarView() {
        calendarView = binding.calendarView;
        calendarView.performClick();
        
        // set current date
        selectedCalendar = Calendar.getInstance();
        calendarView.setDate(selectedCalendar);
        onTaskFilterByDate(selectedCalendar.getTime());

        calendarView.setOnDayClickListener(eventDay -> {
            selectedCalendar = eventDay.getCalendar();
            onTaskFilterByDate(selectedCalendar.getTime());
        });
        calendarView.showCurrentMonthPage();
    }

    private void onTaskFilterByDate(Date date) {
        List<EventDay> events = new ArrayList<>();
//        List<Order> results;
        mViewModel.getOrders().observe(requireActivity(), orders -> {
            List<Order> results = new ArrayList<>();
            if (orders != null && !orders.isEmpty()) {
                orders.forEach(order -> {
                    if (CommonUtil.isSameDay(date, order.getWorkingDate()) ) {
                        results.add(order);
                    }

                    Calendar calendar1 = Calendar.getInstance();
                    calendar1.setTime(order.getWorkingDate());
                    events.add(new EventDay(calendar1, R.drawable.ic_repair_event));

//                    int index = adapter.getItems().indexOf(order);
//                    if (index >= 0) {
//                        results.set(index, order);
//                    }
                });
            }
            adapter.update(results);
            calendarView.setEvents(events);
            onFilterListener(results.size());
        });
    }

    private void onFilterListener(int count) {
        if (count == 0) {
            binding.noTaskSchedule.getRoot().setVisibility(View.VISIBLE);
            binding.rvTaskListByCalendar.setVisibility(View.GONE);
        } else {
            binding.noTaskSchedule.getRoot().setVisibility(View.GONE);
            binding.rvTaskListByCalendar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onTaskClickedListener(int position) {
        Order order = adapter.getItem(position);
        navigateToOrderDetailScreen(order);
    }

    private void navigateToOrderDetailScreen(Order order) {
        Intent intent = new Intent(requireActivity(), TaskDetailActivity.class);
        intent.putExtra("order", Parcels.wrap(order));
        Log.d(TAG, selectedCalendar.getTime() + "");
        startActivityForResult(intent, SEE_DETAIL_ORDER_REQ_CODE);
    }

    @Override
    public void onTaskNextStatusClickedListener(int position) {
        Order order = adapter.getItem(position);

        switch (order.getStatus()) {
            case ARRIVED:
            case INVESTIGATING:
                navigateToOrderDetailScreen(order);
                break;
            case WAIT_FOR_CONFIRMATION_TO_PAY:
                Intent intent1 = new Intent(requireActivity(), ConfirmationOfPaymentActivity.class);
                intent1.putExtra("order", Parcels.wrap(order));
                startActivityForResult(intent1, CONFIRMATION_OF_PAYMENT_REQ_CODE);
                break;
            default:  updateOrderStatus(position, order);
        }
    }

    private void updateOrderStatus(int position, Order order) {
        OrderStatus status = order.getStatus().nextStatus();
        db.collection(CollectionConst.COLLECTION_ORDER)
                .document(order.getId())
                .update(
                        "status", status,
                        "finishDate", status == OrderStatus.COMPLETED ? new Date() : null
                )
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        adapter.notifyItemChanged(position);
                        //onTaskFilterByDate(selectedCalendar.getTime());
                    } else {
                        Log.d(TAG, Objects.requireNonNull(task.getException()).getMessage());
                    }
                });
    }


    @Override
    public void onTaskCancelClickedListener(int position) {
        String orderId = adapter.getItem(position).getId();
        Intent intent = new Intent(requireActivity(), CancelOrderActivity.class);
        intent.putExtra("deleteOrderId", orderId);
        startActivityForResult(intent, CANCEL_ORDER_REQ_CODE);
    }

    @Override
    public void onChatClickedListener(int position) {
        Order order = adapter.getItem(position);
        String orderId = order.getId();
        Intent intent = new Intent(requireActivity(), ChatActivity.class);
        intent.putExtra("orderId", orderId);
        intent.putExtra("customerId", order.getCustomer().getId());
        startActivity(intent, ActivityOptions.makeCustomAnimation(getActivity(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
    }

    @Override
    public void onAudioCallClickedListener(int position) {
        Order order = adapter.getItem(position);

        if (!TedPermission.isGranted(requireActivity(), CALL_PERMISSIONS)) {
            TedPermission.with(requireActivity())
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            callToCustomer(order);
                        }

                        @Override
                        public void onPermissionDenied(List<String> deniedPermissions) {
                            Snackbar.make(binding.getRoot(), "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                        }
                    })
                    .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                    .setPermissions(CALL_PERMISSIONS)
                    .check();
        } else {
            callToCustomer(order);
        }
    }

    private void callToCustomer(Order order) {
        order.getCustomer()
                .get()
                .addOnSuccessListener(documentSnapshot1 -> {
                    Intent intent = new Intent(requireActivity(), CallScreenActivity.class);
                    intent.putExtra("customer", Parcels.wrap(documentSnapshot1.toObject(User.class)));
                    intent.putExtra(OUT_GOING_CALL, true);
                    startActivity(intent, ActivityOptions.makeCustomAnimation(getActivity(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, e.getMessage());
                    notifyFailedAudioCall();
                });

    }
    private void notifyFailedAudioCall() {
        MyCustomAlertDialog dialog = MyCustomAlertDialog.create(requireActivity());
        dialog.setTitle("Thông báo")
                .setMessage("Gọi thoại cho khách hàng xảy ra lỗi. Hãy thử lại xem sao.")
                .setPositiveButton("Thử lại", v -> {
                    dialog.dismiss();
                })
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Log.d(TAG, selectedCalendar.getTime() + "");
            onTaskFilterByDate(selectedCalendar.getTime());
        }
    }
}