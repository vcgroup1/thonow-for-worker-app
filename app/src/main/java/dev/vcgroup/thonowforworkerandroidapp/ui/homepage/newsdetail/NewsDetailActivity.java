package dev.vcgroup.thonowforworkerandroidapp.ui.homepage.newsdetail;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.firestore.FirebaseFirestore;

import org.parceler.Parcels;

import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.News;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivityNewsBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.homepage.adapter.NewsAdapter;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;

public class NewsDetailActivity extends AppCompatActivity implements NewsAdapter.OnNewsListener {
    private static final String TAG = NewsDetailActivity.class.getSimpleName();
    private ActivityNewsBinding binding;
    private News news;
    private NewsAdapter newsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_news);
        setupToolbar();
        if(getIntent().getExtras() != null) {
            news = Parcels.unwrap(getIntent().getParcelableExtra("news"));
            if(news != null) {
                binding.setItem(news);
                setupRelatedNewsSection();
            }
        }

        binding.newsDetailScrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY > 250) {
                    CommonUtil.animateFade(binding.newsDetailBottomSection, View.VISIBLE);
                } else {
                    CommonUtil.animateFade(binding.newsDetailBottomSection, View.GONE);
                }
            }
        });
    }

    private void setupToolbar() {
        setSupportActionBar(binding.mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
                finish();
                onBackPressed();
            return true;
        }
        return false;
    }

    private void setupRelatedNewsSection() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(CollectionConst.COLLECTION_NEWS)
                .whereNotEqualTo("id", news.getId())
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    newsAdapter = new NewsAdapter(this, NewsAdapter.NewsAdapterType.RELATED_NEWS_ITEM);
                    binding.rvRelatedNews.setAdapter(newsAdapter);
                    binding.rvRelatedNews.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
                    binding.rvRelatedNews.setItemAnimator(new DefaultItemAnimator());
                    newsAdapter.update(queryDocumentSnapshots.toObjects(News.class));
                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, e.getMessage());
                });
    }

    @Override
    public void onNewsClickedListener(int position) {
        Intent intent = new Intent(this, NewsDetailActivity.class);
        intent.putExtra("news", Parcels.wrap(newsAdapter.getItem(position)));
        startActivity(intent, ActivityOptions.makeCustomAnimation(this, android.R.anim.slide_in_left, android.R.anim.slide_out_right).toBundle());
        finish();
    }
}