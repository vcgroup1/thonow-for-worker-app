package dev.vcgroup.thonowforworkerandroidapp.ui.message.chatwindow.messageviewholder;

import android.view.View;

public interface OnMessageClickListener {
    void onMessageItemLongClickListener(View view, int position);

    void onTrySendMessageAgain(int position);

    void onMessageItemClickListener(int position);
}
