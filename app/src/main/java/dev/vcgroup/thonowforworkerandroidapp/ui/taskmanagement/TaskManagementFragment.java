package dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.tabs.TabLayoutMediator;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Order;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentTaskManagementBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.adapter.TaskPagerAdapter;

public class TaskManagementFragment extends Fragment {
    private static final String TAG = TaskManagementFragment.class.getSimpleName();
    private FragmentTaskManagementBinding binding;
    private TaskManagementViewModel mViewModel;
    private FirebaseFirestore db;

    public static TaskManagementFragment newInstance() {
        return new TaskManagementFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_task_management, container, false);
        binding.setLifecycleOwner(this);
        mViewModel = ViewModelProviders.of(requireActivity()).get(TaskManagementViewModel.class);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        populateData();
        setupTabLayout();
    }

    private void populateData() {
        // Tìm tất cả order có customerId = currentUserId
        if (db == null) {
            this.db = FirebaseFirestore.getInstance();
        }
        DocumentReference workerDocRef = db.collection(CollectionConst.COLLECTION_WORKER).document(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        FirebaseFirestore.getInstance()
                .collection(CollectionConst.COLLECTION_ORDER)
                .whereEqualTo("worker", workerDocRef)
                .addSnapshotListener((value, error) -> {
                    if (error != null) {
                        Log.d(TAG, error.getMessage());
                        return;
                    }

                    if (value != null) {
                        mViewModel.setOrders(value.toObjects(Order.class));
                    }
                });
    }

    private void setupTabLayout() {
        TaskPagerAdapter adapter = new TaskPagerAdapter(getChildFragmentManager(), this.getLifecycle());
        binding.taskManagementViewpager.setAdapter(adapter);
        new TabLayoutMediator(binding.taskManagementTablayout, binding.taskManagementViewpager, (tab, position) -> {
            switch (position) {
                case 0:
                    tab.setText("Lịch");
                    tab.setIcon(R.drawable.ic_calendar_day);
                    break;
                case 1:
                    tab.setText("Danh sách");
                    tab.setIcon(R.drawable.ic_list_ul);
                    break;
            }
        }).attach();
    }

}