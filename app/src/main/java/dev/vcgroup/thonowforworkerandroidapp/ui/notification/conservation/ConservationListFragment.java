package dev.vcgroup.thonowforworkerandroidapp.ui.notification.conservation;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.chat.Message;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentConservationListBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.message.chatwindow.ChatActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.notification.adapter.ConservationListAdapter;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.VCLoadingDialog;

public class ConservationListFragment extends Fragment implements ConservationListAdapter.OnConservationItemListener {
    private static final String TAG = ConservationListFragment.class.getSimpleName();
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }
    private ConservationListAdapter adapter;
    private FragmentConservationListBinding binding;
    private VCLoadingDialog loading;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_conservation_list,
                container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (loading == null) {
            loading = VCLoadingDialog.create(requireActivity());
        }
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        adapter = new ConservationListAdapter(new ArrayList<>(), this);
        binding.rvConservation.setAdapter(adapter);
        populateData();
    }

    private void populateData() {
        loading.show();
        db.collection(CollectionConst.COLLECTION_ORDER)
                .whereEqualTo("worker", CommonUtil.getCurrentUserRef(db))
                .addSnapshotListener((value, error) -> {
                    if (error != null) {
                        Log.w(TAG, "Listen failed!");
                        return;
                    }

                    List<DocumentReference> docRefs = new ArrayList<>();
                    if (value != null) {
                        value.forEach(queryDocumentSnapshot -> docRefs.add(queryDocumentSnapshot.getReference()));
                    }

                    if (!docRefs.isEmpty()) {
                        db.collection(CollectionConst.COLLECTION_LAST_MESSAGE)
                                .whereIn("order", docRefs)
                                .addSnapshotListener((value1, error1) -> {
                                    loading.dismiss();
                                    if (error1 != null) {
                                        Log.w(TAG, "Listen failed!");
                                        return;
                                    }

                                    List<Message> conservations = new ArrayList<>();
                                    if (value1 != null) {
                                        value1.getDocuments().forEach(documentSnapshot -> {
                                            Message conservation =
                                                    documentSnapshot.toObject(Message.class);
                                            if (conservation != null) {
                                                conservation.setId(documentSnapshot.getId());
                                            }
                                            conservations.add(conservation);
                                        });

                                        if (!conservations.isEmpty()) {
                                            adapter.update(conservations);
                                            binding.rvConservation.setVisibility(View.VISIBLE);
                                            binding.emptySection.setVisibility(View.INVISIBLE);
                                        } else {
                                            binding.rvConservation.setVisibility(View.INVISIBLE);
                                            binding.emptySection.setVisibility(View.VISIBLE);
                                        }
                                    }

                                });
                    } else {
                        loading.dismiss();
                        binding.rvConservation.setVisibility(View.INVISIBLE);
                        binding.emptySection.setVisibility(View.VISIBLE);
                    }

                });
    }

    @Override
    public void onConservationItemClick(int position) {
        String orderId = adapter.getItem(position).getId();
        Intent intent = new Intent(requireActivity(), ChatActivity.class);
        intent.putExtra("orderId", orderId);
        startActivity(intent, ActivityOptions.makeCustomAnimation(getActivity(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
    }

    @Override
    public void onConservationItemLongClick(int position) {

    }
}