package dev.vcgroup.thonowforworkerandroidapp.ui.audiocall;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

import dev.vcgroup.thonowforworkerandroidapp.data.remote.sinchservice.SinchService;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;

public abstract class BaseSinchActivity extends AppCompatActivity implements ServiceConnection {
    private static final String TAG = BaseSinchActivity.class.getSimpleName();
    private SinchService.SinchServiceInterface mSinchServiceInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.bindService(new Intent(this, SinchService.class), this,
                BIND_AUTO_CREATE);
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (SinchService.class.getName().equals(componentName.getClassName())) {
            Log.d(TAG, "service connected!");
            mSinchServiceInterface = (SinchService.SinchServiceInterface) iBinder;
            onServiceConnected();
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        if (SinchService.class.getName().equals(componentName.getClassName())) {
            Log.d(TAG, "service disconnected!");
            mSinchServiceInterface = null;
            onServiceDisconnected();
        }
    }

    protected void onServiceConnected() {
        if (!getSinchServiceInterface().isStarted()) {
            String currentUserId = CommonUtil.currentUserUid;
            if (currentUserId == null) {
                currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
            }
            getSinchServiceInterface().startClient(currentUserId);
        }
    }

    protected void onServiceDisconnected() {
        // for subclasses
    }

    protected SinchService.SinchServiceInterface getSinchServiceInterface() {
        return mSinchServiceInterface;
    }
}
