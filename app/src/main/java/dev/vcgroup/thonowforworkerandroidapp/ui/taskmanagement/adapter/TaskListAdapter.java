package dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.data.model.Order;
import dev.vcgroup.thonowforworkerandroidapp.data.model.OrderStatus;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Service;
import dev.vcgroup.thonowforworkerandroidapp.data.model.ServiceType;
import dev.vcgroup.thonowforworkerandroidapp.data.model.User;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Worker;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ItemByCalendarTaskBinding;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ItemTaskBinding;

public class TaskListAdapter extends RecyclerView.Adapter<TaskListAdapter.TaskViewHolder> implements Filterable {
    public enum TaskAdapterType {
        TASK_BY_LIST_ITEM,
        TASK_BY_CALENDAR_ITEM
    }

    private static final String TAG = TaskListAdapter.class.getSimpleName();
    private List<Order> orders;
    private TaskAdapterType type;
    private OnTaskListener onTaskListener;
    private ViewDataBinding binding;
    private AsyncListDiffer<Order> mDiffer;
    private DiffUtil.ItemCallback<Order> itemCallback = new DiffUtil.ItemCallback<Order>() {
        @Override
        public boolean areItemsTheSame(@NonNull @NotNull Order oldItem, @NonNull @NotNull Order newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull @NotNull Order oldItem, @NonNull @NotNull Order newItem) {
            return oldItem.getId().equals(newItem.getId());
        }
    };

    public TaskListAdapter(List<Order> orders, TaskAdapterType taskAdapterType, OnTaskListener onTaskListener) {
        this.orders = orders;
        this.type = taskAdapterType;
        this.onTaskListener = onTaskListener;
        this.mDiffer = new AsyncListDiffer<>(this, itemCallback);
    }

    @NonNull
    @NotNull
    @Override
    public TaskListAdapter.TaskViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (type.equals(TaskAdapterType.TASK_BY_LIST_ITEM)) {
            this.binding = ItemTaskBinding.inflate(inflater, parent, false);
        } else {
            this.binding = ItemByCalendarTaskBinding.inflate(inflater, parent, false);
        }
        return new TaskViewHolder(binding, onTaskListener);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull TaskListAdapter.TaskViewHolder holder, int position) {
        Order order = getItem(position);

        holder.bindOrder(binding, order);
        order.getCustomer()
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    User customer = documentSnapshot.toObject(User.class);
                    holder.bindCustomer(binding, customer);
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
        order.getOrderDetails().get(0).getService().get()
                .addOnSuccessListener(documentSnapshot -> {
                    Service service = documentSnapshot.toObject(Service.class);
                    Objects.requireNonNull(service).getServiceType().get()
                            .addOnSuccessListener(documentSnapshot1 -> {
                                ServiceType serviceType = documentSnapshot1.toObject(ServiceType.class);
                                holder.bindServiceType(binding, serviceType);
                            })
                            .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public Order getItem(int pos) {
        return mDiffer.getCurrentList().get(pos);
    }

    public void update(List<Order> newList) {
        mDiffer.submitList(newList);
        if (orders.isEmpty()) {
            orders = newList;
            Log.d(TAG, orders.toString());
        }
    }

    public List<Order> getItems() {
        return mDiffer.getCurrentList();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults filterResults = new FilterResults();
                if (charSequence != null && charSequence.length() == 0) {
                    filterResults.count = orders.size();
                    filterResults.values = orders;
                } else {
                    String keyword = charSequence.toString();
                    OrderStatus orderStatus = OrderStatus.getOrderStatusByTitle(keyword);
                    List<Order> resultList = new ArrayList<>();
                    orders.forEach(order -> {
                        if (order.getStatus().equals(orderStatus)) {
                            resultList.add(order);
                        }
                    });
                    filterResults.count = resultList.size();
                    filterResults.values = resultList;
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                update((List<Order>) results.values);
            }
        };
    }

    public int indexOf(Order order) {
        return mDiffer.getCurrentList().indexOf(order);
    }

    public static class TaskViewHolder extends RecyclerView.ViewHolder {
        private ItemByCalendarTaskBinding binding;
        private ItemTaskBinding itemTaskBinding;
        private OnTaskListener onTaskListener;
        private View.OnClickListener onTaskClickedListener = v -> {
            if (this.onTaskListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    this.onTaskListener.onTaskClickedListener(position);
                }
            }
        };

        public TaskViewHolder(ViewDataBinding binding, OnTaskListener onTaskListener) {
            super(binding.getRoot());
            this.onTaskListener = onTaskListener;
            if (binding instanceof ItemTaskBinding) {
                this.itemTaskBinding = (ItemTaskBinding) binding;
            } else {
                this.binding = (ItemByCalendarTaskBinding) binding;

                this.binding.btnCancelOrder.setOnClickListener(v -> {
                    if (this.onTaskListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            this.onTaskListener.onTaskCancelClickedListener(position);
                        }
                    }
                });

                this.binding.btnDoActionForNextStatus.setOnClickListener(v -> {
                    if (this.onTaskListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            this.onTaskListener.onTaskNextStatusClickedListener(position);
                        }
                    }
                });

                this.binding.ibnChat.setOnClickListener(v -> {
                    if (this.onTaskListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            this.onTaskListener.onChatClickedListener(position);
                        }
                    }
                });

                this.binding.ibnCall.setOnClickListener(v -> {
                    if (this.onTaskListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            this.onTaskListener.onAudioCallClickedListener(position);
                        }
                    }
                });
            }
            binding.getRoot().setOnClickListener(onTaskClickedListener);
        }

        public void bindOrder(ViewDataBinding binding, Order order) {
            if (binding instanceof ItemByCalendarTaskBinding) {
                this.binding.setOrder(order);
                this.binding.executePendingBindings();

                switch (order.getStatus()) {
                    case WAITING_FOR_WORK:
                        this.binding.btnDoActionForNextStatus.setText("Di chuyển");
                        this.binding.cardview.setClickable(false);
                        this.binding.executePendingBindings();
                        break;
                    case ON_THE_MOVE:
                        this.binding.btnDoActionForNextStatus.setText("Đã đến");
                        this.binding.cardview.setClickable(false);
                        this.binding.executePendingBindings();
                        break;
                    case ARRIVED:
                        this.binding.btnDoActionForNextStatus.setText("Khảo sát");
                        this.binding.cardview.setClickable(true);
                        this.binding.executePendingBindings();
                        break;
                    case INVESTIGATING:
                        this.binding.btnDoActionForNextStatus.setText("Cập nhật khảo sát");
                        this.binding.cardview.setClickable(true);
                        this.binding.executePendingBindings();
                        break;
                    case WAIT_FOR_ORDER_CONFIRMATION:
                        this.binding.btnDoActionForNextStatus.setVisibility(View.GONE);
                        this.binding.cardview.setClickable(true);
                        this.binding.executePendingBindings();
                        break;
                    case CONFIRMED_ORDER:
                        this.binding.btnDoActionForNextStatus.setText("Bắt đầu làm");
                        this.binding.executePendingBindings();
                        break;
                    case WORKING:
                        this.binding.btnDoActionForNextStatus.setText("Hoàn thành");
                        this.binding.btnCancelOrder.setVisibility(View.GONE);
                        this.binding.executePendingBindings();
                        break;
                    case WAIT_FOR_CONFIRMATION_TO_COMPLETE:
                    case COMPLETED:
                        this.binding.btnDoActionForNextStatus.setVisibility(View.GONE);
                        this.binding.btnCancelOrder.setVisibility(View.GONE);
                        this.binding.cardview.setClickable(true);
                        this.binding.executePendingBindings();
                        break;
                    case CANCELED:
                        this.binding.btnDoActionForNextStatus.setVisibility(View.GONE);
                        this.binding.btnCancelOrder.setVisibility(View.GONE);
                        this.binding.cardview.setClickable(false);
                        this.binding.executePendingBindings();
                        break;
                    case WAIT_FOR_CONFIRMATION_TO_PAY:
                        this.binding.btnDoActionForNextStatus.setVisibility(View.VISIBLE);
                        this.binding.btnDoActionForNextStatus.setText("Xác nhận thanh toán");
                        this.binding.btnCancelOrder.setVisibility(View.GONE);
                        this.binding.executePendingBindings();
                        break;
                }

            } else {
                this.itemTaskBinding.setOrder(order);
                this.itemTaskBinding.executePendingBindings();

                switch (order.getStatus()) {
                    case WAITING_FOR_WORK:
                    case ON_THE_MOVE:
                    case ARRIVED:
                        this.itemTaskBinding.cardview.setClickable(false);
                        this.itemTaskBinding.executePendingBindings();
                        break;
                    case INVESTIGATING:
                    case WAIT_FOR_ORDER_CONFIRMATION:
                    case CONFIRMED_ORDER:
                    case WORKING:
                    case WAIT_FOR_CONFIRMATION_TO_COMPLETE:
                    case WAIT_FOR_CONFIRMATION_TO_PAY:
                    case COMPLETED:
                    case CANCELED:
                        this.itemTaskBinding.cardview.setClickable(true);
                        this.itemTaskBinding.executePendingBindings();
                        break;
                }

               
            }
        }


        public void bindCustomer(ViewDataBinding binding, User customer) {
            if (binding instanceof ItemByCalendarTaskBinding) {
                this.binding.setUser(customer);
                this.binding.executePendingBindings();
            } else {
                this.itemTaskBinding.setUser(customer);
                this.itemTaskBinding.executePendingBindings();
            }
        }

        public void bindServiceType(ViewDataBinding binding, ServiceType serviceType) {
            if (binding instanceof ItemByCalendarTaskBinding) {
                this.binding.setServiceType(serviceType);
                this.binding.executePendingBindings();
            } else {
                this.itemTaskBinding.setServiceType(serviceType);
                this.itemTaskBinding.executePendingBindings();
            }
        }
    }

    public interface OnTaskListener {
        void onTaskClickedListener(int position);
        void onTaskNextStatusClickedListener(int position);
        void onTaskCancelClickedListener(int position);
        void onChatClickedListener(int position);
        void onAudioCallClickedListener(int position);
    }
}
