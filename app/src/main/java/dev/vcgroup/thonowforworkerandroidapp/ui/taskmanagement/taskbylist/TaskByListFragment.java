package dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.taskbylist;

import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.parceler.Parcels;

import java.util.ArrayList;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Order;
import dev.vcgroup.thonowforworkerandroidapp.databinding.BsdTaskFilterBinding;
import dev.vcgroup.thonowforworkerandroidapp.databinding.TaskByListFragmentBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.TaskManagementViewModel;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.adapter.TaskListAdapter;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.cancelorder.CancelOrderActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.taskdetail.TaskDetailActivity;
import dev.vcgroup.thonowforworkerandroidapp.util.SpacesItemDecoration;

public class TaskByListFragment extends Fragment implements TaskListAdapter.OnTaskListener {
    private static final String TAG = TaskByListFragment.class.getSimpleName();
    private TaskByListFragmentBinding binding;
    private TaskListAdapter adapter;
    private BottomSheetDialog bsdTaskFilter;
    private TaskManagementViewModel mViewModel;

    public static TaskByListFragment newInstance() {
        return new TaskByListFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.task_by_list_fragment, container, false);
        mViewModel = ViewModelProviders.of(requireActivity()).get(TaskManagementViewModel.class);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setupRvTaskList();

        binding.btnTaskFilter.setOnClickListener(v -> {
            BsdTaskFilterBinding taskFilterBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.bsd_task_filter, (ViewGroup) v.getRootView(), false);
            bsdTaskFilter = new BottomSheetDialog(requireActivity());
            bsdTaskFilter.setContentView(taskFilterBinding.getRoot());

            bsdTaskFilter.show();

            taskFilterBinding.lvOrderStatus.setOnItemClickListener((parent, view, position, id) -> {
                String query = (String) taskFilterBinding.lvOrderStatus.getItemAtPosition(position);
                binding.tvTaskFilter.setText(query);
                adapter.getFilter().filter(query, count -> {
                    if (count != 0) {
                        binding.rvTaskList.setVisibility(View.VISIBLE);
                        binding.noTaskSchedule.getRoot().setVisibility(View.GONE);
                    } else {
                        binding.rvTaskList.setVisibility(View.GONE);
                        binding.noTaskSchedule.getRoot().setVisibility(View.VISIBLE);
                    }
                });
                bsdTaskFilter.dismiss();
            });


        });
    }

    private void setupRvTaskList() {
        adapter = new TaskListAdapter(new ArrayList<>(), TaskListAdapter.TaskAdapterType.TASK_BY_LIST_ITEM, this);
        binding.rvTaskList.setAdapter(adapter);
        binding.rvTaskList.addItemDecoration(new SpacesItemDecoration(20));

        mViewModel.getOrders().observe(requireActivity(), orders -> {
            adapter.update(orders);
        });
    }

    @Override
    public void onTaskClickedListener(int position) {
        Order order = adapter.getItem(position);
        Intent intent = new Intent(requireActivity(), TaskDetailActivity.class);
        intent.putExtra("order", Parcels.wrap(order));
        startActivity(intent);
    }

    @Override
    public void onTaskNextStatusClickedListener(int position) {

    }

    @Override
    public void onTaskCancelClickedListener(int position) {
        String orderId = adapter.getItem(position).getId();
        Intent intent = new Intent(requireActivity(), CancelOrderActivity.class);
        intent.putExtra("deleteOrderId", Parcels.wrap(orderId));
        startActivity(intent);
    }

    @Override
    public void onChatClickedListener(int position) {

    }

    @Override
    public void onAudioCallClickedListener(int position) {

    }
}