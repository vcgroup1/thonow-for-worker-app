package dev.vcgroup.thonowforworkerandroidapp.ui.profile.mywalletmanagement.chargemoney;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentThankYourForChargeMoneyBinding;

public class ThankYourForChargeMoneyFragment extends Fragment {
	private FragmentThankYourForChargeMoneyBinding binding;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		binding = DataBindingUtil.inflate(inflater, R.layout.fragment_thank_your_for_charge_money
				, container, false);
		return binding.getRoot();
	}

	@Override
	public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		Bundle bundle = this.getArguments();
		if (bundle != null) {
			long amount = bundle.getLong("amount", 0);
			double balanceMoney = bundle.getDouble("balanceMoney", 0);
			String transactionId = bundle.getString("transactionId");

			binding.setAmount(amount);
			binding.setBalanceMoney(balanceMoney);
			binding.setTransactionId(transactionId);
		}

		binding.btnBackToMainScreen.setOnClickListener(v -> {
			requireActivity().setResult(Activity.RESULT_OK);
			requireActivity().finish();
		});
	}
}