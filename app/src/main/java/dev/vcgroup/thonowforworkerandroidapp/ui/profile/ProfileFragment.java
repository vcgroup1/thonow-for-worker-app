package dev.vcgroup.thonowforworkerandroidapp.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.firebase.auth.FirebaseAuth;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcels;

import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.data.model.User;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Worker;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentProfileBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.MainScreenActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.MainScreenViewModel;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.bankingaccountmanagement.BankingAccountManagementActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.editprofile.EditProfileActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.jobhistory.JobHistoryActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.mywalletmanagement.MyWalletManagementActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.ProfileManagementActivity;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;

public class ProfileFragment extends Fragment {
    private MainScreenViewModel mViewModel;
    private FragmentProfileBinding binding;
    private Toolbar mToolbar;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        mViewModel = new ViewModelProvider(this).get(MainScreenViewModel.class);
        binding.setItem(mViewModel);
        binding.setLifecycleOwner(getActivity());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewModel.setCurrentUser(Worker.convertFrom(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser())));
        setHasOptionsMenu(true);
        setupToolbar();

        binding.jobHistorySection.setOnClickListener(v -> {
            startActivity(new Intent(requireActivity(), JobHistoryActivity.class));
        });

        binding.walletSection.setOnClickListener(v -> {
            Intent intent = new Intent(requireActivity(), MyWalletManagementActivity.class);
            startActivity(intent);
        });

        binding.moneyAccountSection.setOnClickListener(v -> {
            Intent intent = new Intent(requireActivity(), BankingAccountManagementActivity.class);
            intent.putExtra("currentUser", Parcels.wrap(mViewModel.getCurrentUser().getValue()));
            startActivity(intent);
        });

        binding.profileManagementSection.setOnClickListener(v -> {
            Intent intent = new Intent(requireActivity(), ProfileManagementActivity.class);
            intent.putExtra("currentUser", Parcels.wrap(mViewModel.getCurrentUser().getValue()));
            startActivity(intent);
        });

        binding.goToEditProfile.setOnClickListener(v -> navigateToEditProfile());
    }

    private void setupToolbar() {
        mToolbar = binding.topAppBar.mToolbar;
        mToolbar.setNavigationIcon(null);
        ((MainScreenActivity) requireActivity()).setSupportActionBar(mToolbar);
        Objects.requireNonNull(((MainScreenActivity) requireActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(false);
        Objects.requireNonNull(((MainScreenActivity) requireActivity()).getSupportActionBar()).setTitle(null);
        binding.topAppBar.tvTitleToolbar.setText("Hồ sơ của bạn");
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.profile_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.menu_profile);
        menuItem.setVisible(false);
        binding.fragmentProfileScrollview.setOnScrollChangeListener((View.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            int actionBarHeight = CommonUtil.getActionBarHeight(getActivity());
            menuItem.setVisible(scrollY >= actionBarHeight);
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_profile) {
            navigateToEditProfile();
            return true;
        }
        return false;
    }

    private void navigateToEditProfile() {
        Intent intent = new Intent(getActivity(), EditProfileActivity.class);
        intent.putExtra("currentUser", Parcels.wrap(mViewModel.getCurrentUser().getValue()));
        startActivity(intent);
    }
}