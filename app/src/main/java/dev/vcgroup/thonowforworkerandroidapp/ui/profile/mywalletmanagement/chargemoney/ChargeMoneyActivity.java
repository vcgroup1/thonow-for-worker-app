package dev.vcgroup.thonowforworkerandroidapp.ui.profile.mywalletmanagement.chargemoney;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivityChargeMoneyBinding;
import dev.vcgroup.thonowforworkerandroidapp.util.FragmentUtil;

public class ChargeMoneyActivity extends AppCompatActivity {
	private static final String TAG = ChargeMoneyActivity.class.getSimpleName();
	private ActivityChargeMoneyBinding binding;
	private ChargeMoneyViewModel mViewModel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = DataBindingUtil.setContentView(this, R.layout.activity_charge_money);
		mViewModel = ViewModelProviders.of(this).get(ChargeMoneyViewModel.class);
		FragmentUtil.replaceFragment(this, R.id.charge_money_fragment_container, new ChargeMoneyStepOneFragment(),
				null, ChargeMoneyStepOneFragment.class.getSimpleName(), false);

//		FirebaseDynamicLinks.getInstance()
//				.getDynamicLink(getIntent())
//				.addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
//					@Override
//					public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
//						// Get deep link from result (may be null if no link is found)
//						if (pendingDynamicLinkData != null) {
//							Uri deepLink = pendingDynamicLinkData.getLink();
//							Intent intent = getIntent();
//							Uri data = intent.getData();
//
//							Log.d(TAG, deepLink.toString());
//							Log.d(TAG, "data" + data.toString());
//
//							if (data != null) {
//								String dataParams = data.getQueryParameter("data");
//								// amount=%s;transactionId=%s
//
//								if (dataParams != null && !dataParams.isEmpty()) {
//									data = Uri.parse(dataParams.replace(";","&"));
//
//									Bundle bundle = new Bundle();
//									bundle.putLong("amount",
//											Long.parseLong(data.getQueryParameter("amount"))/100);
//									Log.d("amount", Long.parseLong(data.getQueryParameter("amount"))/100 + "");
//
//									//bundle.putDouble("balanceMoney", balanceMoney);
//									bundle.putString("transactionId", data.getQueryParameter("transactionId"));
//
//									Log.d("amount", data.getQueryParameter("transactionId"));
//
//									FragmentUtil.replaceFragment(ChargeMoneyActivity.this,
//											R.id.charge_money_fragment_container, new ThankYourForChargeMoneyFragment(),
//											bundle, ThankYourForChargeMoneyFragment.class.getSimpleName(),
//											true);
//								}
//							}
//						}
//					}
//				})
//				.addOnFailureListener(this, new OnFailureListener() {
//					@Override
//					public void onFailure(@NonNull Exception e) {
//						Log.w(TAG, "getDynamicLink:onFailure", e);
//					}
//				});
	}

	Fragment currentFragment;

	private void setupToolbar() {
		Toolbar toolbar = binding.topAppBar.mToolbar;
		setSupportActionBar(toolbar);
		Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
		setTitle(null);
		binding.topAppBar.tvTitleToolbar.setText("Nạp tiền vào tài khoản");
		getSupportActionBar().show();

		getSupportFragmentManager().addOnBackStackChangedListener(() -> {
			currentFragment = FragmentUtil.getCurrentFragment(ChargeMoneyActivity.this, R.id.charge_money_fragment_container);
			if (currentFragment instanceof ChargeMoneyStepOneFragment) {
				binding.topAppBar.tvTitleToolbar.setText("Nạp tiền vào tài khoản");
			} else if (currentFragment instanceof ChooseTransactionMethodFragment) {
				binding.topAppBar.tvTitleToolbar.setText("Chọn phương thức giao dịch");
			} else if (currentFragment instanceof ThankYourForChargeMoneyFragment) {
				binding.topAppBar.tvTitleToolbar.setText("Nạp tiền thành công");
			}

		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		setupToolbar();
	}

	@Override
	protected void onStop() {
		super.onStop();
		Objects.requireNonNull(getSupportActionBar()).hide();
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			if (currentFragment != null)
				FragmentUtil.removeFragmentByTag(this, currentFragment.getTag());
			onBackPressed();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		for (Fragment fragment : getSupportFragmentManager().getFragments()) {
			fragment.onActivityResult(requestCode, resultCode, data);
		}
	}
}