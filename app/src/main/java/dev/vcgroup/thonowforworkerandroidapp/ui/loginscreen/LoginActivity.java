package dev.vcgroup.thonowforworkerandroidapp.ui.loginscreen;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.gun0912.tedpermission.TedPermission;


import java.util.List;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.constant.CommonConst;
import dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.AuthMethod;
import dev.vcgroup.thonowforworkerandroidapp.data.model.CountryInfo;
import dev.vcgroup.thonowforworkerandroidapp.data.model.User;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivityLoginScreenBinding;
import dev.vcgroup.thonowforworkerandroidapp.databinding.BsdChooseCountryCodeBinding;
import dev.vcgroup.thonowforworkerandroidapp.databinding.DialogAskPermissionBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.MainScreenActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.askpermission.AskPermissionActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.forgotpasswordscreen.ForgotPasswordActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.loginscreen.adapter.CountryCodeAdapter;
import dev.vcgroup.thonowforworkerandroidapp.ui.signinscreen.SignInActivity;
import dev.vcgroup.thonowforworkerandroidapp.util.AuthenticationUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.MyCustomAlertDialog;
import lombok.SneakyThrows;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, CountryCodeAdapter.OnCountryCodeListener {
    private static final String TAG = LoginActivity.class.getSimpleName();
    private ActivityLoginScreenBinding binding;
    private static FirebaseAuth mAuth;
    private static FirebaseFirestore db;
    static {
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
    }

    private BottomSheetDialog changeLanguageBsd;
    private RadioButton rbVietnamese, rbEnglish;
    private BottomSheetDialog changeCountryBsd;
    private CountryCodeAdapter countryCodeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login_screen);
        askForPermission();

        binding.ivbtnLanguage.setOnClickListener(v -> showChangeLanguage());

        binding.setCountryInfo(new CountryInfo("Vietnam", "VN", "84"));

        binding.btnLogin.setOnClickListener(v -> {
            String phone = CommonUtil.getInputText(binding.tipPhone);
            String password = CommonUtil.getInputText(binding.tipPassword);

            if (phone.isEmpty() || password.isEmpty()) {
                MyCustomAlertDialog dialog = MyCustomAlertDialog.create(this);
                dialog.setTitle("Đăng nhập thất bại!")
                        .setMessage("Vui lòng nhập đầy đủ thông tin.")
                        .setPositiveButton("Đã hiểu", v1 -> {
                            dialog.dismiss();
                        })
                        .show();
                return;
            }

            if (!AuthenticationUtil.isValidNumber(phone, binding.getCountryInfo().getAlp2Code())) {
                MyCustomAlertDialog dialog = MyCustomAlertDialog.create(this);
                dialog.setTitle("Đăng nhập thất bại!")
                        .setMessage("Vui lòng nhập số điện thoại hợp lệ.")
                        .setPositiveButton("Đã hiểu", v1 -> {
                            dialog.dismiss();
                        })
                        .show();
                return;
            }

            doLogin(AuthenticationUtil.generateEmailFromPhone(phone, binding.getCountryInfo().getAlp2Code()), password);
        });

        binding.dropDownCountry.setOnClickListener(v -> {
            BsdChooseCountryCodeBinding chooseCountryCodeBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.bsd_choose_country_code, (ViewGroup) v.getRootView(), false);
            changeCountryBsd = new BottomSheetDialog(this);
            changeCountryBsd.setContentView(chooseCountryCodeBinding.getRoot());

            countryCodeAdapter = new CountryCodeAdapter(db, this);
            chooseCountryCodeBinding.rvCountries.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
            chooseCountryCodeBinding.rvCountries.setItemAnimator(new DefaultItemAnimator());
            chooseCountryCodeBinding.rvCountries.setAdapter(countryCodeAdapter);
            changeCountryBsd.show();

            chooseCountryCodeBinding.svCountryCode.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    countryCodeAdapter.getFilter().filter(query);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    countryCodeAdapter.getFilter().filter(newText);
                    return true;
                }
            });
        });

        binding.btnSignIn.setOnClickListener(v -> {
            startActivity(new Intent(this, SignInActivity.class), ActivityOptions.makeCustomAnimation(this, android.R.anim.slide_in_left, android.R.anim.slide_out_right).toBundle());
        });

        binding.btnForgotPwd.setOnClickListener(v -> {
            startActivity(new Intent(this, ForgotPasswordActivity.class), ActivityOptions.makeCustomAnimation(this, android.R.anim.slide_in_left, android.R.anim.slide_out_right).toBundle());
        });
    }

    private void doLogin(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                   if (task.isSuccessful()) {
                       navigateToMainScreen();
                   } else {
                       MyCustomAlertDialog dialog = MyCustomAlertDialog.create(this);
                       dialog.setTitle("Đăng nhập thất bại!")
                               .setMessage("Tài khoản không tồn tại hoặc đã bị khóa!")
                               .setPositiveButton("Đã hiểu", v1 -> {
                                   dialog.dismiss();
                               })
                               .show();
                   }
                });
    }

    private void askForPermission() {
        // show ask permission dialog
        if (!CommonUtil.hasPermissions(this, CommonConst.REQUIRED_PERMISSION)) {
            DialogAskPermissionBinding askPermissionBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_ask_permission, (ViewGroup) binding.getRoot(), false);
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this, R.style.MaterialAlertDialog_Rounded);
            builder.setView(askPermissionBinding.getRoot());
            AlertDialog dialog = builder.create();
            dialog.show();

            askPermissionBinding.btnAccept.setOnClickListener(v -> {
                Intent intent = new Intent(this, AskPermissionActivity.class);
                startActivityForResult(intent, RequestCodeConst.ASK_PERMISSION_CODE);
                dialog.cancel();
            });

            askPermissionBinding.btnIgnore.setOnClickListener(v -> dialog.cancel());
        }
    }

    public void showChangeLanguage() {
        View changeLanguageView = getLayoutInflater().inflate(R.layout.bsd_change_language, null);
        changeLanguageBsd = new BottomSheetDialog(this);
        changeLanguageBsd.setContentView(changeLanguageView);
        changeLanguageBsd.show();

        LinearLayout btnVietnamese = changeLanguageView.findViewById(R.id.btn_vietnamese);
        LinearLayout btnEnglish = changeLanguageView.findViewById(R.id.btn_english);

        rbVietnamese = changeLanguageView.findViewById(R.id.rd_btn_vietnamese);
        rbEnglish = changeLanguageView.findViewById(R.id.rd_btn_en);

        btnVietnamese.setOnClickListener(this);
        btnEnglish.setOnClickListener(this);
        rbVietnamese.setOnClickListener(this);
        rbEnglish.setOnClickListener(this);
    }

    private void navigateToMainScreen() {
        Intent intent = new Intent(this, MainScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent, ActivityOptions.makeCustomAnimation(this, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
        finish();
    }

    @SneakyThrows
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_vietnamese:
            case R.id.rd_btn_vietnamese:
                rbVietnamese.setChecked(true);
                rbEnglish.setChecked(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        changeLanguageBsd.dismiss();
                    }
                }, 1000);

                break;
            case R.id.btn_english:
            case R.id.rd_btn_en:
                rbVietnamese.setChecked(false);
                rbEnglish.setChecked(true);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        changeLanguageBsd.dismiss();
                    }
                }, 1000);
                break;
        }
    }

    @Override
    public void onCountryCodeClickedListener(int position) {
        binding.setCountryInfo(countryCodeAdapter.getItem(position));
        new Handler().postDelayed(() -> {
            changeCountryBsd.cancel();
        }, 1000);
    }
}