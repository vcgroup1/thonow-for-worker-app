package dev.vcgroup.thonowforworkerandroidapp.ui.signinscreen;

import android.text.Html;
import android.text.Spanned;

import androidx.databinding.ObservableField;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.i18n.phonenumbers.Phonenumber;

import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.data.model.CountryInfo;
import dev.vcgroup.thonowforworkerandroidapp.util.AuthenticationUtil;

public class SignInViewModel extends ViewModel {
    private MutableLiveData<String> phoneNumber = new MutableLiveData<>();
    public Spanned acceptTerms = Html.fromHtml("<strong>Bằng việc sử dụng ứng dụng. Tôi đồng ý " +
            "với <span style=\"text-decoration: underline; color: #0000ff;\">Điều khoản sử dụng</span> của ThoNOW</strong>");
    public final ObservableField<String> errPhoneNumber = new ObservableField<>();
    public MutableLiveData<CountryInfo> selectedCountryCode = new MutableLiveData<CountryInfo>();
    private MutableLiveData<Phonenumber.PhoneNumber> phoneNumberProto = new MutableLiveData<>();

    public void setPhoneNumber(String phoneNum) {
        phoneNumber.setValue(phoneNum);
        this.setPhoneNumberProto(AuthenticationUtil.parsePhoneNumber(phoneNum, Objects.requireNonNull(getSelectedCountryCode().getValue()).getAlp2Code()));
    }

    public void setSelectedCountryCode(CountryInfo countryInfo) {
        selectedCountryCode.setValue(countryInfo);
    }

    public MutableLiveData<CountryInfo> getSelectedCountryCode() {
        return selectedCountryCode;
    }

    public MutableLiveData<String> getPhoneNumber() {
        return phoneNumber;
    }

    public MutableLiveData<Phonenumber.PhoneNumber> getPhoneNumberProto() {
        return phoneNumberProto;
    }

    public void setPhoneNumberProto(Phonenumber.PhoneNumber phoneNumberProto) {
        this.phoneNumberProto.setValue(phoneNumberProto);
    }
}
