package dev.vcgroup.thonowforworkerandroidapp.ui.message.chatwindow;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.sj.emoji.EmojiBean;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.constant.VCKeyboardConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Order;
import dev.vcgroup.thonowforworkerandroidapp.data.model.User;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Worker;
import dev.vcgroup.thonowforworkerandroidapp.data.model.chat.Message;
import dev.vcgroup.thonowforworkerandroidapp.data.model.chat.MessageType;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivityChatBinding;
import dev.vcgroup.thonowforworkerandroidapp.databinding.DialogSampleMessageListBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.audiocall.callscreen.CallScreenActivity;
import dev.vcgroup.thonowforworkerandroidapp.ui.message.chatwindow.messageviewholder.OnMessageClickListener;
import dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.taskdetail.TaskDetailActivity;
import dev.vcgroup.thonowforworkerandroidapp.util.MyCustomAlertDialog;
import dev.vcgroup.thonowforworkerandroidapp.util.keyboard.VCEmoticonsKeyBoard;
import dev.vcgroup.thonowforworkerandroidapp.util.keyboard.VCKeyboardUtils;
import gun0912.tedbottompicker.TedBottomPicker;
import sj.keyboard.interfaces.EmoticonClickListener;
import sj.keyboard.utils.EmoticonsKeyboardUtils;
import sj.keyboard.widget.EmoticonsEditText;
import sj.keyboard.widget.FuncLayout;

import static dev.vcgroup.thonowforworkerandroidapp.constant.AudioCallConst.OUT_GOING_CALL;
import static dev.vcgroup.thonowforworkerandroidapp.constant.CommonConst.CALL_PERMISSIONS;

public class ChatActivity extends AppCompatActivity implements
        VCEmoticonsKeyBoard.OnChatKeyboardListener,
        OnMessageClickListener,
        FuncLayout.OnFuncKeyBoardListener {

    private static final String TAG = ChatActivity.class.getSimpleName();
    private ActivityChatBinding binding;
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }
    private DocumentReference orderDocRef, customerDocRef;
    private VCEmoticonsKeyBoard chatKeyBoard;
    private EmoticonClickListener emoticonClickListener = new EmoticonClickListener() {
        @Override
        public void onEmoticonClick(Object o, int actionType, boolean isDelBtn) {
            if (isDelBtn) {
                int action = KeyEvent.ACTION_DOWN;
                int code = KeyEvent.KEYCODE_DEL;
                KeyEvent event = new KeyEvent(action, code);
                chatKeyBoard.getEtChat().onKeyDown(KeyEvent.KEYCODE_DEL, event);
            } else {
                if (o == null) {
                    return;
                }

                if (actionType == VCKeyboardConst.EMOTICON_CLICK_BIGIMAGE) {
                    //onSendSticker(((EmoticonEntity) o).getIconUri());
                } else {
                    String content = null;
                    if (o instanceof EmojiBean) {
                        content = ((EmojiBean) o).emoji;
                    }

                    if (TextUtils.isEmpty(content)) {
                        return;
                    }

                    int index = chatKeyBoard.getEtChat().getSelectionStart();
                    Editable editable = chatKeyBoard.getEtChat().getText();
                    editable.insert(index, content);
                }
            }
        }
    };
    private MessageAdapter messageAdapter;
    private ListenerRegistration chatListenerRegistration;
    private ArrayList<Message> messages;
    private RecyclerView rvChatList;
    private User customer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat);

        setupToolbar();

        if (getIntent().getExtras() != null) {
            String orderId = getIntent().getStringExtra("orderId");
            if (!orderId.isEmpty()) {
                orderDocRef = db.collection(CollectionConst.COLLECTION_ORDER).document(orderId);
            }

            String customerId = getIntent().getStringExtra("customerId");
            if (customerId != null && !customerId.isEmpty()) {
                customerDocRef = db.collection(CollectionConst.COLLECTION_USER).document(customerId);
                getCustomer();
            } else {
                orderDocRef.get().addOnSuccessListener(documentSnapshot -> {
                    Order order = documentSnapshot.toObject(Order.class);
                    if(order != null) {
                        customerDocRef = order.getCustomer();
                        getCustomer();
                    }
                });
            }
        }

        setupKeyboard();
        setupChatList();
    }

    private void getCustomer() {
        customerDocRef.get()
                .addOnSuccessListener(documentSnapshot -> {
                    customer = documentSnapshot.toObject(User.class);
                    if (customer != null) {
                        binding.setCustomer(customer);
                    }
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
    }

    private void setupChatList() {
        messages = new ArrayList<>();
        rvChatList = binding.rvChatList;
        messageAdapter = new MessageAdapter(this, messages, this);
        rvChatList.setAdapter(messageAdapter);
        rvChatList.setHasFixedSize(true);
        rvChatList.setItemAnimator(null);
        rvChatList.setItemViewCacheSize(50);
        if (orderDocRef != null) {
            listenForChatMessages();
        }
    }

    private void listenForChatMessages() {
        chatListenerRegistration = db.collection(CollectionConst.COLLECTION_MESSAGE)
                .whereEqualTo("order", orderDocRef)
                .addSnapshotListener((value, error) -> {
                    if (error != null) {
                        Log.w(TAG, "Listen failed.", error);
                        return;
                    }

                    if (value != null) {
                        messages.clear();
                        for (DocumentSnapshot docSnap : value.getDocuments()) {
                            Message messageResponse = docSnap.toObject(Message.class);
                            if (messageResponse != null) {
                                messageResponse.setId(docSnap.getId());

                                switch (messageResponse.getType()) {
                                    case TEXT:
                                        messages.add(messageResponse);
                                        break;
                                    case IMAGE:
                                        if (!messageResponse.getFrom().equals(customerDocRef)) {
                                            messages.add(messageResponse);
                                        } else {
                                            if (messageResponse.getContent().contains(getResources().getString(R.string.google_storage_bucket))) {
                                                messages.add(messageResponse);
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                        messages.sort(Comparator.comparing(Message::getTimestamp));
                        messageAdapter.submitList(messages);
                        scrollToBottom();
                    }
                });
    }

    private void scrollToBottom() {
        rvChatList.requestLayout();
        rvChatList.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int count = rvChatList.getAdapter().getItemCount();
                if (count > 0) {
                    rvChatList.smoothScrollToPosition(count - 1);
                }
            }
        });
    }

    private void setupKeyboard() {
        chatKeyBoard = binding.chatKeyboard;
        VCKeyboardUtils.initEmotionsEditText(chatKeyBoard.getEtChat());
        chatKeyBoard.setAdapter(VCKeyboardUtils.getCommonAdapter(this, emoticonClickListener));
        chatKeyBoard.setOnChatKeyboardListener(this);
        chatKeyBoard.addOnFuncKeyBoardListener(this);

        chatKeyBoard.getEtChat().setOnSizeChangedListener((w, h, oldw, oldh) -> {

        });
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (EmoticonsKeyboardUtils.isFullScreen(this)) {
            boolean isConsum = chatKeyBoard.dispatchKeyEventInFullScreen(event);
            return isConsum ? isConsum : super.dispatchKeyEvent(event);
        }
        return super.dispatchKeyEvent(event);
    }

    private void setupToolbar() {
        Toolbar toolbar = binding.mToolbar;
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.menu_call) {
            if (!TedPermission.isGranted(this, CALL_PERMISSIONS)) {
                TedPermission.with(this)
                        .setPermissionListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted() {
                                callToCustomer(customer);
                            }

                            @Override
                            public void onPermissionDenied(List<String> deniedPermissions) {
                                Snackbar.make(binding.getRoot(), "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                            }
                        })
                        .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                        .setPermissions(CALL_PERMISSIONS)
                        .check();
            } else {
                callToCustomer(customer);
            }
            return true;
        }
        return false;
    }

    private void callToCustomer(User customer) {
        if (customer != null) {
            Intent intent = new Intent(this, CallScreenActivity.class);
            intent.putExtra("customer", Parcels.wrap(customer));
            intent.putExtra(OUT_GOING_CALL, true);
            startActivity(intent, ActivityOptions.makeCustomAnimation(ChatActivity.this, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
        } else {
            notifyFailedAudioCall();
        }
    }
    private void notifyFailedAudioCall() {
        MyCustomAlertDialog dialog = MyCustomAlertDialog.create(this);
        dialog.setTitle("Thông báo")
                .setMessage("Gọi thoại cho khách hàng xảy ra lỗi. Hãy thử lại xem sao.")
                .setPositiveButton("Thử lại", v -> {
                    dialog.dismiss();
                })
                .show();
    }

    @Override
    public void onMessageItemLongClickListener(View view, int position) {

    }

    @Override
    public void onTrySendMessageAgain(int position) {

    }

    @Override
    public void onMessageItemClickListener(int position) {

    }

    @Override
    public void onChatImageClickListener(View view) {
        TedPermission.with(this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        showImagePicker();
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        Toast.makeText(ChatActivity.this, "Cấp quyền bị từ chối\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                    }
                })
                .setDeniedMessage("Nếu từ chối, bạn sẽ có thể không thể sử dụng dịch vụ này\n\nXin hãy cấp quyền tại [Setting] > [Permission]")
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .check();
    }

    private void showImagePicker() {
        chatKeyBoard.reset();
        TedBottomPicker.with(this)
                .setTitle("Chọn ảnh")
                .setPeekHeight(1500)
                .setCompleteButtonText("Xong")
                .setEmptySelectionText("Chưa chọn ảnh")
                .show(uri -> {
                    doSendMessage(uri.toString(), MessageType.IMAGE);
                });
    }

    @Override
    public void onSendButtonClickListener(View view) {
        String message = chatKeyBoard.getEtChat().getText().toString().trim();

        if (!TextUtils.isEmpty(message)) {
            doSendMessage(message, MessageType.TEXT);
        }
    }

    private void doSendMessage(String message, MessageType messageType) {
        Message sendMessageRequest = Message.createNewMessage(db, message, orderDocRef, messageType);
        db.collection(CollectionConst.COLLECTION_MESSAGE)
                .add(sendMessageRequest)
                .addOnSuccessListener(documentReference -> {
                    scrollToBottom();
                    chatKeyBoard.getEtChat().setText("");
                    saveLastMessage(sendMessageRequest);
                })
                .addOnFailureListener(e -> Log.d(TAG, "sendMessage::onFailure: " + e.getMessage()));
    }

    private void saveLastMessage(Message sendMessageRequest) {
        db.collection(CollectionConst.COLLECTION_LAST_MESSAGE)
                .document(orderDocRef.getId())
                .set(sendMessageRequest)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "updateLastMessage::onSuccess");
                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, "updateLastMessage::onFailure: " + e.getMessage());
                });
    }

    @Override
    public void onSampleMessageClickListener(View view) {
        DialogSampleMessageListBinding sampleMessageListBinding = DialogSampleMessageListBinding.inflate(LayoutInflater.from(this), (ViewGroup) this.getWindow().getDecorView(), false);
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        builder.setView(sampleMessageListBinding.getRoot());

        AlertDialog dialog = builder.create();
        dialog.show();

        sampleMessageListBinding.lvSampleMessage.setOnItemClickListener((parent, view1, position, id) -> {
            String message = (String) sampleMessageListBinding.lvSampleMessage.getItemAtPosition(position);
            doSendMessage(message, MessageType.TEXT);
            dialog.dismiss();
            chatKeyBoard.reset();
        });

        sampleMessageListBinding.btnCancel.setOnClickListener(v -> {
            dialog.dismiss();
            chatKeyBoard.reset();
        });

        dialog.setOnDismissListener(dialog1 -> chatKeyBoard.reset());
        dialog.setOnCancelListener(dialog1 -> chatKeyBoard.reset());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat_window, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onAudioChatClickListener(View view) {

    }

    @Override
    public void OnFuncPop(int i) {

    }

    @Override
    public void OnFuncClose() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        chatKeyBoard.reset();
    }
}