package dev.vcgroup.thonowforworkerandroidapp.ui;

import android.util.Log;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Address;
import dev.vcgroup.thonowforworkerandroidapp.data.model.CountryInfo;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Worker;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.MyCustomAlertDialog;

public class MainScreenViewModel extends ViewModel {
    private static final String TAG = MainScreenViewModel.class.getSimpleName();
    private MutableLiveData<Worker> currentUser = new MutableLiveData<>();
    private MutableLiveData<CountryInfo> selectedCountryCode = new MutableLiveData<>();
    private MutableLiveData<Address> currentAddress = new MutableLiveData<>();
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }

    public MainScreenViewModel() {
        this.getCurrentWorker();
        this.setSelectedCountryCode(new CountryInfo("Vietnam", "VN", "84"));
    }

    public MutableLiveData<Worker> getCurrentUser() {
        if (this.currentUser.getValue() == null) {
            this.getCurrentWorker();
        }

        return currentUser;
    }

    public void setCurrentUser(Worker currentUser) {
        this.currentUser.setValue(currentUser);
        Log.d(TAG, "phoneNum " + currentUser.toString());
        getCountryInfoFromPhone(currentUser.getPhoneNumber());
    }

    public void onActiveChange() {
        Worker worker = getCurrentUser().getValue();
        if (worker != null) {
            worker.setActive(!worker.isActive());
        }
        this.setCurrentUser(worker);
    }

    public MutableLiveData<CountryInfo> getSelectedCountryCode() {
        return selectedCountryCode;
    }

    public void setSelectedCountryCode(CountryInfo selectedCountryCode) {
        this.selectedCountryCode.setValue(selectedCountryCode);
    }

    private void getCountryInfoFromPhone(String phoneNumber) {
        String callingCode = phoneNumber.substring(1, 3);
        db.collection(CollectionConst.LIB_COUNTRY_CODE)
                .whereEqualTo("callingCode", callingCode)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<CountryInfo> list = queryDocumentSnapshots.toObjects(CountryInfo.class);
                    if (!list.isEmpty()) {
                        CountryInfo countryInfo = queryDocumentSnapshots.toObjects(CountryInfo.class).get(0);
                        this.setSelectedCountryCode(countryInfo);
                    }
                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, e.getMessage());
                });
    }

    public void getCurrentWorker() {
        db.collection(CollectionConst.COLLECTION_WORKER)
                .document(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                .addSnapshotListener((value, error) -> {
                    if (error != null) {
                        Log.d(TAG, error.getMessage());
                        return;
                    }

                    if (value != null) {
                        setCurrentUser(value.toObject(Worker.class));
                    }
                });
    }

    public int getGenderRadioButtonId() {
        if (getCurrentUser().getValue().isMale()) {
            return R.id.rb_gender_male;
        }
        return R.id.rb_gender_female;
    }

    public RadioGroup.OnCheckedChangeListener rgCheckedChangeListener = (group, checkedId) -> {
        Worker worker = getCurrentUser().getValue();
        if (worker != null) {
            worker.setMale(checkedId == R.id.rb_gender_male);
        }
        setCurrentUser(worker);
    };

    public CompoundButton.OnCheckedChangeListener switchCheckedChangeListener = (buttonView, isChecked) -> {
        if (!isChecked) {
            MyCustomAlertDialog dialog = MyCustomAlertDialog.create(buttonView.getContext());
            dialog.setTitle("Tắt trạng thái hoạt động")
                    .setMessage("Việc tắt trạng thái hoạt động bạn sẽ không nhận được bất kỳ công việc nào trong thời gian tới.")
                    .setPositiveButton("Tắt", v -> {
                        updateActiveStatus(false);
                        dialog.dismiss();
                    })
                    .setNegativeButton("Hủy", v -> {
                        buttonView.setChecked(true);
                        dialog.dismiss();
                    })
                    .show();
        } else {
            CommonUtil.getCurrentUserRef(db)
                    .get()
                    .addOnSuccessListener(documentSnapshot -> {
                        Worker worker = documentSnapshot.toObject(Worker.class);
                        if (worker.getBalanceMoney() >= 50000 && worker.isVerified()) {
                            updateActiveStatus(true);
                        } else {
                            MyCustomAlertDialog dialog = MyCustomAlertDialog.create(buttonView.getContext());
                            dialog.setTitle("Không thể bật trạng thái nhận việc")
                                    .setMessage("Bạn cần xác minh danh tính hoặc số dư trong ví phải lớn hơn bằng 50.000đ để tiếp tục.")
                                    .setPositiveButton("Đã hiểu", v -> {
                                        dialog.dismiss();
                                    })
                                    .show();
                        }
                    })
                    .addOnFailureListener(e -> {
                        Log.d(TAG, e.getMessage());
                        Toast.makeText(buttonView.getContext(), "Đã có lỗi xảy ra. Xin thử lại!", Toast.LENGTH_SHORT).show();
                    });
        }
    };

    public OnClickListener onSwitchClickedListener = v -> {
        SwitchMaterial btnSwitch = (SwitchMaterial) v;
        if (btnSwitch.isChecked()) {
            MyCustomAlertDialog dialog = MyCustomAlertDialog.create(btnSwitch.getContext());
            dialog.setTitle("Tắt trạng thái hoạt động")
                    .setMessage("Việc tắt trạng thái hoạt động bạn sẽ không nhận được bất kỳ công việc nào trong thời gian tới.")
                    .setPositiveButton("Tắt", v1 -> {
                        btnSwitch.setChecked(false);
                        btnSwitch.setOnCheckedChangeListener(switchCheckedChangeListener);
                        updateActiveStatus(false);
                        dialog.dismiss();
                    })
                    .setNegativeButton("Hủy", v1 -> dialog.dismiss())
                    .show();
        }
    };


    private void updateActiveStatus(boolean isActive) {
        db.collection(CollectionConst.COLLECTION_WORKER)
                .document(this.currentUser.getValue().getId())
                .update("active", isActive)
                .addOnSuccessListener(aVoid -> {

                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
    }

    public MutableLiveData<Address> getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(Address currentAddress) {
        this.currentAddress.setValue(currentAddress);}

}
