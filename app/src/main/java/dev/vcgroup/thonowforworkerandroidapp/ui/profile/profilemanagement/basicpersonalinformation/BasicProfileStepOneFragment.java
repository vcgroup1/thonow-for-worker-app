package dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.basicpersonalinformation;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Worker;
import dev.vcgroup.thonowforworkerandroidapp.databinding.BsdChooseCountryCodeBinding;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentBasicProfileStepOneBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.MainScreenViewModel;
import dev.vcgroup.thonowforworkerandroidapp.ui.homepage.HomePageFragment;
import dev.vcgroup.thonowforworkerandroidapp.ui.loginscreen.adapter.CountryCodeAdapter;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.basicpersonalinformation.definemylocation.DefineMyLocationFragment;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.FragmentUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.MyCustomAlertDialog;

import static dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil.getInputText;

public class BasicProfileStepOneFragment extends Fragment implements Step, CountryCodeAdapter.OnCountryCodeListener {
    private static final String TAG = BasicProfileStepOneFragment.class.getSimpleName();
    private FragmentBasicProfileStepOneBinding binding;
    private MainScreenViewModel mViewModel;
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }
    private BottomSheetDialog changeCountryBsd;
    private CountryCodeAdapter countryCodeAdapter;


    public static BasicProfileStepOneFragment newInstance() {
        return new BasicProfileStepOneFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_basic_profile_step_one, container, false);
        mViewModel = ViewModelProviders.of(requireActivity()).get(MainScreenViewModel.class);
        binding.setItem(mViewModel);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.dropDownCountry.setOnClickListener(v -> {
            BsdChooseCountryCodeBinding chooseCountryCodeBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.bsd_choose_country_code, (ViewGroup) v.getRootView(), false);
            changeCountryBsd = new BottomSheetDialog(requireActivity());
            changeCountryBsd.setContentView(chooseCountryCodeBinding.getRoot());

            countryCodeAdapter = new CountryCodeAdapter(db, this);
            chooseCountryCodeBinding.rvCountries.addItemDecoration(new DividerItemDecoration(requireActivity(), DividerItemDecoration.VERTICAL));
            chooseCountryCodeBinding.rvCountries.setItemAnimator(new DefaultItemAnimator());
            chooseCountryCodeBinding.rvCountries.setAdapter(countryCodeAdapter);
            changeCountryBsd.show();

            chooseCountryCodeBinding.svCountryCode.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    countryCodeAdapter.getFilter().filter(query);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    countryCodeAdapter.getFilter().filter(newText);
                    return true;
                }
            });
        });

        Objects.requireNonNull(binding.tipAddress.getEditText()).setOnClickListener(v -> {
            FragmentUtil.replaceFragment(requireActivity(), R.id.basic_profile_container,
                    DefineMyLocationFragment.newInstance(), null, DefineMyLocationFragment.class.getSimpleName(),
                    true);
        });

        mViewModel.getCurrentAddress().observe(requireActivity(), address -> {
            if (address != null) {
                CommonUtil.setInputText(binding.tipAddress, address.getAddress());
            }
        });
    }

    private Worker validateForm(Worker worker) {
        binding.tipName.setError(null);
        binding.tipResidentsAddress.setError(null);
        binding.tipId.setError(null);
        String displayName = getInputText(binding.tipName);
        String residentAddress = getInputText(binding.tipResidentsAddress);
        String identityNumber = getInputText(binding.tipId);

        if (displayName.isEmpty()) {
            binding.tipName.setError("Vui lòng nhập họ tên");
            return null;
        }
        worker.setDisplayName(displayName);

        if (residentAddress.isEmpty()) {
            binding.tipResidentsAddress.setError("Vui lòng nhập địa chỉ thường trú");
            return null;
        }
        worker.setResidentAddress(residentAddress);

        if (identityNumber.isEmpty()) {
            binding.tipId.setError("Vui lòng nhập số CMND, CCCD");
            return null;
        } else {
//            Log.d(TAG, identityNumber.length() + "");
//            if (identityNumber.length() != 9 || identityNumber.length() != 12) {
//                binding.tipId.setError("Vui lòng nhập số CMND, CCCD hợp lệ");
//                return null;
//            }
        }
        worker.setIdNumber(identityNumber);

        if (mViewModel.getCurrentAddress().getValue() != null) {
            worker.setAddress(mViewModel.getCurrentAddress().getValue());
        }

        return worker;
    }

    private void updateUserProfile(Worker worker) {
        db.collection(CollectionConst.COLLECTION_WORKER)
                .document(worker.getId())
                .set(worker)
                .addOnSuccessListener(aVoid -> {
                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setDisplayName(worker.getDisplayName())
                            .build();

                    Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).updateProfile(profileUpdates)
                            .addOnSuccessListener(aVoid1 -> Log.d("updateDisplayName", "Success!"))
                            .addOnFailureListener(e -> Log.d("updateDisplayName", "Failed!"));

                });

    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        if (!binding.rbAcceptTerms.isChecked()) {
            MyCustomAlertDialog dialog = MyCustomAlertDialog.create(requireActivity());
            dialog.setTitle("Thông báo")
                    .setMessage("Vui lòng đồng ý điều khoản và chính sách bảo mật để tiếp túc.")
                    .setPositiveButton("Đã hiểu", v1 -> dialog.dismiss())
                    .show();
            return new VerificationError("Vui lòng điền đầy đủ trước khi tiếp tục");
        } else {
            Worker worker = validateForm(mViewModel.getCurrentUser().getValue());
            if (worker != null) {
                updateUserProfile(worker);
            } else {
                return new VerificationError("Vui lòng điền đầy đủ trước khi tiếp tục");
            }
        }
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public void onCountryCodeClickedListener(int position) {
        mViewModel.setSelectedCountryCode(countryCodeAdapter.getItem(position));
        new Handler().postDelayed(() -> {
            changeCountryBsd.cancel();
        }, 1000);
    }
}