package dev.vcgroup.thonowforworkerandroidapp.ui.signinscreen;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivitySignInScreenBinding;
import dev.vcgroup.thonowforworkerandroidapp.util.FragmentUtil;

public class SignInActivity extends AppCompatActivity {
    private static final String SIGN_IN_STEP_ONE_TAG = SignInStepOneFragment.class.getSimpleName();
    private ActivitySignInScreenBinding binding;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in_screen);
        Bundle bundle = null;
        if (getIntent() != null) {
            bundle = getIntent().getExtras();
        }
        setupToolbar();
        FragmentUtil.replaceFragment(this, R.id.register_fragment_container, new SignInStepOneFragment(),
                bundle, SIGN_IN_STEP_ONE_TAG, true);

    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.mToolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setTitle(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sign_in_guideline, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_ask_question_guide) {
            showSignInGuide();
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigateUp() {
        onBackPressed();
        return super.onNavigateUp();
    }

    private void showSignInGuide() {

    }
}