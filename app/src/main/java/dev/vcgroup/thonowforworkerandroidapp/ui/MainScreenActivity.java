package dev.vcgroup.thonowforworkerandroidapp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.sinch.android.rtc.SinchError;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcels;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CallWorkerConst;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.fcm.Token;
import dev.vcgroup.thonowforworkerandroidapp.data.remote.sinchservice.SinchService;
import dev.vcgroup.thonowforworkerandroidapp.ui.audiocall.BaseSinchActivity;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;

import static dev.vcgroup.thonowforworkerandroidapp.constant.CallWorkerConst.IS_FROM_RECEIVED_CALL_WORKER_REQUEST_CODE;

public class MainScreenActivity extends BaseSinchActivity {
    private static final String TAG = MainScreenActivity.class.getSimpleName();
    public BottomNavigationView navView;
    private MainScreenViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        mViewModel = ViewModelProviders.of(this).get(MainScreenViewModel.class);
        navView = findViewById(R.id.nav_view);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(navView, navController);

        if (getIntent().getExtras() != null) {
            if (getIntent().getBooleanExtra(IS_FROM_RECEIVED_CALL_WORKER_REQUEST_CODE, false)) {
                navView.setSelectedItemId(R.id.navigation_task_management);
            }
        }
        
        createOrUpdateFcmToken();
    }

    private void createOrUpdateFcmToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Token tokenObj = new Token(task.getResult());
                        FirebaseFirestore.getInstance().collection(CollectionConst.COLLECTION_TOKEN)
                                .document(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                                .set(tokenObj)
                                .addOnSuccessListener(aVoid -> Log.d(TAG, "updated new token"))
                                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
                    } else {
                        Log.d(TAG, "create or update token cause errors!");
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCodeConst.CHARGE_MONEY_REQ_CODE && resultCode == RESULT_OK) {
            Log.d(TAG, "vào đây");
            mViewModel.getCurrentWorker();
        }
    }
}