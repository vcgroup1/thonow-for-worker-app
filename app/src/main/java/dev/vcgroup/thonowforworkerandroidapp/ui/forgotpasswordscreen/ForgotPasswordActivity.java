package dev.vcgroup.thonowforworkerandroidapp.ui.forgotpasswordscreen;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivityForgotPasswordBinding;
import dev.vcgroup.thonowforworkerandroidapp.util.FragmentUtil;

public class ForgotPasswordActivity extends AppCompatActivity {
    private ActivityForgotPasswordBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        FragmentUtil.replaceFragment(this, R.id.forgot_password_fragment_container, new ForgotPasswordStepOneFragment(),
                null, ForgotPasswordStepOneFragment.class.getSimpleName(), false);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupToolbar();
    }

    @Override
    protected void onStop() {
        super.onStop();
        getSupportActionBar().hide();
    }

    private void setupToolbar() {
        Toolbar toolbar = binding.topAppBar.mToolbar;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(null);
        binding.topAppBar.tvTitleToolbar.setText("Quên mật khẩu");
        getSupportActionBar().show();

        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            Fragment currentFragment = FragmentUtil.getCurrentFragment(ForgotPasswordActivity.this, R.id.forgot_password_fragment_container);
            if (currentFragment instanceof ForgotPasswordStepOneFragment) {
                binding.topAppBar.tvTitleToolbar.setText("Quên mật khẩu");
            } else if (currentFragment instanceof  ForgotPasswordStepTwoFragment) {
                binding.topAppBar.tvTitleToolbar.setText("Nhập mã xác thực");
            } else if (currentFragment instanceof ForgotPasswordStepThreeFragment) {
                binding.topAppBar.tvTitleToolbar.setText("Tạo mật khẩu mới");
            } else {
                binding.topAppBar.tvTitleToolbar.setText("Tạo mật khẩu mới");
            }
        });
    }
}
