package dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.basicpersonalinformation.definemylocation.picklocationinmap;

import android.Manifest;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.List;

import dev.vcgroup.thonowforworkerandroidapp.BuildConfig;
import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Address;
import dev.vcgroup.thonowforworkerandroidapp.data.model.ApiError;
import dev.vcgroup.thonowforworkerandroidapp.data.remote.mapservice.geocoding.GeocodeApiService;
import dev.vcgroup.thonowforworkerandroidapp.data.remote.mapservice.geocoding.RetrofitClientInstance;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentChooseLocationInMapBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.MainScreenViewModel;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.basicpersonalinformation.BasicProfileStepOneFragment;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.basicpersonalinformation.definemylocation.DefineMyLocationFragment;
import dev.vcgroup.thonowforworkerandroidapp.util.ApiErrorUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.FragmentUtil;
import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PickLocationInMapFragment extends Fragment implements GoogleMap.OnMarkerDragListener {
    private static final String TAG = PickLocationInMapFragment.class.getSimpleName();
    private FragmentChooseLocationInMapBinding binding;
    private MainScreenViewModel mViewModel;
    private BottomSheetDialog extraAddressInfoBsd;

    // Google Maps
    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private GeocodeApiService geocodeApiService;
    private Address lastSelectedAddress;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_choose_location_in_map, container, false);
        mViewModel = ViewModelProviders.of(requireActivity()).get(MainScreenViewModel.class);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (TextUtils.isEmpty(BuildConfig.MAPS_API_KEY)) {
            throw new IllegalStateException("You forgot to supply a Google Maps API key");
        }
        lastSelectedAddress = mViewModel.getCurrentAddress().getValue();
        if (lastSelectedAddress != null) {
            binding.tvPickLocationAddr.setText(lastSelectedAddress.getAddress());
        }

        setupMapIfNeeded();

        binding.btnPickLocationSubmit.setOnClickListener(v -> {
            showAddExtraInfoBsd();
        });
    }

    protected void setupMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mapFragment == null) {
            mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map2);
            // Check if we were successful in obtaining the map.
            if (mapFragment != null) {
                mapFragment.getMapAsync(this::loadMap);
            } else {
                Log.d(TAG, "Error - Map Fragment was null!!");
            }
        }
    }

    private void loadMap(GoogleMap googleMap) {
        map = googleMap;
        if (map != null) {
            Log.d(TAG, "Map Fragment was loaded properly!");
            // Map is ready
            TedPermission.with(requireActivity())
                    .setPermissionListener(new PermissionListener() {
                        @SuppressLint("MissingPermission")
                        @Override
                        public void onPermissionGranted() {
                            // Add a marker at current location
                            Address currentAddress = mViewModel.getCurrentAddress().getValue();
                            if (currentAddress != null) {
                                LatLng currentLatlng = new LatLng(currentAddress.getLatitude(), currentAddress.getLongtitude());
                                MarkerOptions markerOptions = new MarkerOptions()
                                        .position(currentLatlng)
                                        .draggable(true);
                                map.addMarker(markerOptions);
                                map.moveCamera(CameraUpdateFactory.newLatLng(currentLatlng));
                                map.getUiSettings().setZoomControlsEnabled(true);
                            }
                            map.setOnMarkerDragListener(PickLocationInMapFragment.this);
                        }

                        @Override
                        public void onPermissionDenied(List<String> deniedPermissions) {
                            Snackbar.make(binding.getRoot(), "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                        }
                    })
                    .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                    .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                    .check();
        } else {
            Log.d(TAG, "Error - Map was null!!");
        }
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        LatLng latLng = marker.getPosition();
        updateNewAddress(latLng);
    }

    private void updateNewAddress(LatLng latLng) {
        if (geocodeApiService == null) {
            geocodeApiService = RetrofitClientInstance.getInstance(getActivity()).getRetrofit().create(GeocodeApiService.class);
        }
        Call<JsonObject> call = geocodeApiService.getFormattedAddress(CommonUtil.getFormattedLatlng(latLng));
        call.enqueue(new Callback<JsonObject>() {
            @SneakyThrows
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        JsonObject result = response.body();
                        if (result.get("status").getAsString().equals("OK")) {
                            JsonArray results = result.getAsJsonArray("results");
                            String fullAddress = results.get(0).getAsJsonObject().get("formatted_address").getAsString();
                            binding.tvPickLocationAddr.setText(fullAddress);
                            lastSelectedAddress = new Address(latLng.latitude, latLng.longitude, fullAddress);
                            mViewModel.setCurrentAddress(lastSelectedAddress);
                        }
                    }
                } else {
                    ApiError apiError = ApiErrorUtil.parseError(getActivity(), response);
                    Log.d(TAG, "getAddress::onError " + apiError.toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "getAddress::onFailure" + t.getMessage());
            }
        });
    }

    private void showAddExtraInfoBsd() {
        View addExtraInfoView = getLayoutInflater().inflate(R.layout.bsd_define_my_location_extra_info, null);
        extraAddressInfoBsd = new BottomSheetDialog(getContext());
        extraAddressInfoBsd.setContentView(addExtraInfoView);
        extraAddressInfoBsd.show();

        TextInputLayout tipExtraInfo = addExtraInfoView.findViewById(R.id.tip_extra_info);

        TextView tvCurrentAddress = addExtraInfoView.findViewById(R.id.tvCurrentAddress);
        tvCurrentAddress.setText(lastSelectedAddress.getAddress());

        addExtraInfoView.findViewById(R.id.extra_info_cancel).setOnClickListener(v -> {
            extraAddressInfoBsd.cancel();
            backToCallWorkerFragment();
        });

        addExtraInfoView.findViewById(R.id.bsd_close).setOnClickListener(v -> {
            extraAddressInfoBsd.cancel();
        });

        addExtraInfoView.findViewById(R.id.extra_info_add).setOnClickListener(v -> {
            String extraInfoStr = CommonUtil.getInputText(tipExtraInfo);
            if (!extraInfoStr.trim().isEmpty()) {
                String currentAddress = extraInfoStr + " " + tvCurrentAddress.getText();
                Address newAddress = new Address(lastSelectedAddress.getLatitude(), lastSelectedAddress.getLongtitude(), currentAddress);
                mViewModel.setCurrentAddress(newAddress);
            }
            extraAddressInfoBsd.cancel();
            backToCallWorkerFragment();
        });
    }

    private void backToCallWorkerFragment() {
        FragmentUtil.replaceFragment(getActivity(), R.id.basic_profile_container,
                FragmentUtil.getFragmentByTag((AppCompatActivity) requireActivity(), BasicProfileStepOneFragment.class.getSimpleName()), null,
                BasicProfileStepOneFragment.class.getSimpleName(), true);
        FragmentUtil.removeFragmentByTag((AppCompatActivity) requireActivity(), PickLocationInMapFragment.class.getSimpleName());
        FragmentUtil.removeFragmentByTag((AppCompatActivity) requireActivity(), DefineMyLocationFragment.class.getSimpleName());
    }
}
