package dev.vcgroup.thonowforworkerandroidapp.ui.walkthroughscreen;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.ui.loginscreen.LoginActivity;

public class WalkthroughActivity extends AppCompatActivity {
    private static String TAG = WalkthroughActivity.class.getSimpleName();
    private ViewPager2 vpWelcomeSlider;
    private DotsIndicator dotsIndicator;
    private FloatingActionButton fabNext;
    private WalkthroughSliderAdapter walkthroughSliderAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkthrough);
        
        init();

        setupWelcomeSlider();

        findViewById(R.id.btn_skip).setOnClickListener(v -> {
            navigateToWelcomeScreen();
        });
    }

    private void setupWelcomeSlider() {
        walkthroughSliderAdapter = new WalkthroughSliderAdapter();
        vpWelcomeSlider.setAdapter(walkthroughSliderAdapter);
        dotsIndicator.setViewPager2(vpWelcomeSlider);
        vpWelcomeSlider.setClipToPadding(false);
        vpWelcomeSlider.setClipChildren(false);
        vpWelcomeSlider.setOffscreenPageLimit(3);
        vpWelcomeSlider.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

        fabNext.setOnClickListener(v -> {
            int index = vpWelcomeSlider.getCurrentItem();
            if (index == (walkthroughSliderAdapter.getItemCount() - 1)) {
                navigateToWelcomeScreen();
            }
            else
                vpWelcomeSlider.setCurrentItem(vpWelcomeSlider.getCurrentItem() + 1);
        });
    }

    private void navigateToWelcomeScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent, ActivityOptions.makeCustomAnimation(this, android.R.anim.slide_in_left, android.R.anim.slide_out_right).toBundle());
        finish();
    }

    private void init() {
        vpWelcomeSlider = findViewById(R.id.vpWelcomeInApp);
        dotsIndicator = findViewById(R.id.slider_indicator);
        fabNext = findViewById(R.id.fab_welcome_next);
    }
}