package dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import dev.vcgroup.thonowforworkerandroidapp.data.model.OrderDetail;
import dev.vcgroup.thonowforworkerandroidapp.data.model.OrderDetail;
import dev.vcgroup.thonowforworkerandroidapp.data.model.OrderDetail;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Service;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ItemOrderDetailServiceBinding;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class OrderDetailsAdapter extends RecyclerView.Adapter<OrderDetailsAdapter.OrderDetailVH> {
    private static final String TAG = OrderDetailsAdapter.class.getSimpleName();
    private List<OrderDetail> orderDetails;
    private AsyncListDiffer<OrderDetail> mDiffer;
    private OnOrderDetailListener onOrderDetailListener;
    private DiffUtil.ItemCallback<OrderDetail> mCallback = new DiffUtil.ItemCallback<OrderDetail>() {
        @Override
        public boolean areItemsTheSame(@NonNull OrderDetail oldItem, @NonNull OrderDetail newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull OrderDetail oldItem, @NonNull OrderDetail newItem) {
            return oldItem.equals(newItem);
        }
    };

    public OrderDetailsAdapter(List<OrderDetail> orderDetails, OnOrderDetailListener onOrderDetailListener) {
        this.orderDetails = orderDetails;
        this.mDiffer = new AsyncListDiffer<>(this, mCallback);
        this.onOrderDetailListener = onOrderDetailListener;
    }

    @NonNull
    @NotNull
    @Override
    public OrderDetailVH onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new OrderDetailVH(ItemOrderDetailServiceBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false), onOrderDetailListener);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull OrderDetailsAdapter.OrderDetailVH holder, int position) {
        OrderDetail orderDetail = getItemAt(position);
        if (orderDetail != null) {
            orderDetail.getService()
                    .get()
                    .addOnSuccessListener(documentSnapshot -> {
                        Service service = documentSnapshot.toObject(Service.class);
                        holder.bindItem(orderDetail, service);
                    })
                    .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
        }
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public OrderDetail getItemAt(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    public void update(List<OrderDetail> newList) {
        mDiffer.submitList(newList);
    }
    
    public void addOrUpdateItem(OrderDetail orderDetail) {
        List<OrderDetail> list = new ArrayList<>(mDiffer.getCurrentList());
        if(!updateItem(orderDetail)) {
            list.add(orderDetail);
            notifyItemInserted(list.size() - 1);
            update(list);
        }
    }

    public List<OrderDetail> getCurrentList()   {
        return mDiffer.getCurrentList();
    }

    public void deleteItem(int position) {
        List<OrderDetail> list = new ArrayList<>(mDiffer.getCurrentList());
        if (!list.isEmpty()) {
            list.remove(position);
            update(list);
        }
    }

    private boolean updateItem(OrderDetail orderDetail) {
        List<OrderDetail> list = new ArrayList<>(mDiffer.getCurrentList());
        int index = list.indexOf(orderDetail);
        if (index >= 0) {
            list.set(index, orderDetail);
            notifyItemChanged(index);
            update(list);
            return true;
        }
        return false;
    }

    public static class OrderDetailVH extends RecyclerView.ViewHolder {
        private ItemOrderDetailServiceBinding binding;
        private OnOrderDetailListener onOrderDetailListener;

        public OrderDetailVH(@NonNull @NotNull ItemOrderDetailServiceBinding binding, OnOrderDetailListener onOrderDetailListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.onOrderDetailListener = onOrderDetailListener;
            this.binding.getRoot().setOnClickListener(v -> {
                if (onOrderDetailListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        onOrderDetailListener.onOrderDetailClickedListener(v, position);
                    }
                }
            });
        }

        public void bindItem(OrderDetail orderDetail, Service service) {
            binding.setOrderDetail(orderDetail);
            binding.setService(service);
            binding.executePendingBindings();
        }
    }

    public interface OnOrderDetailListener {
        void onOrderDetailClickedListener(View view, int position);
    }
}
