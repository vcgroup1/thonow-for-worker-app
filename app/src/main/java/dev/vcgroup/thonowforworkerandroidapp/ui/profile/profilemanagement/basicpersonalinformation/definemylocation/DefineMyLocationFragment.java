package dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.basicpersonalinformation.definemylocation;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;
import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Address;
import dev.vcgroup.thonowforworkerandroidapp.data.model.ApiError;
import dev.vcgroup.thonowforworkerandroidapp.data.remote.mapservice.geocoding.GeocodeApiService;
import dev.vcgroup.thonowforworkerandroidapp.data.remote.mapservice.geocoding.RetrofitClientInstance;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentDefineMyLocationBinding;
import dev.vcgroup.thonowforworkerandroidapp.ui.MainScreenViewModel;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.basicpersonalinformation.BasicProfileStepOneFragment;
import dev.vcgroup.thonowforworkerandroidapp.ui.profile.profilemanagement.basicpersonalinformation.definemylocation.picklocationinmap.PickLocationInMapFragment;
import dev.vcgroup.thonowforworkerandroidapp.util.ApiErrorUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.FragmentUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.GPSTracker;
import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DefineMyLocationFragment extends Fragment implements PlaceAutocompleteAdapter.OnPlaceAutocompleteListener {
    private static final String TAG = DefineMyLocationFragment.class.getSimpleName();
    private FragmentDefineMyLocationBinding binding;
    private MainScreenViewModel mViewModel;
    private BottomSheetDialog extraAddressInfoBsd;
    private PlaceAutocompleteAdapter adapter;
    private Address oldAddress;
    private GPSTracker gpsTracker;
    private GeocodeApiService geocodeApiService;

    public static DefineMyLocationFragment newInstance() {
        return new DefineMyLocationFragment();
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_define_my_location, container, false);
        mViewModel = ViewModelProviders.of(requireActivity()).get(MainScreenViewModel.class);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerForClickListener();

        geocodeApiService = RetrofitClientInstance.getInstance(requireActivity()).getRetrofit().create(GeocodeApiService.class);
        gpsTracker = new GPSTracker(requireActivity());
        if (gpsTracker.canGetLocation()) {
            LatLng latLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());

            Call<JsonObject> call = geocodeApiService.getFormattedAddress(CommonUtil.getFormattedLatlng(latLng));
            call.enqueue(new Callback<JsonObject>() {
                @SneakyThrows
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            JsonObject result = response.body();
                            if (result.get("status").getAsString().equals("OK")) {
                                JsonArray results = result.getAsJsonArray("results");
                                String fullAddress = results.get(0).getAsJsonObject().get("formatted_address").getAsString();

                                binding.tvUpdateLocation.setText(fullAddress);
                                oldAddress = new Address(latLng.latitude, latLng.longitude, fullAddress);
                                mViewModel.setCurrentAddress(oldAddress);
                            }
                        }
                    } else {
                        ApiError apiError = ApiErrorUtil.parseError(getActivity(), response);
                        Log.d(TAG, "getAddress::onError " + apiError.toString());
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.d(TAG, "getAddress::onFailure" + t.getMessage());
                }
            });
        }

//        mViewModel.getCurrentAddress().observe(requireActivity(), (Observer<Address>) address -> {
//            oldAddress = address;
//            if (oldAddress != null) {
//                binding.tvUpdateLocation.setText(oldAddress.getAddress());
//            }
//        });

        setupAutocompleteSearchPlaces();
    }

    private void setupAutocompleteSearchPlaces() {
        adapter = new PlaceAutocompleteAdapter(requireActivity(), this);
        binding.rvPlaceSearch.setAdapter(adapter);

        binding.svPlace.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                doSearchPlace(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                doSearchPlace(newText);
                return true;
            }
        });
    }

    private void doSearchPlace(String queryText) {
        if (queryText.isEmpty()) {
            binding.btnAddExtraInfo.setVisibility(View.VISIBLE);
            binding.btnPickLocationInMap.setVisibility(View.VISIBLE);
            binding.rvPlaceSearch.setVisibility(View.GONE);
        } else {
            binding.btnAddExtraInfo.setVisibility(View.GONE);
            binding.btnPickLocationInMap.setVisibility(View.GONE);
            binding.rvPlaceSearch.setVisibility(View.VISIBLE);
            adapter.getFilter().filter(queryText);
        }
    }

    private void registerForClickListener() {
        binding.btnPickLocationInMap.setOnClickListener(v -> {
            FragmentUtil.replaceFragment(getActivity(), R.id.basic_profile_container,
                    new PickLocationInMapFragment(), null, PickLocationInMapFragment.class.getSimpleName(), true);
        });

        binding.btnAddExtraInfo.setOnClickListener(v -> {
            if (oldAddress != null) {
                showAddExtraInfoBsd();
            }
        });
    }

    private void showAddExtraInfoBsd() {
        View addExtraInfoView = getLayoutInflater().inflate(R.layout.bsd_define_my_location_extra_info, null);
        extraAddressInfoBsd = new BottomSheetDialog(requireActivity());
        extraAddressInfoBsd.setContentView(addExtraInfoView);
        extraAddressInfoBsd.show();

        TextInputLayout tipExtraInfo = addExtraInfoView.findViewById(R.id.tip_extra_info);

        TextView tvCurrentAddress = addExtraInfoView.findViewById(R.id.tvCurrentAddress);
        tvCurrentAddress.setText(oldAddress.getAddress());

        addExtraInfoView.findViewById(R.id.extra_info_cancel).setOnClickListener(v -> {
            extraAddressInfoBsd.cancel();
            backToCallWorkerFragment();
        });

        addExtraInfoView.findViewById(R.id.bsd_close).setOnClickListener(v -> {
            extraAddressInfoBsd.cancel();
        });

        addExtraInfoView.findViewById(R.id.extra_info_add).setOnClickListener(v -> {
            String extraInfoStr = CommonUtil.getInputText(tipExtraInfo);
            if (!extraInfoStr.trim().isEmpty()) {
                String currentAddress = extraInfoStr + " " + tvCurrentAddress.getText();
                Address newAddress = new Address(oldAddress.getLatitude(), oldAddress.getLongtitude(), currentAddress);
                mViewModel.setCurrentAddress(newAddress);
            }
            extraAddressInfoBsd.cancel();
            backToCallWorkerFragment();
        });
    }

    private void backToCallWorkerFragment() {
        requireActivity().onBackPressed();
    }

    @Override
    public void onPlaceAutocompleteClickedListener(int pos) {
        String address = adapter.getItem(pos).getFullText(null).toString();
        oldAddress = new Address(oldAddress.getLatitude(), oldAddress.getLongtitude(), address);
        mViewModel.setCurrentAddress(oldAddress);
        showAddExtraInfoBsd();
    }
}
