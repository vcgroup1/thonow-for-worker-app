package dev.vcgroup.thonowforworkerandroidapp.ui.taskmanagement.cancelorder;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.OrderStatus;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivityCancelOrderBinding;
import dev.vcgroup.thonowforworkerandroidapp.util.MyCustomAlertDialog;

public class CancelOrderActivity extends AppCompatActivity {
    private static final String TAG = CancelOrderActivity.class.getSimpleName();
    private ActivityCancelOrderBinding binding;
    private String deleteOrderId;
    private FirebaseFirestore db;
    private String reasonStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cancel_order);

        setupToolbar();

        if (getIntent().getExtras() != null) {
            deleteOrderId = getIntent().getStringExtra("deleteOrderId");

            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.cancel_order_reasons));
            binding.tipCancelReason.setAdapter(adapter);
            binding.tipCancelReason.setOnItemClickListener((parent, view, position, id) -> reasonStr = (String) adapter.getItem(position));

            if (!deleteOrderId.isEmpty()) {
                binding.btnCancelOrder.setOnClickListener(v -> {
                    MyCustomAlertDialog dialog = MyCustomAlertDialog.create(this);
                    dialog.setTitle("Xác nhận huỷ đơn hàng")
                            .setMessage("Bạn có chắc muốn huỷ đơn hàng này?")
                            .setPositiveButton("Huỷ đơn", v1 -> {
                                if (db == null) {
                                    this.db = FirebaseFirestore.getInstance();
                                }

                                this.db.collection(CollectionConst.COLLECTION_ORDER)
                                        .document(deleteOrderId)
                                        .update(
                                                "status", OrderStatus.CANCELED,
                                                "note", reasonStr
                                        )
                                        .addOnSuccessListener(unused -> {
                                            setResult(RESULT_OK);
                                            finish();
                                        })
                                        .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
                            })
                            .setNegativeButton("Quay lại", v1 -> {
                                dialog.dismiss();
                            })
                            .show();
                });
            }
        }

    }

    private void setupToolbar() {
        Toolbar mToolbar = binding.topAppBar.mToolbar;
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}