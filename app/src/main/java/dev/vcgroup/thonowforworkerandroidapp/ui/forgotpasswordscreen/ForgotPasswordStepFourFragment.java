package dev.vcgroup.thonowforworkerandroidapp.ui.forgotpasswordscreen;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentForgotPasswordStepFourBinding;
import dev.vcgroup.thonowforworkerandroidapp.util.MyCustomAlertDialog;

import static dev.vcgroup.thonowforworkerandroidapp.util.AuthenticationUtil.isConfirmPasswordMatch;
import static dev.vcgroup.thonowforworkerandroidapp.util.AuthenticationUtil.isValidPasswordForChangePassword;
import static dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil.getInputText;

public class ForgotPasswordStepFourFragment extends Fragment {
    private FragmentForgotPasswordStepFourBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forgot_password_step_four, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        validateForm();

        binding.btnUpdateResetPassword.setOnClickListener(view1 -> doChangePassword());

    }

    private void validateForm() {
        binding.tipNewPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateFormNotEmpty(charSequence.toString(),
                        getInputText(binding.tipNewConfirmPassword));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateFormNotEmpty(charSequence.toString(),
                        getInputText(binding.tipNewConfirmPassword));
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!isValidPasswordForChangePassword(editable.toString())) {
                    binding.tipNewPassword.setError("Mật khẩu phải từ 6-32 ký tự, gồm chữ kèm theo số hoặc ký tự đặc biệt");
                } else {
                    binding.tipNewPassword.setError(null);
                }
            }
        });

        binding.tipNewConfirmPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateFormNotEmpty(charSequence.toString(),
                        getInputText(binding.tipNewPassword));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateFormNotEmpty(charSequence.toString(),
                        getInputText(binding.tipNewPassword));
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!isConfirmPasswordMatch(getInputText(binding.tipNewPassword), editable.toString())) {
                    binding.tipNewConfirmPassword.setError("Mật khẩu không trùng khớp");
                } else {
                    binding.tipNewConfirmPassword.setError(null);
                }
            }
        });
    }

    private void validateFormNotEmpty(String str1, String str2) {
        binding.btnUpdateResetPassword.setEnabled(!str1.isEmpty() && !str2.isEmpty());
    }

    private void doChangePassword() {
        Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).updatePassword(getInputText(binding.tipNewPassword))
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        showNotificationDialog();
                    } else {
                        Snackbar.make(binding.getRoot(), "Cập nhật mật khẩu thất bại. Hãy thử lại", BaseTransientBottomBar.LENGTH_INDEFINITE)
                                .setAction("Thử lại", null).show();
                    }
                });
    }

    private void showNotificationDialog() {
        MyCustomAlertDialog dialog = MyCustomAlertDialog.create(requireActivity());
        dialog.setTitle("Cập nhật thành công")
                .setMessage("Mật khấu của bạn đã được cập nhật. Vui lòng sử dụng mật khẩu này vào lần đăng nhập tiếp theo.")
                .setPositiveButton("Hoàn tất", v -> {
//                    Intent intent = new Intent(getActivity().getApplicationContext(), MainScreenActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intent);
                })
                .show();
    }
}
