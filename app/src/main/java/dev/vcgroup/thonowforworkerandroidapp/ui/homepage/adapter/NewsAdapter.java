package dev.vcgroup.thonowforworkerandroidapp.ui.homepage.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.FirebaseFirestore;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.News;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ItemNewsBinding;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ItemRelatedNewsBinding;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsOfferItemViewHolder> {
    public enum NewsAdapterType {
        NEWS_ITEM,
        RELATED_NEWS_ITEM
    }
    private static final String TAG = NewsAdapter.class.getSimpleName();
    private List<News> list;
    private OnNewsListener onNewsListener;
    private AsyncListDiffer<News> mDiffer;
    private FirebaseFirestore db;
    private NewsAdapterType type;
    private DiffUtil.ItemCallback<News> diffCallback = new DiffUtil.ItemCallback<News>() {
        @Override
        public boolean areItemsTheSame(@NonNull @NotNull News oldItem, @NonNull @NotNull News newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull @NotNull News oldItem, @NonNull @NotNull News newItem) {
            return oldItem.getContent().equals(newItem.getContent());
        }
    };

    public NewsAdapter(OnNewsListener onNewsListener, NewsAdapterType type) {
        this.mDiffer = new AsyncListDiffer<>(this, diffCallback);
        this.onNewsListener = onNewsListener;
        this.db = FirebaseFirestore.getInstance();
        this.type = type;
        this.list = new ArrayList<>();
        if(type.equals(NewsAdapterType.NEWS_ITEM)) {
            this.populateData();
        }
    }

    private void populateData() {
        db.collection(CollectionConst.COLLECTION_NEWS)
                .whereEqualTo("isForWorker", true)
                .addSnapshotListener((value, error) -> {
                    if(error != null) {
                        Log.d(TAG, "get news cause error!");
                        return;
                    }

                    if(value != null) {
                        this.list = value.toObjects(News.class);
                        this.update(list);
                    }
                });
    }

    @NonNull
    @NotNull
    @Override
    public NewsOfferItemViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        NewsOfferItemViewHolder holder = null;
        if(type.equals(NewsAdapterType.NEWS_ITEM)) {
            holder = new NewsOfferItemViewHolder(ItemNewsBinding.inflate(inflater, parent, false), onNewsListener);
        } else {
            holder = new NewsOfferItemViewHolder(ItemRelatedNewsBinding.inflate(inflater, parent, false), onNewsListener);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull NewsAdapter.NewsOfferItemViewHolder holder, int position) {
        News item = getItem(position);
        if(type.equals(NewsAdapterType.NEWS_ITEM)) {
            holder.bindItemNews(item);
        } else {
            holder.bindRelatedNews(item);
        }
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public News getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    public void update(List<News> newList) {
        mDiffer.submitList(newList != null ? new ArrayList<>(newList) : null);
    }

    public class NewsOfferItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ItemNewsBinding binding;
        private ItemRelatedNewsBinding relatedNewsBinding;
        private OnNewsListener onNewsListener;

        public NewsOfferItemViewHolder(@NonNull @NotNull ItemNewsBinding binding, OnNewsListener onNewsListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.onNewsListener = onNewsListener;
            binding.getRoot().setOnClickListener(this);
        }

        public NewsOfferItemViewHolder(@NonNull @NotNull ItemRelatedNewsBinding binding, OnNewsListener onNewsListener) {
            super(binding.getRoot());
            this.relatedNewsBinding = binding;
            this.onNewsListener = onNewsListener;
            binding.getRoot().setOnClickListener(this);
        }

        public void bindItemNews(News item) {
            binding.setItem(item);
            binding.executePendingBindings();
        }

        public void bindRelatedNews(News item) {
            relatedNewsBinding.setItem(item);
            relatedNewsBinding.executePendingBindings();
        }

        @Override
        public void onClick(View v) {
            if (onNewsListener != null) {
                int position = getAdapterPosition();
                if(position != RecyclerView.NO_POSITION) {
                    onNewsListener.onNewsClickedListener(position);
                }
            }
        }
    }

    public interface OnNewsListener {
        void onNewsClickedListener(int position);
    }
}
