package dev.vcgroup.thonowforworkerandroidapp.ui.askpermission;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.List;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.CommonConst;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivityAskPermissionBinding;


public class AskPermissionActivity extends AppCompatActivity {
    private ActivityAskPermissionBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_ask_permission);
        binding.setLifecycleOwner(this);

        binding.btnAccept.setOnClickListener(v -> {
            doAskPermission();
        });

        binding.btnNotNow.setOnClickListener(v -> {
            setResult(RESULT_OK);
            finish();
        });
    }

    private void doAskPermission() {
        TedPermission.with(this)
                .setPermissionListener(new PermissionListener() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onPermissionGranted() {
                        setResult(RESULT_OK);
                        finish();
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        setResult(RESULT_OK);
                        finish();
                    }
                })
                .setPermissions(CommonConst.REQUIRED_PERMISSION)
                .check();
    }
}