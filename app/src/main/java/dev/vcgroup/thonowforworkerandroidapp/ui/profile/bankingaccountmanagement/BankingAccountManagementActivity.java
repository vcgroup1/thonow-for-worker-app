package dev.vcgroup.thonowforworkerandroidapp.ui.profile.bankingaccountmanagement;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.data.model.CreditCardInfo;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Worker;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivityBankingAccountManagementBinding;
import dev.vcgroup.thonowforworkerandroidapp.databinding.BsdCreditCardInfoBinding;
import dev.vcgroup.thonowforworkerandroidapp.util.CommonUtil;
import dev.vcgroup.thonowforworkerandroidapp.util.SpacesItemDecoration;

public class BankingAccountManagementActivity extends AppCompatActivity implements BankingAccountAdapter.OnCreditCardInfoListener {
    private ActivityBankingAccountManagementBinding binding;
    private BankingAccountAdapter adapter;
    private BottomSheetDialog creditCardBsd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_banking_account_management);
        setupCreditCardRv();

        binding.btnAddCreditCard.setOnClickListener(v -> {
            setupBsdCreditCard(v, -1);
        });

        setupToolbar();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupToolbar() {
        Toolbar toolbar = binding.topAppBar.mToolbar;
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        binding.topAppBar.tvTitleToolbar.setText("Tài khoản nhận tiền");
    }

    private void setupCreditCardRv() {
        adapter = new BankingAccountAdapter(new ArrayList<>(), this);
        if (getIntent().getExtras() != null) {
            Worker currentUser = Parcels.unwrap(getIntent().getParcelableExtra("currentUser"));
            adapter.update(currentUser.getCreditCardInfos());
        }
        binding.rvCreditCard.setAdapter(adapter);
        binding.rvCreditCard.addItemDecoration(new SpacesItemDecoration(10));
    }

    @Override
    public void onCreditCardInfoClickedListener(View view, int position) {
        setupBsdCreditCard(view, position);
    }

    private void setupBsdCreditCard(View view, int position) {
        BsdCreditCardInfoBinding bsdCreditCardInfoBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.bsd_credit_card_info, (ViewGroup) view.getRootView(), false);
        creditCardBsd = new BottomSheetDialog(this);
        creditCardBsd.setContentView(bsdCreditCardInfoBinding.getRoot());

        if (position >= 0) {
            CreditCardInfo creditCardInfo = adapter.getItem(position);
            bsdCreditCardInfoBinding.setItem(creditCardInfo);
            bsdCreditCardInfoBinding.btnDeleteCreditCard.setVisibility(View.VISIBLE);
        }

        creditCardBsd.show();

        bsdCreditCardInfoBinding.btnClose.setOnClickListener(v -> creditCardBsd.dismiss());

        bsdCreditCardInfoBinding.btnSaveCreditCard.setOnClickListener(v -> {
           CreditCardInfo creditCardInfo = bsdCreditCardInfoBinding.getItem();
           if (creditCardInfo == null) {
               creditCardInfo = new CreditCardInfo();
           }
            creditCardInfo.setAccountNumber(CommonUtil.getInputText(bsdCreditCardInfoBinding.cardNumber));
            creditCardInfo.setAccountName(CommonUtil.getInputText(bsdCreditCardInfoBinding.cardHolderName));
            creditCardInfo.setBankName(CommonUtil.getInputText(bsdCreditCardInfoBinding.cardBankName));
            creditCardInfo.setBranchName(CommonUtil.getInputText(bsdCreditCardInfoBinding.cardBranch));

            adapter.addOrUpdateItem(creditCardInfo);

            creditCardBsd.dismiss();
        });

        bsdCreditCardInfoBinding.btnDeleteCreditCard.setOnClickListener(v -> {
            adapter.deleteItem(position);
            creditCardBsd.dismiss();
        });
    }

}