package dev.vcgroup.thonowforworkerandroidapp.ui.profile.mywalletmanagement;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import java.util.Objects;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.constant.RequestCodeConst;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ActivityMyWalletManagementBinding;
import dev.vcgroup.thonowforworkerandroidapp.util.FragmentUtil;

public class MyWalletManagementActivity extends AppCompatActivity {
    private ActivityMyWalletManagementBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_wallet_management);
        setupToolbar();
        binding.chargeMoney.setOnClickListener(v -> {
            FragmentUtil.replaceFragment(this, R.id.my_wallet_container, new BalanceMoneyFragment(), null,
                    BalanceMoneyFragment.class.getSimpleName(), false);
        });

        binding.transactionHistory.setOnClickListener(v -> {
            FragmentUtil.replaceFragment(this, R.id.my_wallet_container, new TransactionHistoryFragment(), null,
                    TransactionHistoryFragment.class.getSimpleName(), false);
        });

        binding.revenueStatistics.setOnClickListener(v -> {
            FragmentUtil.replaceFragment(this, R.id.my_wallet_container, new RevenueStatisticFragment(), null,
                    RevenueStatisticFragment.class.getSimpleName(), false);
        });
    }

    private void setupToolbar() {
        Toolbar toolbar = binding.topAppBar.mToolbar;
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        binding.topAppBar.tvTitleToolbar.setText("Dịch vụ");
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            setResult(RESULT_OK);
            onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCodeConst.CHARGE_MONEY_REQ_CODE && resultCode == RESULT_OK) {
            FragmentUtil.replaceFragment(this, R.id.my_wallet_container, new BalanceMoneyFragment(), null,
                    BalanceMoneyFragment.class.getSimpleName(), false);
        }
    }
}