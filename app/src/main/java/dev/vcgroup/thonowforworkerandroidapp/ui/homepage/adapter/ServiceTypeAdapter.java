package dev.vcgroup.thonowforworkerandroidapp.ui.homepage.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import dev.vcgroup.thonowforworkerandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowforworkerandroidapp.data.model.ServiceType;
import dev.vcgroup.thonowforworkerandroidapp.data.model.Worker;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ItemServiceTypeBinding;
import dev.vcgroup.thonowforworkerandroidapp.databinding.ItemServiceTypeCardviewBinding;

public class ServiceTypeAdapter extends RecyclerView.Adapter<ServiceTypeAdapter.ServiceTypeViewHolder> implements Filterable {
    private static final String TAG = ServiceTypeAdapter.class.getSimpleName();
    public enum ServiceTypeEnum {
        BASIC,
        CARD_VIEW
    }

    private List<ServiceType> list;
    private List<ServiceType> filteredList;
    private OnServiceTypeListener onServiceTypeListener;
    private ServiceTypeEnum serviceTypeEnum;
    private Set<Integer> lastCheckedPositions = new HashSet<>();
    private Worker currentUser;
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }

    public ServiceTypeAdapter(OnServiceTypeListener onServiceTypeListener, @NonNull ServiceTypeEnum serviceTypeEnum, Worker currentUser) {
        this.onServiceTypeListener = onServiceTypeListener;
        this.list = new ArrayList<>();
        this.currentUser = currentUser;
        this.serviceTypeEnum = serviceTypeEnum;
        this.populateData();
    }

    private void populateData() {
        list.add(new ServiceType("1", "Sửa chữa nước", "ic_tho_sua_chua_nuoc"));
        list.add(new ServiceType("2", "Sửa máy tính", "ic_tho_sua_may_tinh"));
        list.add(new ServiceType("3", "Sửa điện gia dụng", "ic_tho_sua_dien_gia_dung"));
        list.add(new ServiceType("4", "Người giúp việc", "ic_nguoi_giup_viec"));
        list.add(new ServiceType("5", "Sửa điện lạnh", "ic_tho_sua_dien_lanh"));
        list.add(new ServiceType("6", "Sửa thông hút cống", "ic_tho_thong_hut_cong"));
        this.filteredList = list;

        if (currentUser != null) {
            List<DocumentReference> selectedServiceType = currentUser.getServiceTypeList();
            selectedServiceType.forEach(documentReference -> {
                documentReference.get()
                        .addOnSuccessListener(documentSnapshot1 -> {
                            Log.d(TAG, list.indexOf(documentSnapshot1.toObject(ServiceType.class)) + "22");
                            lastCheckedPositions.add(list.indexOf(documentSnapshot1.toObject(ServiceType.class)));
                        })
                        .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
            });
        }

    }

    @NonNull
    @NotNull
    @Override
    public ServiceTypeViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (serviceTypeEnum.equals(ServiceTypeEnum.BASIC)) {
            return new ServiceTypeViewHolder(ItemServiceTypeBinding.inflate(inflater, parent, false), onServiceTypeListener);
        }
        return new ServiceTypeViewHolder(ItemServiceTypeCardviewBinding.inflate(inflater, parent, false), onServiceTypeListener);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint == null || constraint.length() == 0) {
                    filterResults.values = filteredList;
                    filterResults.count = filteredList.size();
                } else {
                    List<ServiceType> resultList = new ArrayList<>();
                    if (resultList.isEmpty()) {
                        filteredList.forEach(serviceType -> {
                            if (serviceType.getName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                resultList.add(serviceType);
                            }
                        });
                    }
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                list = (List<ServiceType>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ServiceTypeAdapter.ServiceTypeViewHolder holder, int position) {
        ServiceType serviceType = getItem(position);

        if (serviceTypeEnum.equals(ServiceTypeEnum.CARD_VIEW)) {
            holder.bindCardView(serviceType);
            if (lastCheckedPositions.isEmpty()) {
                DocumentReference docRef = db.collection(CollectionConst.COLLECTION_SERVICE_TYPE).document(serviceType.getId());
                holder.cardviewBinding.rbServiceTypeCheck.setChecked(currentUser.getServiceTypeList().contains(docRef));
            }
        } else {
            holder.bind(serviceType);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public ServiceType getItem(int position) {
        return list.get(position);
    }

    public List<DocumentReference> getSelectedServiceTypeList() {
        if (this.currentUser.getServiceTypeList() == null) {
            return new ArrayList<>();
        }
        return this.currentUser.getServiceTypeList();
    }

    public void addSelectedServiceType(int position) {
        ServiceType serviceType = getItem(position);
        DocumentReference serviceDocRef = db.collection(CollectionConst.COLLECTION_SERVICE_TYPE).document(serviceType.getId());
        List<DocumentReference> serviceTypeList = getSelectedServiceTypeList();
        if (!serviceTypeList.contains(serviceDocRef)) {
            serviceTypeList.add(serviceDocRef);
        }
        this.currentUser.setServiceTypeList(serviceTypeList);
    }

    public class ServiceTypeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ItemServiceTypeBinding binding;
        private ItemServiceTypeCardviewBinding cardviewBinding;
        private OnServiceTypeListener onServiceTypeListener;

        public ServiceTypeViewHolder(@NonNull @NotNull ItemServiceTypeCardviewBinding binding, OnServiceTypeListener onServiceTypeListener) {
            super(binding.getRoot());
            this.cardviewBinding = binding;
            this.onServiceTypeListener = onServiceTypeListener;
            cardviewBinding.getRoot().setOnClickListener(this);
        }

        public ServiceTypeViewHolder(@NonNull @NotNull ItemServiceTypeBinding binding, OnServiceTypeListener onServiceTypeListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.onServiceTypeListener = onServiceTypeListener;
            binding.getRoot().setOnClickListener(this);
        }

        public void bind(ServiceType item) {
            binding.setItem(item);
            binding.executePendingBindings();
        }

        public void bindCardView(ServiceType item) {
            cardviewBinding.setItem(item);
            cardviewBinding.executePendingBindings();
        }

        @Override
        public void onClick(View v) {
            if (onServiceTypeListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onServiceTypeListener.onServiceTypeClickedListener(position);
                    if (cardviewBinding != null) {
                        cardviewBinding.rbServiceTypeCheck.setChecked(!cardviewBinding.rbServiceTypeCheck.isChecked());
                        addSelectedServiceType(position);
                        Set<Integer> copyOfLastCheckedPositions = lastCheckedPositions;
                        if (cardviewBinding.rbServiceTypeCheck.isChecked()) {
                            lastCheckedPositions.add(position);
                        } else {
                            lastCheckedPositions.remove(position);
                        }

                        copyOfLastCheckedPositions.forEach(ServiceTypeAdapter.this::notifyItemChanged);

                        lastCheckedPositions.forEach(ServiceTypeAdapter.this::notifyItemChanged);
                    }
                }
            }
        }
    }

    public interface OnServiceTypeListener {
        void onServiceTypeClickedListener(int position);
    }
}
