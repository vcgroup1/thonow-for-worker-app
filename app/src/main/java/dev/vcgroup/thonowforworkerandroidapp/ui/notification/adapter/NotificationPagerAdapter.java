package dev.vcgroup.thonowforworkerandroidapp.ui.notification.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import org.jetbrains.annotations.NotNull;

import dev.vcgroup.thonowforworkerandroidapp.ui.notification.conservation.ConservationListFragment;
import dev.vcgroup.thonowforworkerandroidapp.ui.notification.remotenotification.NotificationListFragment;

public class NotificationPagerAdapter extends FragmentStateAdapter {

    public NotificationPagerAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @NotNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 1: return new NotificationListFragment();
            case 0:
            default: return new ConservationListFragment();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
