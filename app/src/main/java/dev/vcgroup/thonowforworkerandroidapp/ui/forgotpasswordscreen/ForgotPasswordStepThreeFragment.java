package dev.vcgroup.thonowforworkerandroidapp.ui.forgotpasswordscreen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import dev.vcgroup.thonowforworkerandroidapp.R;
import dev.vcgroup.thonowforworkerandroidapp.databinding.FragmentForgotPasswordStepThreeBinding;
import dev.vcgroup.thonowforworkerandroidapp.util.FragmentUtil;

public class ForgotPasswordStepThreeFragment extends Fragment {
    private FragmentForgotPasswordStepThreeBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forgot_password_step_three, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.btnGoToLoginAfterReset.setOnClickListener(view1 -> {
            // navigate to ChangePasswordFragment
            FragmentUtil.replaceFragment(getActivity(), R.id.forgot_password_fragment_container, new ForgotPasswordStepFourFragment(),
                    null, ForgotPasswordStepThreeFragment.class.getSimpleName(), false);
        });

        binding.btnDoLater.setOnClickListener(view12 -> {
//            Intent intent = new Intent(getActivity().getApplicationContext(), MainScreenActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
        });
    }
}
